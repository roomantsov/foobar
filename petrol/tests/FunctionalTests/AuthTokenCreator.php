<?php


namespace Tests\FunctionalTests;


use App\Service\RedisService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class AuthTokenCreator extends WebTestCase
{
    public const PHONE = '0682421643';

    public static function getAuthToken(): string
    {
        $client = static::createClient();

        $client->request(Request::METHOD_POST, '/api/sms/send/' . self::PHONE);

        $body = '
            {
              "phone": "' . self::PHONE . '",
              "code": ' . self::getCode() . '
            }
        ';

        $client->request(Request::METHOD_POST, '/api/auth', [], [], [], $body);
        $content = $client->getResponse()->getContent();

        static::ensureKernelShutdown();

        return json_decode($content, true)['token'];
    }

    public static function getCode()
    {
        self::bootKernel();
        $redis = self::$container->get(RedisService::class);
        return $redis->getAuthData(self::PHONE)['code'];
    }
}