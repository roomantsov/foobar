<?php


namespace Tests\FunctionalTests\Account;


use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tests\FunctionalTests\AuthTokenCreator;

class TransactionsControllerTest extends WebTestCase
{
    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = static::createClient([], ['HTTP_AUTHORIZATION' => 'Bearer ' . AuthTokenCreator::getAuthToken()]);
    }

    public function testGetTransactionsAction(): void
    {
        $this->client->request(Request::METHOD_GET, '/api/transactions', [], [], []);
        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $content = $this->client->getResponse()->getContent();

        self::assertJson($content);

        $transactions = json_decode($content, true);

        if (isset($transactions[0])) {
            $this->client->request(Request::METHOD_GET, '/api/transactions/' . $transactions[0]['TransactionId'], [], [], []);
            self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
            self::assertJson($content);
        }
    }

}