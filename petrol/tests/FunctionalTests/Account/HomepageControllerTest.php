<?php


namespace Tests\FunctionalTests\Account;


use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tests\FunctionalTests\AuthTokenCreator;

class HomepageControllerTest extends WebTestCase
{
    private KernelBrowser $client;

    public function setUp(): void
    {
        $token = AuthTokenCreator::getAuthToken();
        $this->client = static::createClient([], ['HTTP_AUTHORIZATION' => 'Bearer ' . $token]);
    }

    public function testGetHomepageData(): void
    {
        $this->client->request(Request::METHOD_GET, '/api/homepage', [], [], []);
        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $content = $this->client->getResponse()->getContent();

        self::assertJson($content);
    }
}