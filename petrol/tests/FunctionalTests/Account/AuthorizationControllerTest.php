<?php


namespace Tests\FunctionalTests\Account;


use App\Service\Account\AuthorizationService;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tests\FunctionalTests\AuthTokenCreator;

class AuthorizationControllerTest extends WebTestCase
{
    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
        $this->client->request(Request::METHOD_POST, '/api/sms/send/' . AuthTokenCreator::PHONE);
    }

    public function testAuthorization(): void
    {
        $body =
            '{
              "phone": "' . AuthTokenCreator::PHONE . '",
              "code": ' . AuthTokenCreator::getCode() . '
            }'
        ;

        $this->client->request(Request::METHOD_POST, '/api/auth', [], [], [], $body);
        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $content = $this->client->getResponse()->getContent();

        self::assertJson($content);

        $content = json_decode($content, true);

        foreach (AuthorizationService::REQUIRE_HOLDER_DATA as $v) {
            self::assertArrayHasKey($v, $content);
        }


    }
}