<?php


namespace Tests\FunctionalTests\Account;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SmsSenderControllerTest extends WebTestCase
{

    public function testSend(): void
    {
        $client = static::createClient();

        $client->request(Request::METHOD_POST, '/api/sms/send/0996789926');

        self::assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }
}