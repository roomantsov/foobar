<?php
namespace Tests\IntegrationTests\Service\Discount;


use App\DTO\Discount\DiscountData;
use App\DTO\Product;
use App\Exception\HttpClientException;
use App\Exception\MPosContentException;
use App\Service\Discount\DiscountService;
use Money\Money;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DiscountServiceIntegrationTest extends KernelTestCase
{
//TestCase -- unit tests
//KernelTestCase -- integration tests
//WebTestCase -- functional tests
    /**
     * @var DiscountService|object|null
     */
    private $discountService;
    /**
     * @var int
     */
    private $terminalId;
    /**
     * @var string
     */
    private $phone;
    /**
     * @var Product
     */
    private $fuel;

    public function setUp()
    {
        self::bootKernel();
        // returns the real and unchanged service container
//        $container = self::$kernel->getContainer();

        // gets the special container that allows fetching private services
        $this->discountService = self::$container->get(DiscountService::class);
        $money = Money::UAH(2650);
        $this->phone = '0682421643';
        $this->terminalId = 1001;
        $this->fuel = new Product();
        $this->fuel
            ->setId(5)
            ->setPrice($money)
            ->setName('А-95 Бензин')
        ;
    }

    /**
     * @dataProvider setDiscountTestData
     * @param $amount
     */
    public function testIssuanceRunsWithoutErrors($amount): void
    {
        $orderId = uniqid('', true);
        $this->fuel
            ->setAmount($amount)
            ->setGiveAmount($amount)
        ;

        $issuance = $this->discountService->issuance($this->phone, $this->terminalId, $this->fuel, $orderId);
        self::assertInstanceOf(DiscountData::class, $issuance);
        self::assertEquals(0, $issuance->getErrorCode());
    }

    /**
     * @dataProvider setDiscountTestData
     * @param $amount
     * @throws HttpClientException
     * @throws MPosContentException
     */
    public function testRedemptionRunWithoutError($amount): void
    {
        $orderId = uniqid('', true);
        $this->fuel
            ->setAmount($amount)
            ->setGiveAmount($amount)
        ;

        $discountData = $this->discountService->calculateDiscount($this->phone, $this->terminalId, $this->fuel, $orderId);
        self::assertInstanceOf(DiscountData::class, $discountData);
        self::assertEquals(0, $discountData->getErrorCode());

        $redemption = $this->discountService->redemption($this->phone, $this->terminalId, $this->fuel, $orderId, $discountData);
        self::assertInstanceOf(DiscountData::class, $redemption);
        self::assertEquals(0, $redemption->getErrorCode());

    }

    /**
     * @return array
     */
    public function setDiscountTestData(): array
    {
        $array = [];
        for ($i = 22; $i < 25; $i++) {
            $array[] = [$i];
        }

        return $array;
    }
}
