<?php


namespace App\Exception;


use Exception;
use Throwable;

class WayforpayException extends Exception
{
    public function __construct($status, $reason, $phone, $code = 0, Throwable $previous = null)
    {
        $message = 'TransactionStatus: ' . $status . '; Reason: '. $reason . '; phone: '. $phone;

        parent::__construct($message, $code, $previous);
    }
}