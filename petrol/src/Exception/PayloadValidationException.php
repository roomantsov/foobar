<?php


namespace App\Exception;


use Exception;

class PayloadValidationException extends Exception
{
    private $violations;

    public function __construct($violations)
    {
        $this->violations = $violations;
        parent::__construct('Validation failed.');
    }

    public function getMessages()
    {
        $messages = [];
        foreach ($this->violations as  $violation) {
            $messages[$violation->getPropertyPath()] = $violation->getMessage();

        }
        return $messages;
    }
}