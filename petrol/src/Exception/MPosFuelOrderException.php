<?php


namespace App\Exception;


use Exception;
use Throwable;

class MPosFuelOrderException extends Exception
{
    public function __construct($status, $errorDesc, $code = 0, Throwable $previous = null)
    {
        parent::__construct('Order status: ' . $status . 'Error: ' . $errorDesc, $code, $previous);
    }
}