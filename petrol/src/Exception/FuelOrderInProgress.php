<?php


namespace App\Exception;


use Exception;
use Throwable;

class FuelOrderInProgress extends Exception
{
    public function __construct($message = "Order in progress...", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}