<?php


namespace App\EventListener;


use App\Exception\PayloadValidationException;
use App\Service\Http\ResponseHelper;
use App\Service\LoggerService;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;

class ApiExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * @var LoggerService
     */
    private $loggerService;

    public function __construct(LoggerService $loggerService)
    {
        $this->loggerService = $loggerService;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException'
        ];
    }

    public function onKernelException(ExceptionEvent $event)
    {
        // only reply to /api URLs
        if (strpos($event->getRequest()->getPathInfo(), '/api') !== 0) {
            return;
        }

        $e = $event->getThrowable();

        if ($e instanceof JWTDecodeFailureException) {
            $errors = $e->getMessage();
            $message = ResponseHelper::INTERNAL_SERVER_ERROR;
            $statusCode = JsonResponse::HTTP_UNAUTHORIZED;
        } elseif ($e instanceof PayloadValidationException) {
            $errors = $e->getMessages();
            $message = $e->getMessage();
            $statusCode = JsonResponse::HTTP_BAD_REQUEST;
        } else {
            $this->loggerService->logError($e);
            $errors = ResponseHelper::INTERNAL_SERVER_ERROR;
            $message = $e->getMessage();
            $statusCode = $e instanceof HttpExceptionInterface ? $e->getStatusCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        }

        $event->setResponse(new JsonResponse([
            'message' => $message,
            'errors' => $errors,
        ], $statusCode));
    }

}