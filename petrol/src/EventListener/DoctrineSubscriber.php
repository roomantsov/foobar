<?php

namespace App\EventListener;

use App\Entity\Deal;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events as OrmEvents;
use Symfony\Component\HttpFoundation\RequestStack;

class DoctrineSubscriber implements EventSubscriber
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var string
     */
    private $iconFolderPath;

    public function getSubscribedEvents(): array
    {
        return [
            OrmEvents::postLoad
        ];
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $baseUrl = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();

        if($entity instanceof Deal){
            $entity->setIconFullPath($baseUrl . $this->iconFolderPath . $entity->getIcon());
        }
    }

    public function __construct(string $iconFolderPath, RequestStack $requestStack){
        $this->iconFolderPath = $iconFolderPath;
        $this->requestStack = $requestStack;
    }
}