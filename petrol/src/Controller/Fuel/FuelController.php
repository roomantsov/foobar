<?php


namespace App\Controller\Fuel;


use App\Entity\Fuel;
use App\Repository\FuelRepository;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;


class FuelController extends AbstractController
{
    /**
     * Fuel prices from admin panel
     * @Route("api/fuel", name="get_fuel", methods="GET"),
     * @OA\Tag(name="Fuel"),
     * @OA\Response(
     *     response=200,
     *     description="information about fuel prices from admin panel",
     *     @Model(type=Fuel::class, groups={"fuel"})
     * ),
     * @OA\Response(
     *     response=401,
     *     description="Invalid or expired token",
     * ),
     * @param FuelRepository $fuelRepository
     * @return JsonResponse
     */
    public function getFuel(FuelRepository $fuelRepository): JsonResponse
    {
        return $this->json($fuelRepository->findAll(), JsonResponse::HTTP_OK, [], [
            'groups' => ['fuel'],
        ]);
    }
}