<?php


namespace App\Controller\Fuel;


use App\DTO\Gas\DispenserStatusData;
use App\Exception\HttpClientException;
use App\Exception\MPosContentException;
use App\Repository\CardTokenRepository;
use App\Service\Fuel\DispenserStatusService;
use App\Service\Http\ResponseHelper;
use App\Service\LoggerService;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Component\Security\Core\User\UserInterface;

class DispenserStatusController extends AbstractController
{
    /**
     * Check dispenser status and fuel prices from current dispenser
     * @Route(
     *    "api/dispenser/{terminal}/{dispenser}/status",
     *    name="dispenser_status",
     *    methods={"GET"},
     *    requirements={"terminal"="^[0-9]{4,5}$", "dispenser"="^[0-9]{1,2}$"}
     *  ),
     * @OA\Tag(name="Fuel"),
     * @OA\Response(
     *     response=200,
     *     description="information about fuel in this nozzle and all user's available credit cards",
     *     @Model(type=DispenserStatusData::class, groups={"dispenser_status", "all_tokens"})
     * ),
     * @OA\Response(
     *     response=401,
     *     description="Invalid or expired token",
     * ),
     * @OA\Response(
     *     response=403,
     *     description="Nozzle didn't take off",
     * )
     * @param int $terminal
     * @param int $dispenser
     * @param DispenserStatusService $dispenserStatusService
     * @param LoggerService $loggerService
     * @param ResponseHelper $responseHelper
     * @param CardTokenRepository $cardTokenRepository
     * @param UserInterface $user
     * @return JsonResponse
     */
    public function dispenserStatus(
        int $terminal,
        int $dispenser,
        DispenserStatusService $dispenserStatusService,
        LoggerService $loggerService,
        ResponseHelper $responseHelper,
        CardTokenRepository $cardTokenRepository,
        UserInterface $user
    ): JsonResponse
    {
        try {
            $content = $dispenserStatusService->getStatus($terminal, $dispenser);
            $tokens = $cardTokenRepository->findBy([
                'phone' => $user->getUsername()
            ]);

            $content->setTokens($tokens);
            $responseCode = JsonResponse::HTTP_OK;
        } catch (HttpClientException $e) {
            $loggerService->logError($e, $user->getUsername());
            $responseCode = $responseHelper->validateErrorCode($e->getCode());
            $content = $responseHelper->getErrorContent($responseCode);
        } catch (MPosContentException $e) {
            $loggerService->logError($e, $user->getUsername());
            $responseCode =
                $responseHelper->validateErrorCode($e->getCode()) !== JsonResponse::HTTP_UNAUTHORIZED ?
                $responseHelper->validateErrorCode($e->getCode()) :
                JsonResponse::HTTP_NOT_FOUND;
            $content = $responseHelper->getErrorContent($responseCode);
        }

        return $this->json($content, $responseCode, [], [
            'groups' => ['dispenser_status', 'all_tokens']
        ]);
    }
}