<?php


namespace App\Controller\Fuel;


use App\Service\Fuel\FuelPriceService;
use App\Service\Http\ResponseHelper;
use App\Service\LoggerService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Component\Security\Core\User\UserInterface;

class FuelPriceController extends AbstractController
{


    /**
     * Fuel prices from current gas station
     * @Route("api/fuel/{terminal}", name="fuel_price", methods={"GET"}, requirements={"terminal"="^[0-9]{4,5}$"})
     * @OA\Tag(name="Fuel"),
     * @OA\Response(
     *     response=200,
     *     description="all available fuel from current gas station",
     * ),
     * @OA\Response(
     *     response=401,
     *     description="Invalid or expired token",
     * ),
     * @param int $terminal
     * @param LoggerService $loggerService
     * @param ResponseHelper $responseHelper
     * @param FuelPriceService $fuelPriceService
     * @param UserInterface $user
     * @return JsonResponse
     */
    public function fuelPrice(
        int $terminal,
        LoggerService $loggerService,
        ResponseHelper $responseHelper,
        FuelPriceService $fuelPriceService,
        UserInterface $user
    ): JsonResponse
    {
        try {
            $content = $fuelPriceService->getFuelPrice($terminal);
            $responseCode = JsonResponse::HTTP_OK;
        } catch (Exception $e) {
            $loggerService->logError($e, $user->getUsername());
            $responseCode = $responseHelper->validateErrorCode($e->getCode());
            $content = $responseHelper->getErrorContent($responseCode);
        }

        return $this->json($content, $responseCode);
    }
}