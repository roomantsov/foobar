<?php


namespace App\Controller\Fuel;

use App\DTO\Product;
use App\DTO\Request\FuelOrderRequestDTO;
use App\Entity\FuelOrders;
use App\Exception\HttpClientException;
use App\Exception\WayforpayException;
use App\Message\FuelOrderStatus;
use App\Message\PublisherMessage;
use App\Repository\CardTokenRepository;
use App\Service\Discount\DiscountService;
use App\Service\Fuel\DispenserStatusService;
use App\Service\Fuel\FuelOrderService;
use App\Service\Http\ResponseHelper;
use App\Service\LoggerService;
use App\Service\Publisher\PublisherService;
use App\Service\SerializerService;
use App\Service\Wayforpay\ChargeService;
use App\Service\Wayforpay\Wayforpay;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Money\Money;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use OpenApi\Annotations as OA;
use Symfony\Component\Security\Core\User\UserInterface;

class FuelOrderController extends AbstractController
{
    /**
     * Initiate order from gas station
     * @Route(
     *     "/api/fuel/{terminal}/{dispenser}/order",
     *      name="fuel_order",
     *      methods={"POST"},
     *      requirements={"terminal"="^[0-9]{4,5}$", "dispenser"="^[0-9]{1,2}$"}
     *  )
     * @OA\Tag(name="Fuel")
     * @OA\RequestBody(
     *     @OA\JsonContent(
     *         ref=@Model(type=FuelOrderRequestDTO::class)
     *     ),
     *     required=true,
     *     description="discountSum - INT. For partial bonus redeem. if null - will be charged all bonuses. IMPORTANT - discountSum MUST be in coins (f.e for 5 UAH discountSum will be 500)",
     * ),
     * @OA\Response(
     *     response=200,
     *     description="Money hold status, order status, publisher token and publisher url for mercure callback",
     * ),
     * @OA\Response(
     *     response=401,
     *     description="Invalid or expired token",
     * )
     * @param string $terminal
     * @param string $dispenser
     * @param FuelOrderRequestDTO $fuelOrderRequest
     * @param LoggerService $loggerService
     * @param ResponseHelper $responseHelper
     * @param FuelOrderService $fuelOrderService
     * @param DispenserStatusService $dispenserStatusService
     * @param ChargeService $chargeService
     * @param CardTokenRepository $cardTokenRepository
     * @param PublisherService $publisherService
     * @param DiscountService $discountService
     * @param UserInterface $user
     * @return JsonResponse
     * @throws HttpClientException
     */
    public function setFuelOrder(
        string $terminal,
        string $dispenser,
        FuelOrderRequestDTO $fuelOrderRequest,
        LoggerService $loggerService,
        ResponseHelper $responseHelper,
        FuelOrderService $fuelOrderService,
        DispenserStatusService $dispenserStatusService,
        ChargeService $chargeService,
        CardTokenRepository $cardTokenRepository,
        PublisherService $publisherService,
        DiscountService $discountService,
        UserInterface $user
    ): JsonResponse
    {
        $fuel = new Product();
        $orderUuid = $chargeService->generateUuid();

        try {
            $dispenserStatus = $dispenserStatusService->getStatus($terminal, $dispenser);

            $fuel
                ->setId($dispenserStatus->getFuelId())
                ->setPrice($dispenserStatus->getFuelPrice())
                ->setName($dispenserStatus->getFuelName())
                ->setAmount($fuelOrderRequest->getFuelAmount())
                ->setGiveAmount($fuelOrderRequest->getFuelAmount());
            ;

            $card = $cardTokenRepository->findOneOrError([
                'uuid' => $fuelOrderRequest->getCardId(),
                'phone' => $user->getUsername()
            ]);

            if ($fuelOrderRequest->isDiscount()) {
                try {
                    $discountSum = Money::UAH($fuelOrderRequest->getDiscountSum());
                    $calculatedDiscount = $discountService->calculateDiscount($user->getUsername(), $terminal, $fuel, $orderUuid);
                    $bonusRedeem = !is_null($fuelOrderRequest->getDiscountSum()) && $discountSum->lessThanOrEqual($calculatedDiscount->getBonusRedeem()) ?
                        $discountSum :
                        $calculatedDiscount->getBonusRedeem()
                    ;

                    $fuel->setBonusRedeem($bonusRedeem);
                } catch (Exception $e) {
                    $loggerService->logError($e, $user->getUsername());
                }
            }

            //hold money here
            $holdMoney = $chargeService->hold($user->getUsername(), $card->getRecToken(), $fuel, $orderUuid);
            //check holding status
            $chargeService->checkWayforpayStatus($holdMoney);
            //starting to fill gas
            $fuelOrder = $fuelOrderService->setFuelOrder($terminal, $dispenser, $fuel);

            $fuelOrderStatus = new FuelOrderStatus();
            $fuelOrderStatus
                ->setPhone($user->getUsername())
                ->setTerminal($terminal)
                ->setDispenser($dispenser)
                ->setCard($card->getRecToken())
                ->setOrderUuid($orderUuid)
                ->setOrderId($fuelOrder->getOrderId())
                ->setFuel($fuel)
                ->setDiscount($fuelOrderRequest->isDiscount())
                ->setDiscountSum($fuelOrderRequest->getDiscountSum())
                ->setCardPan($card->getCardPan())
                ->setCardUuid($card->getUuid())
                ->setHoldStatus($holdMoney->getReason())
                ->setHoldStatusCode($holdMoney->getReasonCode())
                ->setHoldSum($holdMoney->getAmount())
                ->setSuccessUrl($this->generateUrl('fuel_order_success', [], UrlGeneratorInterface::ABSOLUTE_URL))
            ;

            //send to messenger async job to check order status
            $this->dispatchMessage($fuelOrderStatus);

            //check order status
            $orderStatus = $fuelOrderService->getFuelOrderStatus($terminal, $dispenser, $fuelOrder->getOrderId());

            $content = [
              'publisher_url' => $publisherService->getStatusUpdateUrl($fuelOrder->getOrderId()),
              'token' => $publisherService->getSubscribeToken(),
              'hold' => $holdMoney->getReason(),
              'holdSum' => $holdMoney->getAmount(),
              'fuelOrder' => $fuelOrder->getErrorDescription(),
              'OrderStatus' => $orderStatus->getStatusName()
            ];

            $responseCode = JsonResponse::HTTP_OK;
        } catch (Exception $e) {
            if (
                !empty($holdMoney) &&
                !empty($fuelOrder) &&
                $holdMoney->getReasonCode() === Wayforpay::WAYFORPAY_STATUS_OK &&
                (int)$fuelOrder->getErrorCode() !== 0
            ) {
                $chargeService->refund($fuel->getEstimatedOrderAmount(), $holdMoney->getOrderReference());
            }
            $loggerService->logError($e, $user->getUsername());
            $responseCode = $responseHelper->validateErrorCode($e->getCode());
            $content = $responseHelper->getErrorContent($responseCode);
        }

        return $this->json($content, $responseCode);
    }

    /**
     * @Route(
     *     "/api/fuel/success",
     *      name="fuel_order_success",
     *      methods={"POST"},
     *      host="%current_hostname%"
     *  )
     * @param FuelOrderStatus $orderStatus
     * @param LoggerService $loggerService
     * @param DiscountService $discountService
     * @param ChargeService $chargeService
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function onSuccess(
        FuelOrderStatus $orderStatus,
        LoggerService $loggerService,
        DiscountService $discountService,
        ChargeService $chargeService,
        SerializerService $serializerService,
        EntityManagerInterface $em
    ): JsonResponse
    {
        $fuel = $orderStatus->getFuel();
        $phone = $orderStatus->getPhone();
        $orderUuid = $orderStatus->getOrderUuid();
        $fuel->setGiveAmount($orderStatus->getGiveAmount());
        $fuelOrders = new FuelOrders();

        //we recount discount here because user may be fueled less than expected
        if ($orderStatus->isDiscount()) {
            try {
                $discountSum = Money::UAH($orderStatus->getDiscountSum());
                $fuel->setBonusRedeem($discountSum);

                $calculatedDiscount = $discountService->calculateDiscount($phone, $orderStatus->getTerminal(), $fuel, $orderUuid);
                $calculatedDiscount->setBonusRedeem($discountSum);

                $redeem = $discountService->redemption($phone, $orderStatus->getTerminal(), $fuel, $orderUuid, $calculatedDiscount);
                $fuelOrders->setDiscountSum($redeem->getBonusRedeem());
            } catch (Exception $e) {
                $loggerService->logError($e, $phone);
            }
        } else {
            $issuance = $discountService->issuance($phone, $orderStatus->getTerminal(), $fuel, $orderUuid);
        }

        try {
            $pay = $chargeService->confirmHold($fuel->getActualOrderAmount(), $orderUuid);

            $bonusBalance =  isset($issuance) ? $issuance->getBalanceActive() : false;

            $content = [
                'fuelActualAmount' => $orderStatus->getGiveAmount(),
                'orderAmountWithoutBonusesCharge' => $fuel->getOrderAmountWithoutBonuses(),
                'refund' => $chargeService->getRefund($fuel),
                'pay' =>  $pay->getReason(),
                'paySum' =>  $pay->getAmount(),
                'issuance' => isset($issuance) ? $issuance->getBonusIssuance() : $redeem->getBonusIssuance(),
                'redeem' => isset($redeem) ? $redeem->getBonusRedeem() : false,
                'bonusBalance' => isset($redeem) ? $redeem->getBalanceActive() : $bonusBalance,
            ];

            $fuelOrders
                ->setPhone($phone)
                ->setTerminalId($orderStatus->getTerminal())
                ->setDispenserId($orderStatus->getDispenser())
                ->setClientAmount($fuel->getAmount())
                ->setIsDiscount($orderStatus->isDiscount())
                ->setCardUuid($orderStatus->getCardUuid())
                ->setCardPan($orderStatus->getCardPan())
                ->setHoldStatus($orderStatus->getHoldStatus())
                ->setHoldStatusCode($orderStatus->getHoldStatusCode())
                ->setHoldSum($orderStatus->getHoldSum())
                ->setOrderUuid($orderUuid)
                ->setMposOrderStatus($orderStatus->getOrderStatus())
                ->setMposOrderId($orderStatus->getOrderId())
                ->setActualAmount($orderStatus->getGiveAmount())
                ->setRefund($chargeService->getRefund($fuel))
                ->setPayStatus($pay->getReason())
                ->setPayStatusCode($pay->getReasonCode())
                ->setPaySum($pay->getAmount())
                ->setIssuanceSum(isset($issuance) ? $issuance->getBonusIssuance() : $redeem->getBonusIssuance())
                ->setFuelName($fuel->getName())
                ->setFuelPrice($fuel->getPrice())
                ->setCreateDate($orderStatus->getCreateDate())
            ;

            $em->persist($fuelOrders);
            $em->flush();

            $loggerService->debug('SUCCESS', $content);
            //push notification to client
            $publisherMessage = new PublisherMessage();
            $publisherMessage
                ->setOrderId($orderStatus->getOrderId())
                ->setContent($content)
            ;

            $this->dispatchMessage($publisherMessage);
            $chargeService->checkWayforpayStatus($pay);
        } catch (WayforpayException $e) {
            $loggerService->logError($e, $phone);
        } catch (HttpClientException $e) {
            $loggerService->logError($e, $phone);

            if (empty($pay) || $pay->getReasonCode() !== Wayforpay::WAYFORPAY_STATUS_OK) {
                try {
                    $chargeService->confirmHold($fuel->getActualOrderAmount(), $orderUuid);
                } catch (HttpClientException $e) {
                    $loggerService->logError($e, $phone);
                }
            }
        }
        return $this->json(null, JsonResponse::HTTP_OK);
    }
}
