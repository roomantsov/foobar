<?php


namespace App\Controller\Map;


use App\DTO\Request\ReviewRequestDTO;
use App\Entity\GasStation;
use App\Entity\Reviews;
use App\Service\Http\ResponseHelper;
use App\Service\LoggerService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use OpenApi\Annotations as OA;

class ReviewController extends AbstractController
{
    /**
     * Create new review for gas station
     * @Route("api/review", name="set_review", methods="POST")
     * @OA\Tag(name="Map")
     * @OA\RequestBody(
     *     @OA\JsonContent(
     *         ref=@Model(type=ReviewRequestDTO::class)
     *     ),
     *     required=true,
     *     description="Review details. id - Gas station id",
     * ),
     * @OA\Response(
     *     response=200,
     *     description="Gas stations with updated related data",
     *     @Model(type=GasStation::class, groups={"gas_stations"})
     *
     * )
     * @OA\Response(
     *     response=401,
     *     description="Invalid or expired token",
     * )
     * @param ReviewRequestDTO $reviewRequest
     * @param EntityManagerInterface $em
     * @param LoggerService $loggerService
     * @param ResponseHelper $responseHelper
     * @param UserInterface $user
     * @return JsonResponse
     */
    public function setReview(
        ReviewRequestDTO $reviewRequest,
        EntityManagerInterface $em,
        LoggerService $loggerService,
        ResponseHelper $responseHelper,
        UserInterface $user
    ): JsonResponse
    {
        try {

            $gasStationRepository = $em->getRepository(GasStation::class);

            $reviews = new Reviews();
            $reviews
                ->setUsername($reviewRequest->getUsername())
                ->setRate($reviewRequest->getRate())
                ->setReview($reviewRequest->getReview())
                ->setGasStation($gasStationRepository->find($reviewRequest->getId()))
            ;

            $em->persist($reviews);
            $em->flush();

            $responseCode = JsonResponse::HTTP_OK;
            $content = $gasStationRepository->FindAll();
        } catch (Exception $e) {
            $loggerService->logError($e, $user->getUsername());
            $responseCode = $responseHelper->validateErrorCode($e->getCode());
            $content = $responseHelper->getErrorContent($responseCode);
        }

        return $this->json($content, $responseCode, [], [
            'groups' => ['gas_stations'],
            DateTimeNormalizer::FORMAT_KEY => 'd/m/Y'
        ]);
    }
}