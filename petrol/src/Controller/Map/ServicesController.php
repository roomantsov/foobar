<?php


namespace App\Controller\Map;


use App\Repository\FuelRepository;
use App\Repository\ServicesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

class ServicesController extends AbstractController
{
    /**
     * List of available services from admin panel
     * @Route("api/services", name="get_services", methods="GET")
     * @OA\Tag(name="Map"),
     * @OA\Response(
     *     response=200,
     *     description="Services and fuel from admin panel",
     * ),
     * @OA\Response(
     *     response=401,
     *     description="Invalid or expired token",
     * ),
     * @param ServicesRepository $servicesRepository
     * @param FuelRepository $fuelRepository
     * @return JsonResponse
     */
    public function getServices(ServicesRepository $servicesRepository, FuelRepository $fuelRepository): JsonResponse
    {
        $content = [
            'services' => $servicesRepository->findAll(),
            'fuel' => $fuelRepository->findAll()
        ];

        return $this->json($content, JsonResponse::HTTP_OK, [], [
            'groups' => ['services'],
        ]);
    }
}