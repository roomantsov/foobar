<?php


namespace App\Controller\Map;

use App\Repository\GasStationRepository;
use App\Entity\GasStation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use OpenApi\Annotations as OA;

class GasStationController extends AbstractController
{
    /**
     * List of gas stations from admin panel
     * @Route("api/stations", name="get_gas_stations", methods="GET")
     * @OA\Tag(name="Map"),
     * @OA\Response(
     *     response=200,
     *     description="Gas stations with related data",
     *     @Model(type=GasStation::class, groups={"gas_stations"})
     * ),
     * @OA\Response(
     *     response=401,
     *     description="Invalid or expired token",
     * ),
     * @param GasStationRepository $gasStationRepository
     * @return JsonResponse
     */
    public function getGasStations(GasStationRepository $gasStationRepository): JsonResponse
    {
        return $this->json($gasStationRepository->FindAll(), Response::HTTP_OK, [], [
            'groups' => ['gas_stations'],
            DateTimeNormalizer::FORMAT_KEY => 'd/m/Y'
        ]);
    }
}
