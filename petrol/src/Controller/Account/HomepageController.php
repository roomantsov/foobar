<?php


namespace App\Controller\Account;


use App\Repository\DealRepository;
use App\Service\Account\AuthorizationService;
use App\Service\Account\UserDataService;
use App\Service\Http\ResponseHelper;
use App\Service\LoggerService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Component\Security\Core\User\UserInterface;

class HomepageController extends AbstractController
{
    /**
     * QR code, bonuses
     * @Route("api/homepage", name="get_homepage", methods="GET")
     * @OA\Tag(name="Account")
     * @OA\Response(
     *     response=200,
     *     description="Returns homepage info",
     * ),
     * @OA\Response(
     *     response=401,
     *     description="Invalid or expired token",
     *
     * )
     * @param UserDataService $userDataService
     * @param ResponseHelper $responseHelper
     * @param LoggerService $loggerService
     * @param AuthorizationService $authorizationService
     * @param UserInterface $user
     * @return JsonResponse
     */
    public function getHomepageData(
        UserDataService $userDataService,
        ResponseHelper $responseHelper,
        LoggerService $loggerService,
        AuthorizationService $authorizationService,
        UserInterface $user
    ): JsonResponse
    {
        try {
            $sessionId = $authorizationService->serviceAuthorization(
                $this->getParameter('account_api_url'),
                $this->getParameter('site_user'),
                $this->getParameter('site_password')
            );

            $bonusBalance = $userDataService->getBonusBalance($user->getId(), $sessionId);
            $accountInfo = $userDataService->getAccountInfo($user->getUsername(), $sessionId);
            $qrCode = $userDataService->generateQrCode($accountInfo->getDiscountCard());
            $responseCode = JsonResponse::HTTP_OK;
            $content = [
                'bonusBalance' => $bonusBalance,
                'discount' => $accountInfo->getDiscountScheme(),
                'QR' => $qrCode['qr'],
                'smallQR' => $qrCode['small_qr'],
            ];

        } catch (Exception $e) {
            $loggerService->logError($e, $user->getUsername());
            $responseCode = $responseHelper->validateErrorCode($e->getCode());
            $content = $responseHelper->getErrorContent($responseCode);
        }

        return $this->json($content, $responseCode);
    }

    /**
     * List of deals
     * @Route("api/deals", name="get_deals", methods="GET")
     * @OA\Tag(name="Marketing")
     * @OA\Response(
     *     response=200,
     *     description="Returns list of deals",
     * ),
     * @OA\Response(
     *     response=401,
     *     description="Invalid or expired token",
     *
     * )
     * @param ResponseHelper $responseHelper
     * @param LoggerService $loggerService
     * @param UserInterface $user
     * @param DealRepository $dealRepository
     * @return JsonResponse
     */
    public function getDealsList(
        ResponseHelper $responseHelper,
        LoggerService $loggerService,
        UserInterface $user,
        DealRepository $dealRepository
    ): JsonResponse
    {
        try {
            $deals = $dealRepository->findBy(['is_active' => true], ['priority' => 'DESC']);
            $responseCode = JsonResponse::HTTP_OK;
            $content = $deals;
        } catch (Exception $e) {
            $loggerService->logError($e, $user->getUsername());
            $responseCode = $responseHelper->validateErrorCode($e->getCode());
            $content = $responseHelper->getErrorContent($responseCode);
        }

        return $this->json($content, $responseCode);
    }
}