<?php


namespace App\Controller\Account;

use App\DTO\Request\AuthorizationRequestDTO;
use App\Service\Account\AuthorizationService;
use App\Service\Http\ResponseHelper;
use App\Service\LoggerService;
use App\Service\RedisService;
use Exception;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

class AuthorizationController extends AbstractController
{
    private const TOKEN_EXPIRE_TIME = 31536000; // 1 year expiration

    /**
     * User authorization
     * @Route("api/auth", name="auth", methods="POST"),
     * @OA\Tag(name="Authorization"),
     * @OA\RequestBody(
     *     @OA\JsonContent(
     *         ref=@Model(type=AuthorizationRequestDTO::class)
     *     ),
     *     required=true,
     *     description="User's phone number and code from the sms. WARNING: Works only after sms/send method",
     * ),
     * @OA\Response(
     *     response=200,
     *     description="Returns auth info",
     * ),
     * @OA\Response(
     *     response=401,
     *     description="User didn't request sms code or code is wrong",
     * ),
     * @param AuthorizationService $authorizationService
     * @param RedisService $redisService
     * @param ResponseHelper $responseHelper
     * @param LoggerService $loggerService
     * @param JWTEncoderInterface $encoder
     * @param AuthorizationRequestDTO $authorizationRequest
     * @return JsonResponse
     */
    public function authorization(
        AuthorizationService $authorizationService,
        RedisService $redisService,
        ResponseHelper $responseHelper,
        LoggerService $loggerService,
        JWTEncoderInterface $encoder,
        AuthorizationRequestDTO $authorizationRequest
    ): JsonResponse
    {
        $phone = $authorizationRequest->getPhone();
        $code = $authorizationRequest->getCode();

        try {
            $redisService->checkAuthData($phone, $code);

            $sessionId = $authorizationService->serviceAuthorization(
                $this->getParameter('account_api_url'),
                $this->getParameter('site_user'),
                $this->getParameter('site_password')
            );

            $content = $authorizationService->userAuthorization($phone, $sessionId);

            $token = $encoder->encode([
                'username' => $phone,
                'version' => $authorizationRequest->getVersion(),
                'id' => $authorizationService->getHolderId(),
                'exp' => time() + self::TOKEN_EXPIRE_TIME
            ]);

            $redisService->unsetAuthData($phone);

            $content = array_merge($content, ['token' => $token]);
            $responseCode = JsonResponse::HTTP_OK;
        } catch (Exception $e) {
            $loggerService->logError($e, $phone);
            $responseCode = $responseHelper->validateErrorCode($e->getCode());
            $content = [
                'errorCode' => $responseCode,
                'errorDescription' => $e->getMessage()
            ];
        }

        return $this->json($content, $responseCode);
    }
}
