<?php

namespace App\Controller\Account;

use App\Service\Account\AuthorizationService;
use App\Service\Http\ResponseHelper;
use App\Service\LoggerService;
use App\Service\RedisService;
use App\Service\Sms\SmsInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

class SmsSenderController extends AbstractController
{
    private const
        SMS_TEMPLATE = 'This is your one-time pass: ',
        AUTH_CODE_LENGTH = 5
    ;

    /**
     * Sends auth code to user's phone
     * @Route("api/sms/send/{phone}", name="sms_send", methods="POST", requirements={"phone"="^[0-9]{10,10}$"})
     * @OA\Tag(name="Authorization")
     * @OA\Response(
     *     response=200,
     *     description="Returns true, if sms has been sent",
     * ),
     * @OA\Response(
     *     response=500,
     *     description="Server error",
     *
     * )
     * @param string $phone
     * @param AuthorizationService $authorizationService
     * @param RedisService $redisService
     * @param ResponseHelper $responseHelper
     * @param LoggerService $loggerService
     * @param SmsInterface $sms
     * @return JsonResponse
     */
    public function send(
        string $phone,
        AuthorizationService $authorizationService,
        RedisService $redisService,
        ResponseHelper $responseHelper,
        LoggerService $loggerService,
        SmsInterface $sms
    ): JsonResponse
    {
        $code = $authorizationService->generateAuthCode(self::AUTH_CODE_LENGTH);

        try {
            $sms->send($phone, self::SMS_TEMPLATE . $code);
            $redisService->setAuthData($phone, [
                'code' => $code
            ]);

            $responseCode = JsonResponse::HTTP_OK;
            $content = true;
        } catch (Exception $e) {
            $loggerService->logError($e, $phone);
            $responseCode = $responseHelper->validateErrorCode($e->getCode());
            $content = $responseHelper->getErrorContent($responseCode);
        }

        return $this->json($content, $responseCode);
    }
}