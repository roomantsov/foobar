<?php


namespace App\Controller\Account;


use App\Service\Account\AuthorizationService;
use App\Service\Account\TransactionsService;
use App\Service\Http\ResponseHelper;
use App\Service\LoggerService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Component\Security\Core\User\UserInterface;

class TransactionsController extends AbstractController
{
    /**
     * User's transactions
     * @Route("api/transactions", name="get_transactions", methods="GET")
     * @Route("api/transactions/{transactionId}", name="get_transaction_details", methods="GET")
     * @OA\Tag(name="Account")
     * @OA\Response(
     *     response=200,
     *     description="Returns user's transactions or transaction details",
     * ),
     * @OA\Response(
     *     response=401,
     *     description="Invalid or expired token",
     * )
     * @param TransactionsService $transactionsService
     * @param AuthorizationService $authorizationService
     * @param LoggerService $loggerService
     * @param ResponseHelper $responseHelper
     * @param UserInterface $user
     * @param int $transactionId
     * @return JsonResponse
     */
    public function getTransactionsAction(
        TransactionsService $transactionsService,
        AuthorizationService $authorizationService,
        LoggerService $loggerService,
        ResponseHelper $responseHelper,
        UserInterface $user,
        int $transactionId = 0
    ): JsonResponse
    {
        try {
            $sessionId = $authorizationService->serviceAuthorization(
                $this->getParameter('account_api_url'),
                $this->getParameter('site_user'),
                $this->getParameter('site_password')
            );

            $content = $transactionId ?
                $transactionsService->getTransactionDetails($sessionId, $user->getId(), $transactionId) :
                $transactionsService->getTransactions($sessionId, $user->getId());

            $responseCode = JsonResponse::HTTP_OK;
        } catch (Exception $e) {
            $loggerService->logError($e, $user->getUsername());
            $responseCode = $responseHelper->validateErrorCode($e->getCode());
            $content = $responseHelper->getErrorContent($responseCode);
        }

        return $this->json($content, $responseCode);
    }
}