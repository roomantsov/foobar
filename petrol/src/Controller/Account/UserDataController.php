<?php


namespace App\Controller\Account;


use App\DTO\Request\UserRequestDTO;
use App\Service\Account\AuthorizationService;
use App\Service\Account\FileUploader;
use App\Service\Account\UserDataService;
use App\Service\Http\ResponseHelper;
use App\Service\LoggerService;
use Exception;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Component\Security\Core\User\UserInterface;

class UserDataController extends AbstractController
{
    /**
     * Account information
     * @Route("api/user", name="get_user", methods="GET")
     * @param UserDataService $userDataService
     * @param ResponseHelper $responseHelper
     * @param LoggerService $loggerService
     * @param AuthorizationService $authorizationService
     * @param UserInterface $user
     * @return JsonResponse
     * @OA\Tag(name="Account")
     * @OA\Response(
     *     response=200,
     *     description="Returns user's details",
     * ),
     * @OA\Response(
     *     response=401,
     *     description="Invalid or expired token",
     * )
     */
    public function getUserData(
        UserDataService $userDataService,
        ResponseHelper $responseHelper,
        LoggerService $loggerService,
        AuthorizationService $authorizationService,
        UserInterface $user
    ): JsonResponse
    {
        try {
            $sessionId = $authorizationService->serviceAuthorization(
                $this->getParameter('account_api_url'),
                $this->getParameter('site_user'),
                $this->getParameter('site_password')
            );

            $content = $userDataService->getUserData($user->getUsername(), $sessionId);
            $responseCode = JsonResponse::HTTP_OK;
        } catch (Exception $e) {
            $loggerService->logError($e, $user->getUsername());
            $responseCode = $responseHelper->validateErrorCode($e->getCode());
            $content = $responseHelper->getErrorContent($responseCode);
        }

        return $this->json($content, $responseCode);
    }

    /**
     * Update account information
     * @Route("api/user", name="set_user", methods="PATCH")
     * @OA\Tag(name="Account")
     * @OA\RequestBody(
     *     @OA\JsonContent(
     *         ref=@Model(type=UserRequestDTO::class, groups={"default"})
     *     ),
     *     required=true,
     *     description="key - name of changed input. Avaliable: 'FirstName', 'SecondName', 'LastName', 'BirthDate', 'Email', 'Mobile', 'Hobbies' (user's photo), 'CarTank'.
     * value - value of changed input, for 'Hobbies' - base64 converted image",
     * ),
     * @OA\Response(
     *     response=200,
     *     description="Returns true, if data has been updated",
     * ),
     * @OA\Response(
     *     response=401,
     *     description="Invalid or expired token",
     * )
     * @param FileUploader $fileUploader
     * @param UserDataService $userDataService
     * @param ResponseHelper $responseHelper
     * @param Request $request
     * @param LoggerService $loggerService
     * @param UserRequestDTO $userRequest
     * @param AuthorizationService $authorizationService
     * @param UserInterface $user
     * @return JsonResponse
     */
    public function setUserData(
        FileUploader $fileUploader,
        UserDataService $userDataService,
        ResponseHelper $responseHelper,
        Request $request,
        LoggerService $loggerService,
        UserRequestDTO $userRequest,
        AuthorizationService $authorizationService,
        UserInterface $user
    ): JsonResponse
    {
        $key = $userRequest->getKey();
        $value = $userRequest->getValue();
        $image = $userRequest->getDecodedImage();

        try {
            if ($image !== null) {
                $filename = $fileUploader->setFilename($image);
                $value = $request->getUriForPath(FileUploader::IMAGE_DIRECTORY . $filename);
                $fileUploader->upload($image);
            }

            $holderId = $user->getId();
            $sessionId = $authorizationService->serviceAuthorization(
                $this->getParameter('account_api_url'),
                $this->getParameter('site_user'),
                $this->getParameter('site_password')
            );

            $content = $userDataService->setUserData($sessionId, [
                'holderId' => $holderId,
                'holderData' => [
                    $key => $value
                ]
            ]);
            $responseCode = JsonResponse::HTTP_OK;
        } catch (Exception $e) {
            if ($image !== null) {
                $fileUploader->deleteFile();
            }

            $loggerService->logError($e, $user->getUsername());
            $responseCode = $responseHelper->validateErrorCode($e->getCode());
            $content = $responseHelper->getErrorContent($responseCode);
        }

        return $this->json($content, $responseCode);
    }
}