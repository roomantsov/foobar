<?php

namespace App\Controller;

use App\Entity\Settings;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

class SystemController extends AbstractController
{
    /**
     * @Route("api/settings", name="get_settings", methods={"GET"})
     * @OA\Tag(name="System")
     */
    public function getSettingsActions(EntityManagerInterface $entityManager)
    {
        $settings = $entityManager->getRepository(Settings::class)->findAll();
        $response = [];

        /** @var Settings $setting */
        foreach($settings as $setting){
            $response[$setting->getName()] = $setting->getValue();
        }

        return $this->json($response);
    }
}