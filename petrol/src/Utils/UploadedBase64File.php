<?php
namespace App\Utils;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadedBase64File extends UploadedFile
{

    public function __construct(string $base64String, string $originalName, $mimeType = null)
    {
        $filePath = tempnam(sys_get_temp_dir(), 'UploadedFile');
        file_put_contents($filePath, $base64String);
        $error = null;
        $test = true;

        parent::__construct($filePath, $originalName, $mimeType, $error, $test);
    }

}