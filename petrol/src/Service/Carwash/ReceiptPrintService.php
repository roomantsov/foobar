<?php


namespace App\Service\Carwash;


use App\DTO\Carwash\AutoOrderRequest;
use App\DTO\Carwash\AutoOrderResponse;
use App\DTO\Product;
use App\Exception\HttpClientException;
use App\Exception\MPosContentException;
use App\Service\Account\AuthorizationService;
use App\Service\Http\ClientService;
use App\Service\Http\ResponseHelper;
use App\Service\SerializerService;

class ReceiptPrintService
{

    private ResponseHelper $responseHelper;

    private string $sessionId;

    private ClientService $clientService;

    private SerializerService $serializerService;

    private string $gasApiUrl;

    /**
     * ReceiptPrintService constructor.
     * @param AuthorizationService $authorizationService
     * @param SerializerService $serializerService
     * @param ClientService $clientService
     * @param ResponseHelper $responseHelper
     * @param string $gasApiUrl
     * @param string $gasUser
     * @param string $gasPassword
     * @throws HttpClientException
     * @throws MPosContentException
     */
    public function __construct(
        AuthorizationService $authorizationService,
        SerializerService $serializerService,
        ClientService $clientService,
        ResponseHelper $responseHelper,
        string $gasApiUrl,
        string $gasUser,
        string $gasPassword
    )
    {
        $this->sessionId = $authorizationService->serviceAuthorization($gasApiUrl, $gasUser, $gasPassword);
        $this->serializerService = $serializerService;
        $this->clientService = $clientService;
        $this->responseHelper = $responseHelper;
        $this->gasApiUrl = $gasApiUrl;
    }


    /**
     * @param int $terminal
     * @param int $code
     * @param string $transactionId
     * @param Product $product
     * @return AutoOrderResponse
     * @throws HttpClientException
     * @throws MPosContentException
     */
    public function print(int $terminal, int $code, string $transactionId, Product $product): AutoOrderResponse
    {
        $request = new AutoOrderRequest(
            $this->sessionId,
            $terminal,
            $code,
            $transactionId,
            $product->getName(),
            $product->getAmount(),
            $product->getPrice()->getAmount() / 100,
            $product->getOrderAmountWithoutBonuses()->getAmount() / 100,
            $product->getBonusRedeem()->getAmount() / 100
        );

        return $this->sendRequest($request);
    }

    /**
     * @param object $request
     * @return array|object|AutoOrderResponse
     * @throws HttpClientException
     * @throws MPosContentException
     */
    private function sendRequest(object $request): AutoOrderResponse
    {
        $body = $this->serializerService->_toXml($request);

        $content = $this->clientService->sendRequest(ClientService::METHOD_POST, $this->gasApiUrl, $body)
            ->_toObject(AutoOrderResponse::class);

        $this->responseHelper->checkContent($content);

        return $content;

    }
}