<?php


namespace App\Service\Carwash;


use App\DTO\Carwash\SaldoListMposRequest;
use App\DTO\Carwash\SaldoListMposResponse;
use App\Exception\HttpClientException;
use App\Exception\MPosContentException;
use App\Repository\CarwashTypeRepository;
use App\Service\Account\AuthorizationService;
use App\Service\Http\ClientService;
use App\Service\Http\ResponseHelper;
use App\Service\SerializerService;

class ItemDetailsService
{
    private string $sessionId;

    private SerializerService $serializerService;

    private ClientService $clientService;

    private ResponseHelper $responseHelper;

    private string $gasApiUrl;
    /**
     * @var CarwashTypeRepository
     */
    private CarwashTypeRepository $carwashTypeRepository;

    /**
     * ItemDetailsService constructor.
     * @param AuthorizationService $authorizationService
     * @param SerializerService $serializerService
     * @param ClientService $clientService
     * @param ResponseHelper $responseHelper
     * @param CarwashTypeRepository $carwashTypeRepository
     * @param string $gasApiUrl
     * @param string $gasUser
     * @param string $gasPassword
     * @throws HttpClientException
     * @throws MPosContentException
     */
    public function __construct(
        AuthorizationService $authorizationService,
        SerializerService $serializerService,
        ClientService $clientService,
        ResponseHelper $responseHelper,
        CarwashTypeRepository $carwashTypeRepository,
        string $gasApiUrl,
        string $gasUser,
        string $gasPassword
    )
    {
        $this->sessionId = $authorizationService->serviceAuthorization($gasApiUrl, $gasUser, $gasPassword);
        $this->serializerService = $serializerService;
        $this->clientService = $clientService;
        $this->responseHelper = $responseHelper;
        $this->gasApiUrl = $gasApiUrl;
        $this->carwashTypeRepository = $carwashTypeRepository;
    }

    /**
     * @param int $terminal
     * @param int $code
     * @return array|object|SaldoListMposResponse
     * @throws HttpClientException
     * @throws MPosContentException
     */
    public function getItemDetails(int $terminal, int $code): SaldoListMposResponse
    {
        $requestObject = new SaldoListMposRequest($this->sessionId, $terminal, $code);

        return $this->sendRequest($requestObject);
    }

    public function isCarwashProduct(int $productId): bool
    {
        return null !== $this->carwashTypeRepository->findOneBy(['itemCode' => $productId]);
    }

    /**
     * @param object $request
     * @return array|object|SaldoListMposResponse
     * @throws HttpClientException
     * @throws MPosContentException
     */
    private function sendRequest(object $request): SaldoListMposResponse
    {
        $body = $this->serializerService->_toXml($request);

        $content = $this->clientService->sendRequest(ClientService::METHOD_POST, $this->gasApiUrl, $body)
            ->_toObject(SaldoListMposResponse::class)
        ;

        $this->responseHelper->checkContent($content);

        return $content;
    }
}