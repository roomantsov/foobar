<?php


namespace App\Service\Carwash;


use App\DTO\Carwash\OrderRequest;
use App\DTO\Carwash\OrderResponse;
use App\Exception\CarwashOrderException;
use App\Exception\HttpClientException;
use App\Service\Http\ClientService;
use App\Service\SerializerService;

class CarwashOrderService
{
    public const
        CARWASH_ADVANCE_ERROR = 'SEND_ADVANCE_ERROR',
        CARWASH_ERROR = [
            'INVALID_TOKEN'         => 'Ошибка при оплате',
            'INVALID_INPUT_PARAMS'  => 'Ошибка при оплате',
            'NO_ACCESS'             => 'Ошибка при оплате',
            'SEND_ADVANCE_ERROR'    => 'Пожалуйста, повторите попытку на другом терминале',
    ];


    /**
     * @var SerializerService
     */
    private SerializerService $serializerService;

    /**
     * @var ClientService
     */
    private ClientService $clientService;

    private string $carwashApiUrl;

    private string $env;

    private string $carwashApiKey;

    public function __construct(
        SerializerService $serializerService,
        ClientService $clientService,
        string $carwashApiUrl,
        string $carwashApiKey,
        string $env
    ){
        $this->serializerService = $serializerService;
        $this->clientService = $clientService;
        $this->carwashApiUrl = $carwashApiUrl;
        $this->carwashApiKey = $carwashApiKey;
        $this->env = $env;
    }

    /**
     * @param string $id
     * @param string $terminal
     * @param string $dispenser
     * @param int $amount
     * @return array|object
     * @throws HttpClientException
     */
    public function order(string $id, string $terminal, string $dispenser, int $amount)
    {
        $orderRequest = new OrderRequest();
        $orderRequest
            ->setLogin($id)
            ->setTermCode($terminal)
            ->setPostCode($dispenser)
            ->setAdvance($amount)
            ->setApiKey($this->getApiKey())
        ;

       return $this->proceed($orderRequest);
    }

    /**
     * @param int $amount
     * @param string $price
     * @return float
     */
    public function getQuantity(int $amount, string $price): float
    {
        return $amount / $price;
    }

    /**
     * @param OrderResponse $response
     * @return void
     * @throws CarwashOrderException
     */
    public function checkOrderSuccess(OrderResponse $response): void
    {
        if (!$response->isSuccess()) {
            throw new CarwashOrderException($response->getMsg());
        }
    }

    /**
     * @param $object
     * @return array|object|OrderResponse
     * @throws HttpClientException
     */
    private function proceed($object)
    {
        $body = $this->serializerService->_toJson($object);

        return $this->clientService->sendRequest(ClientService::METHOD_POST, $this->carwashApiUrl, $body)
            ->_toObject(OrderResponse::class, 'json');
    }

    /**
     * @return string
     */
    private function getApiKey(): string
    {
        if ($this->env === 'dev') {
            return $this->carwashApiKey;
        }

        return md5($this->carwashApiKey.date('D-M-Y'));
    }
}