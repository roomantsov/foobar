<?php


namespace App\Service;


use Exception;
use Psr\Log\LoggerInterface;
use Throwable;

class LoggerService
{
    private LoggerInterface $smsLogger;

    private LoggerInterface $wayforpayLogger;

    private LoggerInterface $logger;

    private LoggerInterface $mposDebugLogger;

    /**
     * LoggerService constructor.
     * @param LoggerInterface $logger
     * @param LoggerInterface $smsLogger
     * @param LoggerInterface $wayforpayLogger
     * @param LoggerInterface $mposDebugLogger
     */
    public function __construct(
        LoggerInterface $logger,
        LoggerInterface $smsLogger,
        LoggerInterface $wayforpayLogger,
        LoggerInterface $mposDebugLogger
    ) {
        $this->smsLogger = $smsLogger;
        $this->wayforpayLogger = $wayforpayLogger;
        $this->logger = $logger;
        $this->mposDebugLogger = $mposDebugLogger;
    }


    /**
     * @param Exception|Throwable $e
     * @param $phone
     */
    public function logError($e, $phone = null): void
    {
        $this->logger->error($e->getMessage(), [
            'phone' => $phone,
            'code' => $e->getCode(),
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'trace' => $e->getTrace(),
        ]);
    }

    /**
     * @param $responseObject object
     * @param $phone
     * @param $body
     */
    public function LogAcceptSms($responseObject, $phone, $body): void
    {
        $this->smsLogger->info($responseObject->getCode().': '.$responseObject->getDescription(), [
            'phone' => $phone,
            'body' => $body,
            'date' => $responseObject->getDate(),
            'ID' => $responseObject->getCampaignId(),
        ]);
    }

    /**
     * @param Exception $e
     * @param $phone
     */
    public function logWayforpayError(Exception $e, $phone): void
    {
        $this->wayforpayLogger->error($e->getMessage(), [
            'phone' => $phone,
            'code' => $e->getCode(),
            'file' => $e->getFile(),
            'line' => $e->getLine(),
        ]);
    }

    public function debug($string, $context = []): void
    {
        $this->mposDebugLogger->info($string, $context);
    }
}