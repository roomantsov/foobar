<?php


namespace App\Service\Account;


use App\DTO\Discount\DiscountData;
use App\DTO\MPosRequest\GetTransactionDetails;
use App\DTO\MPosRequest\GetTransactions;
use App\DTO\UserData;
use App\Exception\HttpClientException;
use App\Service\Http\ClientService;
use App\Service\SerializerService;

class TransactionsService
{
    /**
     * @var SerializerService
     */
    private $serializerService;
    /**
     * @var ClientService
     */
    private $clientService;
    /**
     * @var string
     */
    private $accountApiUrl;

    public function __construct(SerializerService $serializerService, ClientService $clientService, string $accountApiUrl)
    {

        $this->serializerService = $serializerService;
        $this->clientService = $clientService;
        $this->accountApiUrl = $accountApiUrl;
    }

    /**
     * Returns transactions for last 30 days
     * @param string $sessionId
     * @param $holderId
     * @return array|object|UserData
     * @throws HttpClientException
     */
    public function getTransactions(string $sessionId, $holderId)
    {
        $request = new GetTransactions();
        $request->setHolderId($holderId);
        $body = $this->serializerService->_toXml($request);

        return $this->clientService->sendRequest(ClientService::METHOD_POST, $this->accountApiUrl, $body, $sessionId)
            ->_toObject(UserData::class)
            ->getTransactionsOrderByDesc()
        ;
    }

    /**
     * @param string $sessionId
     * @param $holderId
     * @return DiscountData|array|object
     * @throws HttpClientException
     */
    public function getTransactionDetails(string $sessionId, $holderId, $transactionId)
    {
        $request = new GetTransactionDetails();
        $request
            ->setHolderId($holderId)
            ->setTransactionId($transactionId)
        ;

        $body = $this->serializerService->_toXml($request);

        return $this->clientService->sendRequest(ClientService::METHOD_POST, $this->accountApiUrl, $body, $sessionId)
            ->_toObject(DiscountData::class)
            ->getBonusList()
        ;
    }
}