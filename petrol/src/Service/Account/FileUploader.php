<?php


namespace App\Service\Account;


use App\Service\LoggerService;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    const IMAGE_DIRECTORY = '/uploads/images/';

    /**
     * @var string
     */
    private $filename;

    /**
     * @var string
     */
    private $targetDirectory;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var LoggerService
     */
    private $loggerService;

    /**
     * @param Filesystem $filesystem
     * @param LoggerService $loggerService
     * @param $targetDirectory
     */
    public function __construct(Filesystem $filesystem, LoggerService $loggerService, $targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
        $this->filesystem = $filesystem;
        $this->loggerService = $loggerService;
    }

    /**
     * @param UploadedFile $file
     * @return void
     */
    public function upload(UploadedFile $file)
    {
        if (empty($this->filename)) {
            $this->setFilename($file);
        }

        $file->move($this->getTargetDirectory(), $this->filename);
    }

    /**
     * @return mixed
     */
    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function setFilename(UploadedFile $file)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);

        $this->filename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();

        return $this->filename;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @return void
     */
    public function deleteFile()
    {
        try {
            $this->filesystem->remove($this->getTargetDirectory() . 'FileUploader.php/' . $this->getFilename());
        } catch (IOException $e) {
            $this->loggerService->logError($e);
        }
    }
}