<?php


namespace App\Service\Account;


use App\DTO\MPosRequest\AccountList;
use App\DTO\MPosRequest\ChangeHolderData;
use App\DTO\UserData;
use App\Exception\HttpClientException;
use App\Exception\MPosContentException;
use App\Service\Http\ClientService;
use App\Service\Http\ResponseHelper;
use App\Service\SerializerService;
use Endroid\QrCode\Exception\ValidationException;
use Endroid\QrCode\Factory\QrCodeFactory;

class UserDataService
{
    /**
     * @var AuthorizationService
     */
    private $authorizationService;

    /**
     * @var ClientService
     */
    private $clientService;

    /**
     * @var QrCodeFactory
     */
    private $qrCode;

    /**
     * @var ResponseHelper
     */
    private $responseHelper;

    /**
     * @var SerializerService
     */
    private $serializerService;

    /**
     * @param AuthorizationService $authorizationService
     * @param ClientService $clientService
     * @param QrCodeFactory $qrCode
     * @param ResponseHelper $responseHelper
     * @param SerializerService $serializerService
     * @param ChangeHolderData $changeHolderData
     */
    public function __construct(
        AuthorizationService $authorizationService,
        ClientService $clientService,
        QrCodeFactory $qrCode,
        ResponseHelper $responseHelper,
        SerializerService $serializerService
    ) {
        $this->authorizationService = $authorizationService;
        $this->clientService = $clientService;
        $this->qrCode = $qrCode;
        $this->responseHelper = $responseHelper;
        $this->serializerService = $serializerService;
    }

    /**
     * @param $sessionId
     * @param $data
     * @return array
     * @throws HttpClientException
     * @throws MPosContentException
     */
    public function setUserData($sessionId, $data): array
    {
        $body = $this->serializerService->_toXml(
            (new ChangeHolderData())
                ->setHolderId($data['holderId'])
                ->setHolderData($data['holderData'])
        );

        $content = $this->clientService->sendRequest(ClientService::METHOD_POST, $this->authorizationService->getAccountApiUrl(), $body, $sessionId)->_toObject(UserData::class);

        $this->responseHelper->checkContent($content);

        return [
            'errorCode' => $content->getErrorCode(),
            'errorDescription' => $content->getErrorDescription()
        ];
    }

    /**
     * @param $phone
     * @param $sessionId
     * @return array
     * @throws HttpClientException
     * @throws MPosContentException
     */
    public function getUserData($phone, $sessionId): array
    {
        $content = $this->getAccountInfo($phone, $sessionId);

        return $this->responseHelper->prepareUserData($content);

    }

    /**
     * @param $holderId
     * @param $sessionId
     * @return string
     * @throws MPosContentException
     * @throws HttpClientException
     */
    public function getBonusBalance($holderId, $sessionId): string
    {
        $body = $this->serializerService->_toXml(
            (new AccountList())->setHolderId($holderId)
        );

        $content = $this->clientService->sendRequest(ClientService::METHOD_POST, $this->authorizationService->getAccountApiUrl(), $body, $sessionId)->_toObject(UserData::class);

        $this->responseHelper->checkContent($content);

        return $content->getBonusBalance();
    }

    /**
     * @param $phone
     * @param $sessionId
     * @return object
     * @throws MPosContentException
     * @throws HttpClientException
     */
    public function getAccountInfo($phone, $sessionId)
    {
        $content = $this->authorizationService->login($phone, $sessionId);

        $this->responseHelper->checkContent($content);

       return $content;
    }

    /**
     * @param $accountCard string
     * @return array
     * @throws ValidationException
     */
    public function generateQrCode($accountCard): array
    {
        $Qr = $this->qrCode
            ->create($accountCard)
            ->writeString();

        $smallQr = $this->qrCode
            ->create($accountCard, [
                'size' => 50,
            ])
            ->writeString();

        //fix style conflicts
        $smallQr = str_replace('block', 'block_s', $smallQr);

        return [
            'small_qr' => $smallQr,
            'qr' => $Qr
        ];
    }
}