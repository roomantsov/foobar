<?php


namespace App\Service\Account;


use App\DTO\MPosRequest\Authorization;
use App\DTO\MPosRequest\Login;
use App\DTO\UserData;
use App\Exception\HttpClientException;
use App\Exception\MPosContentException;
use App\Service\Http\ResponseHelper;
use App\Service\Http\ClientService;
use App\Service\SerializerService;
use Exception;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AuthorizationService
{

    public const
        NOT_REGISTERED_USER = 129,
        REGISTRATION_TYPE = 'REGISTRATION',
        REQUIRE_HOLDER_DATA = [
            'FirstName',
            'SecondName',
            'LastName',
            'BirthDate',
            'Email',
            'Mobile',
            'Hobbies',
            'CarTank'
        ]
    ;

    private HttpClientInterface $httpClient;

    private ClientService $clientService;

    private ResponseHelper $responseHelper;

    private SerializerService $serializerService;

    private string $holderId;

    private string $accountApiUrl;

    /**
     * AuthorizationService constructor.
     * @param string $accountApiUrl
     * @param ClientService $clientService
     * @param HttpClientInterface $httpClient
     * @param ResponseHelper $responseHelper
     * @param SerializerService $serializerService
     */
    public function __construct
    (
        string $accountApiUrl,
        ClientService $clientService,
        HttpClientInterface $httpClient,
        ResponseHelper $responseHelper,
        SerializerService $serializerService
    ) {
        $this->clientService = $clientService;
        $this->httpClient = $httpClient;
        $this->responseHelper = $responseHelper;
        $this->serializerService = $serializerService;
        $this->accountApiUrl = $accountApiUrl;
    }

    /**
     * @param string $url
     * @param string $user
     * @param string $password
     * @return string
     * @throws HttpClientException
     * @throws MPosContentException
     */
    public function serviceAuthorization(string $url, string $user, string $password): string
    {
        $body = $this->serializerService->_toXml(
            new Authorization($user, $password)
        );

        $content = $this->clientService->sendRequest(ClientService::METHOD_POST, $url, $body)->_toObject(UserData::class);

        $this->responseHelper->checkContent($content);

         return $content->getSessionId();
    }

    /**
     * @param string $phone
     * @param string $sessionId
     * @return array
     * @throws MPosContentException
     * @throws HttpClientException
     */
    public function userAuthorization(string $phone, string $sessionId): array
    {
        $content = $this->login($phone, $sessionId);
        $checkContent = $this->responseHelper->checkContent($content);

        if ($checkContent === self::NOT_REGISTERED_USER) {
            $content = $this->registration($phone, $sessionId);
            $this->responseHelper->checkContent($content);
        }

        $this->holderId = $content->getHolderId();

        return $this->responseHelper->prepareUserData($content);
    }

    /**
     * @param string $phone
     * @param string $sessionId
     * @return object|UserData
     * @throws HttpClientException
     */
    public function login(string $phone, string $sessionId): UserData
    {
        $body = $this->serializerService->_toXml(
            (new Login())->setMobile($phone)
        );

        return $this->clientService->sendRequest(ClientService::METHOD_POST, $this->accountApiUrl, $body, $sessionId)->_toObject(UserData::class);

    }

    /**
     * @param string $phone
     * @param string $sessionId
     * @return object|UserData
     * @throws HttpClientException
     */
    public function registration(string $phone, string $sessionId): UserData
    {
        $body = $this->serializerService->_toXml(
            (new Login())->setType(self::REGISTRATION_TYPE)->setMobile($phone)
        );

        return $this->clientService->sendRequest(ClientService::METHOD_POST, $this->accountApiUrl, $body, $sessionId)->_toObject(UserData::class);
    }

    /**
     * @param $length
     * @return int
     */
    public function generateAuthCode(int $length): int
    {
        $from = 1;
        $to = 9;
        for ($i = 1; $i < $length; $i++) {
            $from .= 0;
            $to .= 9;
        }

        try {
            return random_int($from, $to);
        } catch (Exception $e) {
            return mt_rand($from, $to);
        }
    }

    /**
     * @return string
     */
    public function getHolderId(): string
    {
        return $this->holderId;
    }

    /**
     * @return string
     */
    public function getAccountApiUrl(): string
    {
        return $this->accountApiUrl;
    }


}