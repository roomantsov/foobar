<?php


namespace App\Service\Publisher;


use App\Service\SerializerService;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Mercure\Update;

class PublisherService
{
    public const STATUS_UPDATE_TARGET = ['ovis_users'];
    /**
     * @var SerializerService
     */
    private $serializerService;
    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;
    /**
     * @var UrlHelper
     */
    private $urlHelper;

    public function __construct(SerializerService $serializerService, ParameterBagInterface $parameterBag, UrlHelper $urlHelper)
    {
        $this->serializerService = $serializerService;
        $this->parameterBag = $parameterBag;
        $this->urlHelper = $urlHelper;
    }

    /**
     * @param $id
     * @param $content
     * @return Update
     */
    public function createStatusUpdate($id, $content): Update
    {
        return new Update(
            $this->generateStatusUpdateTopic($id),
            $this->serializerService->_toJson($content),
            true //private
        );
    }

    /**
     * @param $orderId
     * @return string
     */
    public function generateStatusUpdateTopic($orderId): string
    {
        return 'order/' . $orderId . '/status';
    }

    /**
     * @param $orderId
     * @return string
     */
    public function getStatusUpdateUrl($orderId): string
    {
        return $this->urlHelper->getAbsoluteUrl(
            $this->parameterBag->get('mercure_publish_url') . '?topic=' . $this->generateStatusUpdateTopic($orderId)
        );
    }

    /**
     * @return string
     */
    public function getSubscribeToken(): string
    {
        return (new Builder())
            ->withClaim('mercure', ['subscribe' => self::STATUS_UPDATE_TARGET])
            ->getToken(new Sha256(), new Key($this->parameterBag->get('mercure_secret_key')))
            ->__toString()
        ;
    }
}