<?php


namespace App\Service\Fuel;


use App\DTO\Gas\FuelOrderData;
use App\DTO\MPosRequest\GetFuelOrderStatus;
use App\DTO\MPosRequest\SetFuelOrder;
use App\DTO\Product;
use App\Exception\HttpClientException;
use App\Exception\MPosContentException;
use App\Service\Account\AuthorizationService;
use App\Service\Http\ClientService;
use App\Service\Http\ResponseHelper;
use App\Service\LoggerService;
use App\Service\SerializerService;

class FuelOrderService
{
    public const
        ORDER_IN_PROGRESS = '1',
        ORDER_COMPLETE = '2';


    private $clientService;

    private $serializerService;

    private $responseHelper;

    private $gasApiUrl;

    private $sessionId;

    private $logger;

    /**
     * FuelOrderService constructor.
     * @param ClientService $clientService
     * @param SerializerService $serializerService
     * @param AuthorizationService $authorizationService
     * @param ResponseHelper $responseHelper
     * @param LoggerService $loggerService
     * @param string $gasApiUrl
     * @param string $gasUser
     * @param string $gasPassword
     * @throws HttpClientException
     * @throws MPosContentException
     */
    public function __construct(
        ClientService $clientService,
        SerializerService $serializerService,
        AuthorizationService $authorizationService,
        ResponseHelper $responseHelper,
        LoggerService $loggerService,
        string $gasApiUrl,
        string $gasUser,
        string $gasPassword
    ) {
        $this->clientService = $clientService;
        $this->serializerService = $serializerService;
        $this->responseHelper = $responseHelper;
        $this->gasApiUrl = $gasApiUrl;
        $this->sessionId = $authorizationService->serviceAuthorization($gasApiUrl, $gasUser, $gasPassword);
        $this->logger = $loggerService;
    }

    /**
     * @param string $terminalId
     * @param string $dispenserId
     * @param Product $product
     * @return array|object|FuelOrderData
     * @throws MPosContentException
     * @throws HttpClientException
     */
    public function setFuelOrder(string $terminalId, string $dispenserId, Product $product)
    {
        $fuelOrder = new SetFuelOrder();

        $fuelOrder
            ->setSessionId($this->sessionId)
            ->setTerminalId($terminalId)
            ->setDispenserId($dispenserId)
            ->setFuelId($product->getId())
            ->setOrderSum($product->getActualOrderAmount())
            ->setOrderDiscount($product->getBonusRedeem())
            ->setOrderAmount($product->getAmount())
            ->setInitiatorTransactionId(uniqid('', true))
        ;

        return $this->proceed($fuelOrder);
    }

    /**
     * @param string $terminalId
     * @param string $dispenserId
     * @param $fuelOrderId
     * @return array|object|FuelOrderData
     * @throws MPosContentException
     * @throws HttpClientException
     */
    public function getFuelOrderStatus(string $terminalId, string $dispenserId, $fuelOrderId)
    {
        $fuelOrderStatus = new GetFuelOrderStatus();

        $fuelOrderStatus
            ->setSessionId($this->sessionId)
            ->setTerminalId($terminalId)
            ->setDispenserId($dispenserId)
            ->setOrderId($fuelOrderId)
        ;

        return $this->proceed($fuelOrderStatus);
    }

    /**
     * @param $object
     * @return array|object
     * @throws HttpClientException
     * @throws MPosContentException
     */
    private function proceed($object)
    {
        $body = $this->serializerService->_toXml($object);

        $this->logger->debug($body, ['REQUEST' => __CLASS__]);

        $response = $this->clientService->sendRequest(ClientService::METHOD_POST, $this->gasApiUrl, $body);

        $this->logger->debug($response->_toString(), ['RESPONSE' => __CLASS__]);

        $content = $response->_toObject(FuelOrderData::class)
        ;

        $this->responseHelper->checkContent($content);

        return $content;
    }
}
