<?php


namespace App\Service\Fuel;


use App\DTO\Gas\FuelPriceData;
use App\DTO\MPosRequest\GetFuelPrice;
use App\Exception\HttpClientException;
use App\Exception\MPosContentException;
use App\Service\Account\AuthorizationService;
use App\Service\Http\ClientService;
use App\Service\Http\ResponseHelper;
use App\Service\SerializerService;
use Symfony\Component\HttpFoundation\JsonResponse;

class FuelPriceService
{

    private $clientService;

    private $serializerService;

    private $responseHelper;

    private $gasApiUrl;

    private string $sessionId;

    /**
     * FuelPriceService constructor.
     * @param ClientService $clientService
     * @param SerializerService $serializerService
     * @param AuthorizationService $authorizationService
     * @param ResponseHelper $responseHelper
     * @param string $gasApiUrl
     * @param string $gasUser
     * @param string $gasPassword
     * @throws HttpClientException
     * @throws MPosContentException
     */
    public function __construct(
        ClientService $clientService,
        SerializerService $serializerService,
        AuthorizationService $authorizationService,
        ResponseHelper $responseHelper,
        string $gasApiUrl,
        string $gasUser,
        string $gasPassword
    ) {
        $this->clientService = $clientService;
        $this->serializerService = $serializerService;
        $this->responseHelper = $responseHelper;
        $this->gasApiUrl = $gasApiUrl;
        $this->sessionId = $authorizationService->serviceAuthorization($gasApiUrl, $gasUser, $gasPassword);
    }

    /**
     * @param $terminalId
     * @return array|object
     * @throws MPosContentException
     * @throws HttpClientException
     */
    public function getFuelPrice($terminalId)
    {
        $fuelPrice = new GetFuelPrice();
        $fuelPrice
            ->setTerminalId($terminalId)
            ->setSessionId($this->sessionId);

        $body = $this->serializerService->_toXml($fuelPrice);
        $content = $this->clientService->sendRequest(ClientService::METHOD_POST, $this->gasApiUrl, $body)
            ->_toObject(FuelPriceData::class);

        if ($content->getErrorCode() === JsonResponse::HTTP_UNAUTHORIZED) {
            $content->setErrorCode(JsonResponse::HTTP_NOT_FOUND);
        }

        $this->responseHelper->checkContent($content);

        return $content;
    }
}