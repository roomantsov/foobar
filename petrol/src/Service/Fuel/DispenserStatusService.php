<?php


namespace App\Service\Fuel;


use App\DTO\Gas\DispenserStatusData;
use App\DTO\MPosRequest\GetDispenserStatus;
use App\Exception\HttpClientException;
use App\Exception\MPosContentException;
use App\Service\Account\AuthorizationService;
use App\Service\Http\ClientService;
use App\Service\Http\ResponseHelper;
use App\Service\SerializerService;

class DispenserStatusService
{
    private $clientService;

    private $serializerService;

    private $responseHelper;

    private $gasApiUrl;

    private string $sessionId;

    /**
     * DispenserStatusService constructor.
     * @param ClientService $clientService
     * @param SerializerService $serializerService
     * @param AuthorizationService $authorizationService
     * @param ResponseHelper $responseHelper
     * @param string $gasApiUrl
     * @param string $gasUser
     * @param string $gasPassword
     * @throws HttpClientException
     * @throws MPosContentException
     */
    public function __construct(
        ClientService $clientService,
        SerializerService $serializerService,
        AuthorizationService $authorizationService,
        ResponseHelper $responseHelper,
        string $gasApiUrl,
        string $gasUser,
        string $gasPassword
    ) {
        $this->clientService = $clientService;
        $this->serializerService = $serializerService;
        $this->responseHelper = $responseHelper;
        $this->gasApiUrl = $gasApiUrl;
        $this->sessionId = $authorizationService->serviceAuthorization($gasApiUrl, $gasUser, $gasPassword);
    }

    /**
     * @param string $terminalId
     * @param string $dispenserId
     * @return DispenserStatusData|array|object
     * @throws HttpClientException
     * @throws MPosContentException
     */
    public function getStatus(string $terminalId, string $dispenserId): DispenserStatusData
    {

        $dispenserStatus = new GetDispenserStatus();
        $dispenserStatus
            ->setSessionId($this->sessionId)
            ->setTerminalId($terminalId)
            ->setDispenserId($dispenserId)
        ;

        $body = $this->serializerService->_toXml($dispenserStatus);
        $content = $this->clientService->sendRequest(ClientService::METHOD_POST, $this->gasApiUrl, $body)->_toObject(DispenserStatusData::class);

        $this->responseHelper->checkContent($content);

        return $content;
    }
}