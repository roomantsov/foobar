<?php


namespace App\Service;

use Symfony\Component\Serializer\SerializerInterface;

class SerializerService
{
    private $serializer;

    /**
     * SerializerService constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param $object object
     * @param string $root_tag_name
     * @param bool $format_xml
     * @return string
     */
    public function _toXml($object, $root_tag_name = 'Data', $format_xml = false)
    {
        return $this->serializer->serialize($object, 'xml', [
            'xml_format_output' => $format_xml,
            'xml_root_node_name' => $root_tag_name
        ]);
    }

    /**
     * @param $string
     * @param $objectName
     * @param string $format
     * @param array $context
     * @return array|object
     */
    public function _toObject($string, $objectName, $format, $context = [])
    {
        return $this->serializer->deserialize($string, $objectName, $format, $context);
    }

    public function _toJson($object, $format = 'json', $context = []): string
    {
        return $this->serializer->serialize($object, $format, $context);
    }
}