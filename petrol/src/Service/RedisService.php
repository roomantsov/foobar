<?php


namespace App\Service;


use App\Exception\UserAuthorizationException;
use App\Service\Http\ResponseHelper;
use Predis\ClientInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class RedisService
{
    private $redisClient;

    public function __construct(
        ClientInterface $redisClient

    ) {
        $this->redisClient = $redisClient;
    }

    /**
     * @param $phone
     * @return mixed
     */
    public function getAuthData($phone)
    {
        return json_decode($this->redisClient->get($phone), true);
    }

    /**
     * @param $phone int
     * @param array $authInfo
     * @return void
     */
    public function setAuthData($phone, array $authInfo)
    {
        $this->redisClient->set($phone, json_encode($authInfo));
    }

    /**
     * @param $phone
     * @param $data
     */
    public function addAuthParam(string $phone, array $data)
    {
        $current_data = $this->getAuthData($phone);
        $this->setAuthData($phone, array_merge($current_data, $data));
    }

    /**
     * @param $phone
     * @param $param
     */
    public function unsetAuthParam($phone, $param)
    {
        $data = $this->getAuthData($phone);
        if (isset($data[$param])) {
            unset($data[$param]);
        }
        $this->setAuthData($phone, $data);
    }

    /**
     * @param $phone int
     * @param $code
     * @return bool
     * @throws UserAuthorizationException
     */
    public function checkAuthData($phone, $code): bool
    {
        $userInfo = json_decode($this->redisClient->get($phone), true);
        if (isset($userInfo['code']) && $code === $userInfo['code'] ) {
            return true;
        }

        if (isset($userInfo['code']) && !$this->authCounter($phone, $userInfo)) {
            $this->unsetAuthData($phone);
            throw new UserAuthorizationException(ResponseHelper::TOO_MANY_ATTEMPTS, JsonResponse::HTTP_I_AM_A_TEAPOT);
        }

        throw new UserAuthorizationException(ResponseHelper::NOT_VALID_CODE_ERROR, JsonResponse::HTTP_UNAUTHORIZED);
    }

    /**
     * @param $phone
     */
    public function unsetAuthData($phone)
    {
        $this->redisClient->del($phone);
    }

    /**
     * method counts failed authorizations, if more than 2, returns false
     * @param $phone
     * @param $userInfo array
     * @return bool
     */
    public function authCounter($phone, $userInfo)
    {
        if (!empty($userInfo['authCount']) && $userInfo['authCount'] === 2) {
            return false;
        }

        if (empty($userInfo['authCount'])) {
            $userInfo['authCount'] = 1;
        } else {
            ++$userInfo['authCount'];
        }

        $this->setAuthData($phone, $userInfo);

        return true;
    }
}