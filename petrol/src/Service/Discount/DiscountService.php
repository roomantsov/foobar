<?php


namespace App\Service\Discount;


use App\DTO\Discount\DiscountData;
use App\DTO\MPosRequest\Issuance;
use App\DTO\MPosRequest\Redemption;
use App\DTO\MPosRequest\Refund;
use App\DTO\Product;
use App\Exception\HttpClientException;
use App\Exception\MPosContentException;
use App\Service\Account\AuthorizationService;
use App\Service\Account\UserDataService;
use App\Service\Carwash\ItemDetailsService;
use App\Service\Http\ClientService;
use App\Service\Http\ResponseHelper;
use App\Service\LoggerService;
use App\Service\SerializerService;
use Exception;
use Money\Money;

class DiscountService
{
    private const
        MAX_FUEL_REDEEM_COEFFICIENT = 0.3,
        MAX_CARWASH_REDEEM_COEFFICIENT = 0.99
    ;

    private $clientService;

    private $serializerService;

    private $authorizationService;

    private $responseHelper;

    private $discountApiUrl;

    private $userDataService;

    private LoggerService $loggerService;

    private $minChargeAmount;

    private string $siteUser;

    private string $sitePassword;

    private ItemDetailsService $itemDetailsService;

    private LoggerService $logger;

    public function __construct(
        ClientService $clientService,
        SerializerService $serializerService,
        AuthorizationService $authorizationService,
        ResponseHelper $responseHelper,
        UserDataService $userDataService,
        LoggerService $loggerService,
        ItemDetailsService $itemDetailsService,
        LoggerService $logger,
        string $discountApiUrl,
        string $siteUser,
        string $sitePassword
    ) {
        $this->clientService = $clientService;
        $this->serializerService = $serializerService;
        $this->authorizationService = $authorizationService;
        $this->responseHelper = $responseHelper;
        $this->userDataService = $userDataService;
        $this->loggerService = $loggerService;
        $this->minChargeAmount = Money::UAH(100);
        $this->discountApiUrl = $discountApiUrl;
        $this->siteUser = $siteUser;
        $this->sitePassword = $sitePassword;
        $this->itemDetailsService = $itemDetailsService;
        $this->logger = $logger;
    }

    /**
     * @param string $phone
     * @param string $terminalId
     * @param Product $product
     * @param string $orderId
     * @return DiscountData|null
     */
    public function issuance(string $phone, string $terminalId, Product $product, string $orderId): ?DiscountData
    {
        try {
            $issuance = $this->getIssuanceObject($phone, $terminalId, $product, $orderId);
            return $this->proceed($issuance);
        } catch (Exception $e) {
            $this->loggerService->logError($e, $phone);
            return null;
        }
    }

    /**
     * @param string $phone
     * @param string $terminalId
     * @param Product $product
     * @param string $orderId
     * @return DiscountData|array|object
     * @throws HttpClientException
     * @throws MPosContentException
     */
    public function calculateDiscount(string $phone, string $terminalId, Product $product, string $orderId): DiscountData
    {
        $issuance = $this->getIssuanceObject($phone, $terminalId, $product, $orderId);

        $issuance
            ->setType('GET_DISCOUNT')
            ->setBonusMaxRedeem($product->getOrderAmountWithoutBonuses());

        return $this->proceed($issuance);
    }

    /**
     * @param string $phone
     * @param string $terminalId
     * @param Product $product
     * @param string $orderId
     * @param DiscountData $discountInfo
     * @return array|object|DiscountData
     * @throws HttpClientException
     * @throws MPosContentException
     */
    public function redemption(string $phone, string $terminalId, Product $product, string $orderId, DiscountData $discountInfo): DiscountData
    {
        $sessionId = $this->authorizationService->serviceAuthorization($this->discountApiUrl, $this->siteUser, $this->sitePassword);
        $cardNo = $this->userDataService->getAccountInfo($phone, $sessionId)->getDiscountCard();

        $redemption = new Redemption();
        $redemption
            ->setCardNo($cardNo)
            ->setTerminalId($terminalId)
            ->setTransactionNo($orderId)
            ->setTransactionHash(
                $this->generateTransactionHash($orderId, $redemption->getType())
            )
            ->setTotalAmount($product->getActualOrderAmount())
            ->setBonusRedeem(
                $this->calculateRedeem($discountInfo)
            )
            ->setTotalAmountBeforeDiscount($product->getOrderAmountWithoutBonuses())
            ->setGoodsCodes($product->getId())
            ->setName($product->getName())
            ->setQty($product->getGiveAmount())
            ->setPrice($product->getPrice())
            ->setAmount($product->getOrderAmountWithoutBonuses())
            ->setAmountDiscount($product->getBonusRedeem())
        ;

            return $this->proceed($redemption);
    }

    /**
     * @param string $phone
     * @param string $terminalId
     * @param Product $product
     * @param string $orderId
     * @param string $clmTrnId
     * @param string $transactionDate
     * @return DiscountData|array|object
     * @throws HttpClientException
     * @throws MPosContentException
     */
    public function refund(string $phone, string $terminalId, Product $product, string $orderId, string $clmTrnId, string $transactionDate): DiscountData
    {
        $sessionId = $this->authorizationService->serviceAuthorization($this->discountApiUrl, $this->siteUser, $this->sitePassword);
        $cardNo = $this->userDataService->getAccountInfo($phone, $sessionId)->getDiscountCard();

        $refund = new Refund();
        $refund
            ->setCardNo($cardNo)
            ->setTerminalId($terminalId)
            ->setTransactionNo($orderId)
            ->setTransactionHash(
                $this->generateTransactionHash($orderId, $refund->getType())
            )
            ->setTotalAmount($product->getActualOrderAmount())
            ->setGoodsCodes($product->getId())
            ->setName($product->getName())
            ->setQty($product->getGiveAmount())
            ->setPrice($product->getPrice())
            ->setAmount($product->getOrderAmountWithoutBonuses())
            ->setAmountDiscount($product->getBonusRedeem())
            ->setOriginalTransactionDate($transactionDate)
            ->setOriginalTransactionNo($orderId)
            ->setOriginalTerminalID($terminalId)
            ->setOriginalClmTrnId($clmTrnId)
        ;

        return $this->proceed($refund);
    }

    /**
     * @param Money $orderAmount
     * @param int $productId
     * @return Money
     */
    private function calculateMaxRedeem(Money $orderAmount, int $productId): Money
    {
        // todo: remove this method
        if ($this->itemDetailsService->isCarwashProduct($productId)) {
            return $orderAmount->subtract($orderAmount->multiply(self::MAX_CARWASH_REDEEM_COEFFICIENT))->greaterThanOrEqual($this->minChargeAmount) ?
                $orderAmount->multiply(self::MAX_CARWASH_REDEEM_COEFFICIENT) :
                $orderAmount->subtract($this->minChargeAmount);
        }

        return $orderAmount->multiply(self::MAX_FUEL_REDEEM_COEFFICIENT);
    }

    /**
     * @param $orderId
     * @param $type
     * @return string
     */
    private function generateTransactionHash($orderId, $type): string
    {
        return md5('transaction' . $orderId . $type);
    }

    /**
     * @param DiscountData $discountInfo
     * @return Money
     */
    private function calculateRedeem(DiscountData $discountInfo): Money
    {
        if ($discountInfo->getBalanceActive()->subtract($discountInfo->getBonusRedeem())->isNegative()) {
            return $discountInfo->getBalanceActive();
        }

        return $discountInfo->getBonusRedeem();
    }

    /**
     * @param string $phone
     * @param string $terminalId
     * @param Product $product
     * @param string $orderId
     * @return Issuance
     * @throws HttpClientException
     * @throws MPosContentException
     */
    private function getIssuanceObject(string $phone, string $terminalId, Product $product, string $orderId): Issuance
    {
        $sessionId = $this->authorizationService->serviceAuthorization($this->discountApiUrl, $this->siteUser, $this->sitePassword);
        $cardNo = $this->userDataService->getAccountInfo($phone, $sessionId)->getDiscountCard();

        $issuance = new Issuance();
        $issuance
            ->setCardNo($cardNo)
            ->setTerminalId($terminalId)
            ->setTransactionNo($orderId)
            ->setTransactionHash(
                $this->generateTransactionHash($orderId, $issuance->getType())
            )
            ->setTotalAmount($product->getActualOrderAmount())
            ->setTotalAmountBeforeDiscount($product->getOrderAmountWithoutBonuses())
            ->setGoodsCodes($product->getId())
            ->setName($product->getName())
            ->setQty($product->getGiveAmount())
            ->setPrice($product->getPrice())
            ->setAmount($product->getOrderAmountWithoutBonuses())
            ->setAmountDiscount($product->getBonusRedeem())
        ;

        return $issuance;
    }

    /**
     * @param $proceedData
     * @return DiscountData|array|object
     * @throws HttpClientException
     * @throws MPosContentException
     */
    private function proceed($proceedData): DiscountData
    {
        $sessionId = $this->authorizationService->serviceAuthorization($this->discountApiUrl, $this->siteUser, $this->sitePassword);
        $body = $this->serializerService->_toXml($proceedData);

        $this->logger->debug($body, ['REQUEST' => __CLASS__]);

        /** @var DiscountData $content */
        $response = $this->clientService->sendRequest(ClientService::METHOD_POST, $this->discountApiUrl, $body, $sessionId);

        $this->logger->debug($response->_toString(), ['RESPONSE' => __CLASS__]);

        $content = $response->_toObject(DiscountData::class);

        $this->responseHelper->checkContent($content);

        if ($proceedData instanceof Redemption) {
            $bonusList['GoodsData'] = $content->getBonusList();
            $bonusList['GoodsData']['transactionDate'] = $proceedData->getTransactionDate();
            $content->setBonusList($bonusList);
        }

        return $content;
    }
}