<?php


namespace App\Service\Wayforpay;


use App\DTO\Product;
use App\DTO\Wayforpay\ChargeRequest;
use App\DTO\Wayforpay\PaymentResponse;
use App\DTO\Wayforpay\RefundRequest;
use App\DTO\Wayforpay\SettleRequest;
use App\Exception\HttpClientException;
use App\Service\Http\ClientService;
use App\Service\LoggerService;
use App\Service\SerializerService;
use Money\Money;

class ChargeService extends Wayforpay
{
    private $clientService;

    private $serializerService;

    private LoggerService $logger;

    public function __construct(
        ClientService $clientService,
        SerializerService $serializerService,
        LoggerService $logger
    ){
        $this->clientService = $clientService;
        $this->serializerService = $serializerService;
        $this->logger = $logger;
    }

    /**
     * @param string $phone
     * @param string $creditCardToken
     * @param Product $product
     * @param string $orderUuid
     * @return array|object|PaymentResponse
     * @throws HttpClientException
     */
    public function hold(string $phone, string $creditCardToken, Product $product, string $orderUuid): PaymentResponse
    {
        $user = $this->getUser($phone);

        $chargeRequest = new ChargeRequest();
        $chargeRequest
            ->setMerchantTransactionType(self::TRANSACTION_AUTH)
            ->setAmount($product->getEstimatedOrderAmount())
            ->setRecToken($creditCardToken)
            ->addProductName($product->getName())
            ->addProductPrice($product->getPrice())
            //round here because wayforpay expect integer in Product count, but there are also can be float
            ->addProductCount(round($product->getAmount()))
            ->setClientPhone($phone)
            ->setClientFirstName($user->getFirstName())
            ->setClientLastName($user->getLastName())
            ->setClientEmail($user->getEmail())
            ->setMerchantAccount($this->getMerchantAccount())
            ->setOrderReference($orderUuid)
            ->setMerchantSignature($this->generateSignature([
                $this->getMerchantAccount(),
                self::DOMAIN_NAME,
                $orderUuid,
                $chargeRequest->getOrderDate(),
                $chargeRequest->getAmount(),
                $chargeRequest->getCurrency(),
                $chargeRequest->getProductName(),
                $chargeRequest->getProductCount(),
                $chargeRequest->getProductPrice(),
            ]))
        ;

        return $this->proceed($chargeRequest);
    }

    /**
     * for future one-step charges without holding, currently not used
     * @param string $phone
     * @param string $creditCardToken
     * @param Product $product
     * @return array|object|PaymentResponse
     * @throws HttpClientException
     */
    public function charge(string $phone, string $creditCardToken, Product $product): PaymentResponse
    {
        $uuid = $this->generateUuid();
        $user = $this->getUser($phone);

        $chargeRequest = new ChargeRequest();
        $chargeRequest
            ->setMerchantTransactionType(self::TRANSACTION_SALE)
            ->setAmount($product->getActualOrderAmount())
            ->setRecToken($creditCardToken)
            ->addProductName($product->getName())
            ->addProductPrice($product->getPrice())
            //round here because wayforpay expect integer in Product count, but there are also can be float
            ->addProductCount(round($product->getGiveAmount()))
            ->setClientPhone($phone)
            ->setClientFirstName($user->getFirstName())
            ->setClientLastName($user->getLastName())
            ->setClientEmail($user->getEmail())
            ->setMerchantAccount($this->getMerchantAccount())
            ->setOrderReference($uuid)
            ->setMerchantSignature($this->generateSignature([
                $this->getMerchantAccount(),
                self::DOMAIN_NAME,
                $uuid,
                $chargeRequest->getOrderDate(),
                $chargeRequest->getAmount(),
                $chargeRequest->getCurrency(),
                $chargeRequest->getProductName(),
                $chargeRequest->getProductCount(),
                $chargeRequest->getProductPrice(),
            ]))
        ;

        return $this->proceed($chargeRequest);
    }

    /**
     * @param Money $amount
     * @param string $orderReference
     * @return array|object|PaymentResponse
     * @throws HttpClientException
     */
    public function confirmHold(Money $amount, string $orderReference): PaymentResponse
    {
        $settleRequest = new SettleRequest();
        $settleRequest
            ->setMerchantAccount($this->getMerchantAccount())
            ->setOrderReference($orderReference)
            ->setAmount($amount)
            ->setMerchantSignature($this->generateSignature([
                $this->getMerchantAccount(),
                $orderReference,
                $settleRequest->getAmount(),
                self::CURRENCY
            ]))
        ;

        return $this->proceed($settleRequest);
    }

    /**
     * @param Money $amount
     * @param string $orderReference
     * @return array|object|PaymentResponse
     * @throws HttpClientException
     */
    public function refund(Money $amount, string $orderReference, $comment = 'Recalculation'): PaymentResponse
    {
        $refundRequest = new RefundRequest();
        $refundRequest
            ->setAmount($amount)
            ->setOrderReference($orderReference)
            ->setMerchantAccount($this->getMerchantAccount())
            ->setComment($comment)
            ->setMerchantSignature($this->generateSignature([
                $this->getMerchantAccount(),
                $orderReference,
                $refundRequest->getAmount(),
                self::CURRENCY
            ]))
        ;

        return $this->proceed($refundRequest);
    }

    /**
     * @param object $chargeRequest
     * @return array|object|PaymentResponse
     * @throws HttpClientException
     */
    private function proceed(object $chargeRequest): PaymentResponse
    {
        $body = $this->serializerService->_toJson($chargeRequest);

        $this->logger->debug($body, ['REQUEST' => __CLASS__]);

        $response = $this->clientService->sendRequest(ClientService::METHOD_POST, self::WAYFORPAY_API_URL, $body);

        $this->logger->debug($response->_toString(), ['RESPONSE' => __CLASS__]);

        return $response->_toObject(PaymentResponse::class, 'json')
        ;
    }

    /**
     * @param Product $product
     * @return Money
     */
    public function getRefund(Product $product): Money
    {
        return $product->getEstimatedOrderAmount()->subtract($product->getActualOrderAmount());
    }

}
