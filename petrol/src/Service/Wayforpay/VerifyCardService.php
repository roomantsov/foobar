<?php

namespace App\Service\Wayforpay;

use App\DTO\Wayforpay\VerifyDataForPurchaseWidget;

class VerifyCardService extends Wayforpay
{
    /**
     * @param string $phone
     * @return VerifyDataForPurchaseWidget
     */
    public function verifyWithoutConfirm(string $phone): VerifyDataForPurchaseWidget
    {
        $uuid = $this->generateUuid();
        $date = time();
        $user = $this->getUser($phone);

        $verify = new VerifyDataForPurchaseWidget();
        $verify
            ->setMerchantAccount($this->getMerchantAccount())
            ->setOrderReference($uuid)
            ->setMerchantSignature($this->generateSignature([
                $this->getMerchantAccount(),
                self::DOMAIN_NAME,
                $uuid,
                $date,
                self::VERIFY_AMOUNT,
                self::CURRENCY,
                self::PRODUCT_NAME,
                self::PRODUCT_COUNT,
                self::PRODUCT_PRICE
            ]))
            ->setOrderDate($date)
            ->setClientEmail($user->getEmail())
            ->setClientFirstName($user->getFirstName())
            ->setClientLastName($user->getLastName())
            ->setClientPhone($phone)
            ->setServiceUrl($this->getCallbackUrl())
        ;

        return $verify;
    }
}
