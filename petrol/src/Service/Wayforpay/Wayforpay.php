<?php


namespace App\Service\Wayforpay;


use App\DTO\UserData;
use App\Exception\WayforpayException;
use App\Service\Account\AuthorizationService;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

abstract class Wayforpay
{
    private const FIELDS_DELIMITER = ';';

    protected const
        WAYFORPAY_API_URL = 'https://api.wayforpay.com/api',
        TRANSACTION_AUTH = 'AUTH',
        TRANSACTION_SALE = 'SALE'
    ;

    public const
        DOMAIN_NAME = 'https://ovis.ua',
        VERIFY_AMOUNT = '1',
        CURRENCY = 'UAH',
        WAYFORPAY_STATUS_OK = 1100,
        WAYFORPAY_RESPONSE_ACCEPT = 'accept',
        CARD_NOT_FOUND = 'Wrong credit card ID',
        WAYFORPAY_ERROR = 'Не удалось произвести оплату. Свяжитесь с вашим банком или воспользуйтесь другой картой.',
        PRODUCT_NAME = 'Верификация банковской карты',
        PRODUCT_PRICE = '1',
        PRODUCT_COUNT = '1',
        WIDGET_THEME = 'ovis_ua_bind'
    ;

    private UrlGeneratorInterface $router;

    private AuthorizationService $authorizationService;

    private string $merchantAccount;

    private string $merchantSecretKey;

    private string $siteUser;

    private string $sitePassword;

    private string $accauntApiUrl;

    /**
     * @param UrlGeneratorInterface $router
     * @param AuthorizationService $authorizationService
     * @param string $merchantAccount
     * @param string $merchantSecretKey
     * @param string $accountApiUrl
     * @param string $siteUser
     * @param string $sitePassword
     */
    public function setDependencies(
        UrlGeneratorInterface $router,
        AuthorizationService $authorizationService,
        string $merchantAccount,
        string $merchantSecretKey,
        string $accountApiUrl,
        string $siteUser,
        string $sitePassword
    ){
        $this->router = $router;
        $this->authorizationService = $authorizationService;
        $this->merchantAccount = $merchantAccount;
        $this->merchantSecretKey = $merchantSecretKey;
        $this->accauntApiUrl = $accountApiUrl;
        $this->siteUser = $siteUser;
        $this->sitePassword = $sitePassword;
    }

    public function generateSignature(array $fields): string
    {
        $data = [];

        foreach ($fields as $value) {
            if (is_array($value)) {
                $data[] = implode(self::FIELDS_DELIMITER, $value);
            } else {
                $data[] = (string) $value;
            }
        }

        return hash_hmac('md5', implode(self::FIELDS_DELIMITER, $data), $this->merchantSecretKey);
    }

    /**
     * @return string
     */
    protected function getMerchantAccount(): string
    {
        return $this->merchantAccount;
    }

    /**
     * @param object $object
     * @throws WayforpayException
     */
    public function checkWayforpayStatus($object): void
    {
        if ($object->getReasonCode() !== self::WAYFORPAY_STATUS_OK) {
            throw new WayforpayException($object->getTransactionStatus(), $object->getReason(), $object->getPhone(), $object->getReasonCode());
        }
    }

    protected function getCallbackUrl(): string
    {
        return $this->router->generate('card_verify_callback', [], UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * @return string
     */
    public function generateUuid(): string
    {
        try {
            return Uuid::uuid4()->toString();
        } catch (Exception $e) {
            return uniqid('', true);
        }
    }

    /**
     * @param string $phone
     * @return UserData
     */
    protected function getUser(string $phone): UserData
    {
        try {
            $sessionId = $this->authorizationService->serviceAuthorization($this->accauntApiUrl, $this->siteUser, $this->sitePassword);
            return $this->authorizationService->login($phone, $sessionId);
        } catch (Exception $e) {
            $user = new UserData();
            return $user
                ->setFirstName($phone)
                ->setLastName($phone)
                ->setEmail($phone . '@example.com')
            ;
        }
    }
}
