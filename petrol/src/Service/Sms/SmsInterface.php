<?php


namespace App\Service\Sms;


interface SmsInterface
{

    /**
     * @param string $to
     * @param string $body
     * @return void
     */
    public function send(string $to, string $body);
}