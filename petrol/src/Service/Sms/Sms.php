<?php


namespace App\Service\Sms;


use App\DTO\Sms\SmsData;
use App\DTO\Sms\SmsRequestObject;
use App\Exception\SmsException;
use App\Service\Http\ClientService;
use App\Service\LoggerService;
use App\Service\SerializerService;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;

class Sms implements SmsInterface
{
    private const SMS_URL_NO_AI = 'http://sms-fly.com/api/api.noai.php';

    private const SMS_URL_AI = 'http://sms-fly.com/api/api.php';

    private const SMS_ACCEPT = 'ACCEPT';
    /**
     * @var ClientService
     */
    private $clientService;
    /**
     * @var LoggerService
     */
    private $loggerService;
    /**
     * @var SerializerService
     */
    private $serializerService;
    /**
     * @var string
     */
    private $user;
    /**
     * @var string
     */
    private $password;

    private string $alfaName;

    /**
     * Sms-smsService constructor.
     * @param ClientService $clientService
     * @param LoggerService $loggerService
     * @param SerializerService $serializerService
     * @param string $sms_user
     * @param string $sms_password
     * @param string $alfaName
     */
    public function __construct(
        ClientService $clientService,
        LoggerService $loggerService,
        SerializerService $serializerService,
        string $sms_user,
        string $sms_password,
        string $alfaName
    )
    {
        $this->clientService = $clientService;
        $this->loggerService = $loggerService;
        $this->serializerService = $serializerService;
        $this->user = $sms_user;
        $this->password = $sms_password;
        $this->alfaName = $alfaName;
    }


    /**
     * @param string $to
     * @param string $body
     * @throws SmsException
     * @throws Exception
     */
    public function send(string $to, string $body): void
    {
        $requestObject = $this->setRequestObject($to, $body);

        $xml = $this->serializerService->_toXml($requestObject, 'request');
        //send sms message
        $response = $this->clientService->sendRequest(ClientService::METHOD_POST, $this->getApiUrl($requestObject), $xml, false, $this->getAuthCredentials());

        try {
            //here we accept service message only im XML format, it's not a guarantee delivery success, so we need to check status and log it
            $message_status = $response->_toObject(SmsData::class);
        } catch (Exception $e) {
            //goes here, if catch any message not in XML format
            $error_message = !empty($response->_toString()) ? 'SMS service Error: '. $response->_toString() : $e->getMessage();
            //throw exact error message (it will be logged in controller)
            throw new SmsException($error_message , JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        //checking sms status
        if ($message_status->getCode() === self::SMS_ACCEPT) {
            $this->loggerService->LogAcceptSms($message_status, $to, $body);
        } else {
             throw new SmsException($message_status->getCode().': '.$message_status->getDescription(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param string $phone
     * @param string $body
     * @return SmsRequestObject
     */
    private function setRequestObject(string $phone, string $body): SmsRequestObject
    {
       return (new SmsRequestObject($this->alfaName))
            ->setRecipient($phone)
            ->setBody($body)
        ;
    }

    /**
     * @return string
     */
    private function getAuthCredentials(): string
    {
        return "$this->user:$this->password";
    }

    /**
     * @param $requestObject SmsRequestObject
     * @return string
     */
    private function getApiUrl (SmsRequestObject $requestObject): string
    {
        return $requestObject->isAlfaName() ? self::SMS_URL_AI : self::SMS_URL_NO_AI;
    }

}