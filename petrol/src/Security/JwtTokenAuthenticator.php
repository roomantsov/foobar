<?php


namespace App\Security;


use App\Repository\SettingsRepository;
use App\Service\Http\ResponseHelper;
use App\Service\LoggerService;
use App\Service\RedisService;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class JwtTokenAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var JWTEncoderInterface
     */
    private $jwtEncoder;
    /**
     * @var RedisService
     */
    private $redisService;
    /**
     * @var LoggerService
     */
    private $loggerService;
    /**
     * @var SettingsRepository
     */
    private SettingsRepository $settingsRepo;

    public function __construct(
        JWTEncoderInterface $jwtEncoder,
        RedisService $redisService,
        LoggerService $loggerService,
        SettingsRepository $settingsRepo
    ){
        $this->jwtEncoder = $jwtEncoder;
        $this->redisService = $redisService;
        $this->loggerService = $loggerService;
        $this->settingsRepo = $settingsRepo;
    }

    /**
     * @inheritDoc
     */
    public function supports(Request $request)
    {
        return true;
    }

    /**
     * @param Request $request
     * @return array|false
     * @throws JWTDecodeFailureException
     */
    public function getCredentials(Request $request)
    {
        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
        );

        $token = $extractor->extract($request);
        if (!$token) {
            throw new BadCredentialsException(ResponseHelper::BAD_TOKEN);
        }

        return $this->jwtEncoder->decode($token);
    }

    /**
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     * @return UserInterface|void|null
     */
    public function getUser($credentials, UserProviderInterface $userProvider): ?UserInterface
    {
        $user = $userProvider->loadUserByUsername($credentials['username']);
        $user->setId($credentials['id']);

        return $user;
    }

    /**
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool|void
     */
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        $required_version = $this->settingsRepo->findOneBy(['name' => 'required_application_version'])->getValue();

        if (!isset($credentials['version']) || version_compare($credentials['version'], $required_version, '>=') ) {
            return true;
        }

        throw new BadCredentialsException(sprintf('%s %s.*', ResponseHelper::USER_AUTH_ERROR, $required_version));
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return JsonResponse|null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new JsonResponse([
            'errorCode' => Response::HTTP_UNAUTHORIZED,
            'errorDescription' => $exception->getMessage()
        ], Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     * @return null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    /**
     * @return bool|void
     */
    public function supportsRememberMe()
    {
        return null;
    }

    /**
     * @param Request $request
     * @param AuthenticationException|null $authException
     * @return JsonResponse
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new jsonResponse(ResponseHelper::USER_AUTH_ERROR, JsonResponse::HTTP_UNAUTHORIZED);
    }
}
