<?php


namespace App\Messenger;


use App\Service\Fuel\FuelOrderService;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Retry\RetryStrategyInterface;
use Symfony\Component\Messenger\Stamp\RedeliveryStamp;

class RetryStrategy implements RetryStrategyInterface
{
    private const MAX_RETRIES = 200;

    public function isRetryable(Envelope $message): bool
    {
        if ($message->getMessage()->getOrderStatus() === FuelOrderService::ORDER_COMPLETE) {
            return false;
        }

        if ($message->getMessage()->getOrderStatus() === FuelOrderService::ORDER_IN_PROGRESS) {
            return true;
        }

        $retries = RedeliveryStamp::getRetryCountFromEnvelope($message);

        return $retries <= self::MAX_RETRIES;
    }

    /**
     * @inheritDoc
     */
    public function getWaitingTime(Envelope $message): int
    {
        $delayTime = 1000; //1 sec

        if ($message->getMessage()->getOrderStatus() !== FuelOrderService::ORDER_IN_PROGRESS) {
            $delayTime = 5000; //5 sec
        }

        return $delayTime;
    }
}