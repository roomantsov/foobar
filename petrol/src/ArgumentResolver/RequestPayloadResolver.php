<?php
namespace App\ArgumentResolver;

use App\DTO\Request\RequestObject;
use App\Exception\PayloadValidationException;
use App\Service\SerializerService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestPayloadResolver implements ArgumentValueResolverInterface
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var SerializerService
     */
    private $serializerService;

    public function __construct(
        SerializerService $serializerService,
        ValidatorInterface $validator
    ) {
        $this->serializerService = $serializerService;
        $this->validator = $validator;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return is_subclass_of($argument->getType(), RequestObject::class);
    }

    /**
     * @inheritDoc
     * @throws PayloadValidationException
     */
    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $dto = $this->serializerService->_toObject($request->getContent(), $argument->getType(), 'json');

        $this->validateDTO($dto);

        yield $dto;
    }

    /**
     * @param $dto
     * @throws PayloadValidationException
     */
    public function validateDTO($dto): void
    {
        $errors = $this->validator->validate($dto);
        if ($errors->count() > 0) {
            throw new PayloadValidationException($errors);
        }
    }
}
