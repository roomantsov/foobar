<?php


namespace App\Mercure;


use App\Service\Publisher\PublisherService;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

final class JwtProvider
{
    /**
     * @var ParameterBagInterface
     */
    private $parameter;

    public function __construct(ParameterBagInterface $parameter)
    {
        $this->parameter = $parameter;
    }

    /**
     * @return string
     */
    public function __invoke(): string
    {
        return (new Builder())
            ->withClaim('mercure', ['publish' => PublisherService::STATUS_UPDATE_TARGET])
            ->getToken(new Sha256(), new Key($this->parameter->get('mercure_secret_key')))->__toString()
        ;
    }
}