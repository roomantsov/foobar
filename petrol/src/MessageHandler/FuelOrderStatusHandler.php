<?php


namespace App\MessageHandler;


use App\Exception\FuelOrderInProgress;
use App\Exception\HttpClientException;
use App\Exception\MPosContentException;
use App\DTO\MoneyModifier;
use App\Message\FuelOrderStatus;
use App\Service\Fuel\FuelOrderService;
use App\Service\Http\ClientService;
use App\Service\LoggerService;
use App\Service\SerializerService;
use Exception;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class FuelOrderStatusHandler implements MessageHandlerInterface
{
    /**
     * @var FuelOrderService
     */
    private $fuelOrderService;
    /**
     * @var ClientService
     */
    private $clientService;
    /**
     * @var SerializerService
     */
    private $serializerService;
    /**
     * @var LoggerService
     */
    private $loggerService;

    public function __construct(
        FuelOrderService $fuelOrderService,
        ClientService $clientService,
        SerializerService $serializerService,
        LoggerService $loggerService
    ){
        $this->fuelOrderService = $fuelOrderService;
        $this->clientService = $clientService;
        $this->serializerService = $serializerService;
        $this->loggerService = $loggerService;
    }

    /**
     * @param FuelOrderStatus $orderStatus
     * @throws HttpClientException
     * @throws MPosContentException
     * @throws FuelOrderInProgress
     * @throws Exception|TransportExceptionInterface
     */
    public function __invoke(FuelOrderStatus $orderStatus)
    {
        $status = $this->fuelOrderService->getFuelOrderStatus($orderStatus->getTerminal(), $orderStatus->getDispenser(), $orderStatus->getOrderId());
        $orderStatus->setOrderStatus($status->getStatus());

        if ($status->getStatus() !== FuelOrderService::ORDER_COMPLETE){
            throw new FuelOrderInProgress();
        }
        
        $orderStatus->setDiscountSum($status->getBonusRedeem()->getAmount());
        $orderStatus->setGiveAmount($status->getGiveAmount());

        try {
            //variable needs for non lazy request, do not remove it! TODO figure out how to avoid this variable
            $code = $this->clientService->sendRequest(ClientService::METHOD_POST, $orderStatus->getSuccessUrl(), $this->getJson($orderStatus))
                ->getResponse()->getStatusCode();
        } catch (Exception $e) {
            $orderStatus->setOrderStatus(false);
            throw new FuelOrderInProgress($e->getMessage());
        }
    }

    /**
     * @param $orderStatus
     * @return string
     */
    public function getJson(FuelOrderStatus $orderStatus): string
    {
        $moneySchemaModifier = static function ($innerObject) {
            return new MoneyModifier(
                $innerObject->getAmount(),
                $innerObject->getCurrency()->getCode()
            );
        };

        return $this->serializerService->_toJson($orderStatus, 'json', [
            AbstractNormalizer::CALLBACKS => [
                'price' => $moneySchemaModifier,
                'bonusRedeem' => $moneySchemaModifier,
                'estimatedOrderAmount' => $moneySchemaModifier,
                'actualOrderAmount' => $moneySchemaModifier,
                'orderAmountWithoutBonuses' => $moneySchemaModifier,
                'refundAmount' => $moneySchemaModifier,
            ],
        ]);
    }
}