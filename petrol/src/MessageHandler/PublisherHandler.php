<?php


namespace App\MessageHandler;


use App\Message\PublisherMessage;
use App\Service\LoggerService;
use App\Service\Publisher\PublisherService;
use Exception;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Messenger\Exception\UnrecoverableMessageHandlingException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class PublisherHandler implements MessageHandlerInterface
{
    /**
     * @var PublisherInterface
     */
    private $publisher;
    /**
     * @var PublisherService
     */
    private $publisherService;
    /**
     * @var LoggerService
     */
    private $loggerService;

    public function __construct(PublisherInterface $publisher, PublisherService $publisherService, LoggerService $loggerService)
    {
        $this->publisher = $publisher;
        $this->publisherService = $publisherService;
        $this->loggerService = $loggerService;
    }

    /**
     * @param PublisherMessage $message
     */
    public function __invoke(PublisherMessage $message)
    {
        try {
            $update = $this->publisherService->createStatusUpdate($message->getOrderId(), $message->getContent());
            ($this->publisher)($update);
        } catch (Exception $e) {
            $this->loggerService->logError($e);
            throw new UnrecoverableMessageHandlingException();
        }
    }
}