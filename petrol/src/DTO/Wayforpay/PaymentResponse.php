<?php


namespace App\DTO\Wayforpay;


class PaymentResponse
{
    /**
     * @var string
     */
    private $merchantAccount;

    /**
     * @var string
     */
    private $merchantSignature;

    /**
     * @var string
     */
    private $orderReference;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var string
     */
    private $authCode;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var int|null
     */
    private $createdDate;

    /**
     * @var int|null
     */
    private $processingDate;

    /**
     * @var string
     */
    private $cardPan;

    /**
     * @var string|null
     */
    private $cardType;

    /**
     * @var string|null
     */
    private $issuerBankCountry;

    /**
     * @var string|null
     */
    private $issuerBankName;

    /**
     * @var string
     */
    private $recToken;

    /**
     * @var string
     */
    private $transactionStatus;

    /**
     * @var string
     */
    private $reason;

    /**
     * @var int
     */
    private $reasonCode;

    /**
     * @var float
     */
    private $fee;

    /**
     * @var string
     */
    private $paymentSystem;

    /**
     * @return string
     */
    public function getMerchantAccount(): ?string
    {
        return $this->merchantAccount;
    }

    /**
     * @param string|null $merchantAccount
     * @return PaymentResponse
     */
    public function setMerchantAccount(?string $merchantAccount): PaymentResponse
    {
        $this->merchantAccount = $merchantAccount;
        return $this;
    }

    /**
     * @return string
     */
    public function getMerchantSignature(): ?string
    {
        return $this->merchantSignature;
    }

    /**
     * @param string|null $merchantSignature
     * @return PaymentResponse
     */
    public function setMerchantSignature(?string $merchantSignature): PaymentResponse
    {
        $this->merchantSignature = $merchantSignature;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderReference(): ?string
    {
        return $this->orderReference;
    }

    /**
     * @param string|null $orderReference
     * @return PaymentResponse
     */
    public function setOrderReference(?string $orderReference): PaymentResponse
    {
        $this->orderReference = $orderReference;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }

    /**
     * @param float|null $amount
     * @return PaymentResponse
     */
    public function setAmount(?float $amount): PaymentResponse
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     * @return PaymentResponse
     */
    public function setCurrency(?string $currency): PaymentResponse
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthCode(): ?string
    {
        return $this->authCode;
    }

    /**
     * @param string|null $authCode
     * @return PaymentResponse
     */
    public function setAuthCode(?string $authCode): PaymentResponse
    {
        $this->authCode = $authCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return PaymentResponse
     */
    public function setEmail(?string $email): PaymentResponse
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return PaymentResponse
     */
    public function setPhone(?string $phone): PaymentResponse
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedDate(): ?int
    {
        return $this->createdDate;
    }

    /**
     * @param int|null $createdDate
     * @return PaymentResponse
     */
    public function setCreatedDate(?int $createdDate): PaymentResponse
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return int
     */
    public function getProcessingDate(): ?int
    {
        return $this->processingDate;
    }

    /**
     * @param int|null $processingDate
     * @return PaymentResponse
     */
    public function setProcessingDate(?int $processingDate): PaymentResponse
    {
        $this->processingDate = $processingDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCardPan(): ?string
    {
        return $this->cardPan;
    }

    /**
     * @param string|null $cardPan
     * @return PaymentResponse
     */
    public function setCardPan(?string $cardPan): PaymentResponse
    {
        $this->cardPan = $cardPan;
        return $this;
    }

    /**
     * @return string
     */
    public function getCardType(): ?string
    {
        return $this->cardType;
    }

    /**
     * @param string|null $cardType
     * @return PaymentResponse
     */
    public function setCardType(?string $cardType): PaymentResponse
    {
        $this->cardType = $cardType;
        return $this;
    }

    /**
     * @return string
     */
    public function getIssuerBankCountry(): ?string
    {
        return $this->issuerBankCountry;
    }

    /**
     * @param string|null $issuerBankCountry
     * @return PaymentResponse
     */
    public function setIssuerBankCountry(?string $issuerBankCountry): PaymentResponse
    {
        $this->issuerBankCountry = $issuerBankCountry;
        return $this;
    }

    /**
     * @return string
     */
    public function getIssuerBankName(): ?string
    {
        return $this->issuerBankName;
    }

    /**
     * @param string|null $issuerBankName
     * @return PaymentResponse
     */
    public function setIssuerBankName(?string $issuerBankName): PaymentResponse
    {
        $this->issuerBankName = $issuerBankName;
        return $this;
    }

    /**
     * @return string
     */
    public function getRecToken(): ?string
    {
        return $this->recToken;
    }

    /**
     * @param string|null $recToken
     * @return PaymentResponse
     */
    public function setRecToken(?string $recToken): PaymentResponse
    {
        $this->recToken = $recToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getTransactionStatus(): ?string
    {
        return $this->transactionStatus;
    }

    /**
     * @param string|null $transactionStatus
     * @return PaymentResponse
     */
    public function setTransactionStatus(?string $transactionStatus): PaymentResponse
    {
        $this->transactionStatus = $transactionStatus;
        return $this;
    }

    /**
     * @return string
     */
    public function getReason(): ?string
    {
        return $this->reason;
    }

    /**
     * @param string|null $reason
     * @return PaymentResponse
     */
    public function setReason(?string $reason): PaymentResponse
    {
        $this->reason = $reason;
        return $this;
    }

    /**
     * @return int
     */
    public function getReasonCode(): ?int
    {
        return $this->reasonCode;
    }

    /**
     * @param int|null $reasonCode
     * @return PaymentResponse
     */
    public function setReasonCode(?int $reasonCode): PaymentResponse
    {
        $this->reasonCode = $reasonCode;
        return $this;
    }

    /**
     * @return float
     */
    public function getFee(): ?float
    {
        return $this->fee;
    }

    /**
     * @param float|null $fee
     * @return PaymentResponse
     */
    public function setFee(?float $fee): PaymentResponse
    {
        $this->fee = $fee;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentSystem(): ?string
    {
        return $this->paymentSystem;
    }

    /**
     * @param string|null $paymentSystem
     * @return PaymentResponse
     */
    public function setPaymentSystem(?string $paymentSystem): PaymentResponse
    {
        $this->paymentSystem = $paymentSystem;
        return $this;
    }
}