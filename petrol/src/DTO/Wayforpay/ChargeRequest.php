<?php


namespace App\DTO\Wayforpay;


use App\Service\Wayforpay\Wayforpay;
use DateTime;
use Money\Currencies\ISOCurrencies;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Money;

class ChargeRequest
{
    private $transactionType = 'CHARGE';

    private $merchantAccount;

    private $merchantAuthType = 'SimpleSignature';

    private $merchantDomainName = Wayforpay::DOMAIN_NAME;

    private $merchantTransactionType;

    private $merchantTransactionSecureType = 'NON3DS';

    private $merchantSignature;

    private $apiVersion = 1;

    private $serviceUrl;

    private $orderReference;

    private $orderDate;

    private $amount;

    private $currency = Wayforpay::CURRENCY;

    private $recToken;

    private $productName = [];

    private $productPrice = [];

    private $productCount = [];

    private $clientFirstName;

    private $clientLastName;

    private $clientEmail;

    private $clientPhone;

    private $clientCountry = 'UA';

    public function __construct()
    {
        $time = new DateTime();
        $this->orderDate = $time->getTimestamp();
    }

    /**
     * @return string
     */
    public function getTransactionType(): string
    {
        return $this->transactionType;
    }

    /**
     * @param string $transactionType
     * @return ChargeRequest
     */
    public function setTransactionType(string $transactionType): ChargeRequest
    {
        $this->transactionType = $transactionType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMerchantAccount()
    {
        return $this->merchantAccount;
    }

    /**
     * @param mixed $merchantAccount
     * @return ChargeRequest
     */
    public function setMerchantAccount($merchantAccount)
    {
        $this->merchantAccount = $merchantAccount;
        return $this;
    }

    /**
     * @return string
     */
    public function getMerchantAuthType(): string
    {
        return $this->merchantAuthType;
    }

    /**
     * @param string $merchantAuthType
     * @return ChargeRequest
     */
    public function setMerchantAuthType(string $merchantAuthType): ChargeRequest
    {
        $this->merchantAuthType = $merchantAuthType;
        return $this;
    }

    /**
     * @return string
     */
    public function getMerchantDomainName(): string
    {
        return $this->merchantDomainName;
    }

    /**
     * @param string $merchantDomainName
     * @return ChargeRequest
     */
    public function setMerchantDomainName(string $merchantDomainName): ChargeRequest
    {
        $this->merchantDomainName = $merchantDomainName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMerchantTransactionType()
    {
        return $this->merchantTransactionType;
    }

    /**
     * @param mixed $merchantTransactionType
     * @return ChargeRequest
     */
    public function setMerchantTransactionType($merchantTransactionType)
    {
        $this->merchantTransactionType = $merchantTransactionType;
        return $this;
    }

    /**
     * @return string
     */
    public function getMerchantTransactionSecureType(): string
    {
        return $this->merchantTransactionSecureType;
    }

    /**
     * @param string $merchantTransactionSecureType
     * @return ChargeRequest
     */
    public function setMerchantTransactionSecureType(string $merchantTransactionSecureType): ChargeRequest
    {
        $this->merchantTransactionSecureType = $merchantTransactionSecureType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMerchantSignature()
    {
        return $this->merchantSignature;
    }

    /**
     * @param mixed $merchantSignature
     * @return ChargeRequest
     */
    public function setMerchantSignature($merchantSignature)
    {
        $this->merchantSignature = $merchantSignature;
        return $this;
    }

    /**
     * @return int
     */
    public function getApiVersion(): int
    {
        return $this->apiVersion;
    }

    /**
     * @param int $apiVersion
     * @return ChargeRequest
     */
    public function setApiVersion(int $apiVersion): ChargeRequest
    {
        $this->apiVersion = $apiVersion;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getServiceUrl()
    {
        return $this->serviceUrl;
    }

    /**
     * @param mixed $serviceUrl
     * @return ChargeRequest
     */
    public function setServiceUrl($serviceUrl)
    {
        $this->serviceUrl = $serviceUrl;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderReference()
    {
        return $this->orderReference;
    }

    /**
     * @param mixed $orderReference
     * @return ChargeRequest
     */
    public function setOrderReference($orderReference)
    {
        $this->orderReference = $orderReference;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * @param mixed $orderDate
     * @return ChargeRequest
     */
    public function setOrderDate($orderDate)
    {
        $this->orderDate = $orderDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @param Money $amount
     * @return ChargeRequest
     */
    public function setAmount(Money $amount)
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $this->amount = $moneyFormatter->format($amount);
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return ChargeRequest
     */
    public function setCurrency(string $currency): ChargeRequest
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRecToken()
    {
        return $this->recToken;
    }

    /**
     * @param mixed $recToken
     * @return ChargeRequest
     */
    public function setRecToken($recToken)
    {
        $this->recToken = $recToken;
        return $this;
    }

    /**
     * @return array
     */
    public function getProductName(): array
    {
        return $this->productName;
    }

    /**
     * @param $productName
     * @return ChargeRequest
     */
    public function addProductName($productName): self
    {
        $this->productName[] = $productName;
        return $this;
    }

    /**
     * @return array
     */
    public function getProductPrice(): array
    {
        return $this->productPrice;
    }

    /**
     * @param Money $productPrice
     * @return ChargeRequest
     */
    public function addProductPrice(Money $productPrice): self
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $this->productPrice[] = $moneyFormatter->format($productPrice);
        return $this;
    }

    /**
     * @return array
     */
    public function getProductCount(): array
    {
        return $this->productCount;
    }

    /**
     * @param $productCount
     * @return ChargeRequest
     */
    public function addProductCount($productCount): self
    {
        $this->productCount[] = $productCount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientFirstName()
    {
        return $this->clientFirstName;
    }

    /**
     * @param mixed $clientFirstName
     * @return ChargeRequest
     */
    public function setClientFirstName($clientFirstName)
    {
        $this->clientFirstName = $clientFirstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientLastName()
    {
        return $this->clientLastName;
    }

    /**
     * @param mixed $clientLastName
     * @return ChargeRequest
     */
    public function setClientLastName($clientLastName)
    {
        $this->clientLastName = $clientLastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientEmail()
    {
        return $this->clientEmail;
    }

    /**
     * @param mixed $clientEmail
     * @return ChargeRequest
     */
    public function setClientEmail($clientEmail)
    {
        $this->clientEmail = $clientEmail;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientPhone()
    {
        return $this->clientPhone;
    }

    /**
     * @param mixed $clientPhone
     * @return ChargeRequest
     */
    public function setClientPhone($clientPhone)
    {
        $this->clientPhone = $clientPhone;
        return $this;
    }

    /**
     * @return string
     */
    public function getClientCountry(): string
    {
        return $this->clientCountry;
    }

    /**
     * @param string $clientCountry
     * @return ChargeRequest
     */
    public function setClientCountry(string $clientCountry): ChargeRequest
    {
        $this->clientCountry = $clientCountry;
        return $this;
    }


}