<?php


namespace App\DTO\Wayforpay;


use DateTime;

class CallbackResponse
{
    private $orderReference;

    private $status;

    private $time;

    private $signature;

    public function __construct()
    {
        $time = new DateTime();
        $this->time = $time->getTimestamp();
    }

    /**
     * @return mixed
     */
    public function getOrderReference()
    {
        return $this->orderReference;
    }

    /**
     * @param mixed $orderReference
     * @return CallbackResponse
     */
    public function setOrderReference($orderReference)
    {
        $this->orderReference = $orderReference;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return CallbackResponse
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     * @return CallbackResponse
     */
    public function setTime($time)
    {
        $this->time = $time;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @param mixed $signature
     * @return CallbackResponse
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;
        return $this;
    }
}