<?php


namespace App\DTO\Wayforpay;


use App\Service\Wayforpay\Wayforpay;
use Money\Currencies\ISOCurrencies;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Money;

class RefundRequest
{
    private $transactionType = 'REFUND';

    private $merchantAccount;

    private $orderReference;

    private $merchantSignature;

    private $apiVersion = 1;

    private $amount;

    private $comment;

    private $currency = Wayforpay::CURRENCY;

    /**
     * @return string
     */
    public function getTransactionType(): string
    {
        return $this->transactionType;
    }

    /**
     * @param string $transactionType
     * @return RefundRequest
     */
    public function setTransactionType(string $transactionType): RefundRequest
    {
        $this->transactionType = $transactionType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMerchantAccount()
    {
        return $this->merchantAccount;
    }

    /**
     * @param mixed $merchantAccount
     * @return RefundRequest
     */
    public function setMerchantAccount($merchantAccount)
    {
        $this->merchantAccount = $merchantAccount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderReference()
    {
        return $this->orderReference;
    }

    /**
     * @param mixed $orderReference
     * @return RefundRequest
     */
    public function setOrderReference($orderReference)
    {
        $this->orderReference = $orderReference;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMerchantSignature()
    {
        return $this->merchantSignature;
    }

    /**
     * @param mixed $merchantSignature
     * @return RefundRequest
     */
    public function setMerchantSignature($merchantSignature)
    {
        $this->merchantSignature = $merchantSignature;
        return $this;
    }

    /**
     * @return int
     */
    public function getApiVersion(): int
    {
        return $this->apiVersion;
    }

    /**
     * @param int $apiVersion
     * @return RefundRequest
     */
    public function setApiVersion(int $apiVersion): RefundRequest
    {
        $this->apiVersion = $apiVersion;
        return $this;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @param Money $amount
     * @return RefundRequest
     */
    public function setAmount(Money $amount)
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $this->amount = $moneyFormatter->format($amount);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     * @return RefundRequest
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return RefundRequest
     */
    public function setCurrency(string $currency): RefundRequest
    {
        $this->currency = $currency;
        return $this;
    }
}