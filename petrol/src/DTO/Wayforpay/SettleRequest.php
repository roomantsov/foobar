<?php


namespace App\DTO\Wayforpay;


use App\Service\Wayforpay\Wayforpay;
use Money\Currencies\ISOCurrencies;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Money;

class SettleRequest
{
    /**
     * @var string
     */
    private $transactionType = 'SETTLE';

    /**
     * @var string
     */
    private $merchantAccount;

    /**
     * @var string
     */
    private $orderReference;

    /**
     * @var string
     */
    private $amount;

    /**
     * @var string
     */
    private $currency = Wayforpay::CURRENCY;

    /**
     * @var string
     */
    private $merchantSignature;

    /**
     * @var int
     */
    private $apiVersion = 1;

    /**
     * @return string
     */
    public function getTransactionType(): string
    {
        return $this->transactionType;
    }

    /**
     * @param string $transactionType
     * @return SettleRequest
     */
    public function setTransactionType(string $transactionType): SettleRequest
    {
        $this->transactionType = $transactionType;
        return $this;
    }

    /**
     * @return string
     */
    public function getMerchantAccount(): string
    {
        return $this->merchantAccount;
    }

    /**
     * @param string $merchantAccount
     * @return SettleRequest
     */
    public function setMerchantAccount(string $merchantAccount): SettleRequest
    {
        $this->merchantAccount = $merchantAccount;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderReference(): string
    {
        return $this->orderReference;
    }

    /**
     * @param string $orderReference
     * @return SettleRequest
     */
    public function setOrderReference(string $orderReference): SettleRequest
    {
        $this->orderReference = $orderReference;
        return $this;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @param Money $amount
     * @return SettleRequest
     */
    public function setAmount(Money $amount): SettleRequest
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $this->amount = $moneyFormatter->format($amount);
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return SettleRequest
     */
    public function setCurrency(string $currency): SettleRequest
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getMerchantSignature(): string
    {
        return $this->merchantSignature;
    }

    /**
     * @param string $merchantSignature
     * @return SettleRequest
     */
    public function setMerchantSignature(string $merchantSignature): SettleRequest
    {
        $this->merchantSignature = $merchantSignature;
        return $this;
    }

    /**
     * @return int
     */
    public function getApiVersion(): int
    {
        return $this->apiVersion;
    }

    /**
     * @param int $apiVersion
     * @return SettleRequest
     */
    public function setApiVersion(int $apiVersion): SettleRequest
    {
        $this->apiVersion = $apiVersion;
        return $this;
    }



}