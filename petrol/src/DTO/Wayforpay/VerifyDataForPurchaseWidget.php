<?php

namespace App\DTO\Wayforpay;

use App\Service\Wayforpay\Wayforpay;

class VerifyDataForPurchaseWidget
{
    private $merchantAccount;

    private $merchantDomainName = Wayforpay::DOMAIN_NAME;

    private $authorizationType = 'SimpleSignature';

    private $merchantSignature;

    private $orderReference;

    private int $orderDate;

    private string $amount = Wayforpay::VERIFY_AMOUNT;

    private $currency = Wayforpay::CURRENCY;

    private $productName = Wayforpay::PRODUCT_NAME;

    private $productPrice = Wayforpay::PRODUCT_PRICE;

    private $clientPhone;

    private $productCount = Wayforpay::PRODUCT_COUNT;

    private string $clientFirstName;

    private string $clientLastName;

    private $clientEmail;

    private $language = 'UA';

    private $serviceUrl;

    private $merchantTransactionSecureType = 'NON3DS';

    private string $theme = Wayforpay::WIDGET_THEME;

    private bool $straightWidget = true;

    /**
     * @return mixed
     */
    public function getMerchantAccount()
    {
        return $this->merchantAccount;
    }

    /**
     * @param mixed $merchantAccount
     * @return VerifyDataForPurchaseWidget
     */
    public function setMerchantAccount($merchantAccount)
    {
        $this->merchantAccount = $merchantAccount;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthorizationType(): string
    {
        return $this->authorizationType;
    }

    /**
     * @param string $authorizationType
     * @return VerifyDataForPurchaseWidget
     */
    public function setAuthorizationType(string $authorizationType): VerifyDataForPurchaseWidget
    {
        $this->authorizationType = $authorizationType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMerchantDomainName()
    {
        return $this->merchantDomainName;
    }

    /**
     * @param mixed $merchantDomainName
     * @return VerifyDataForPurchaseWidget
     */
    public function setMerchantDomainName($merchantDomainName)
    {
        $this->merchantDomainName = $merchantDomainName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMerchantSignature()
    {
        return $this->merchantSignature;
    }

    /**
     * @param mixed $merchantSignature
     * @return VerifyDataForPurchaseWidget
     */
    public function setMerchantSignature($merchantSignature)
    {
        $this->merchantSignature = $merchantSignature;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderReference()
    {
        return $this->orderReference;
    }

    /**
     * @param mixed $orderReference
     * @return VerifyDataForPurchaseWidget
     */
    public function setOrderReference($orderReference)
    {
        $this->orderReference = $orderReference;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     * @return VerifyDataForPurchaseWidget
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     * @return VerifyDataForPurchaseWidget
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getServiceUrl()
    {
        return $this->serviceUrl;
    }

    /**
     * @param mixed $serviceUrl
     * @return VerifyDataForPurchaseWidget
     */
    public function setServiceUrl($serviceUrl)
    {
        $this->serviceUrl = $serviceUrl;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientPhone()
    {
        return $this->clientPhone;
    }

    /**
     * @param mixed $clientPhone
     * @return VerifyDataForPurchaseWidget
     */
    public function setClientPhone($clientPhone)
    {
        $this->clientPhone = $clientPhone;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrderDate(): int
    {
        return $this->orderDate;
    }

    /**
     * @param int $orderDate
     * @return VerifyDataForPurchaseWidget
     */
    public function setOrderDate(int $orderDate): VerifyDataForPurchaseWidget
    {
        $this->orderDate = $orderDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getProductName(): string
    {
        return $this->productName;
    }

    /**
     * @param string $productName
     * @return VerifyDataForPurchaseWidget
     */
    public function setProductName(string $productName): VerifyDataForPurchaseWidget
    {
        $this->productName = $productName;
        return $this;
    }

    /**
     * @return string
     */
    public function getProductPrice(): string
    {
        return $this->productPrice;
    }

    /**
     * @param string $productPrice
     * @return VerifyDataForPurchaseWidget
     */
    public function setProductPrice(string $productPrice): VerifyDataForPurchaseWidget
    {
        $this->productPrice = $productPrice;
        return $this;
    }

    /**
     * @return string
     */
    public function getProductCount(): string
    {
        return $this->productCount;
    }

    /**
     * @param string $productCount
     * @return VerifyDataForPurchaseWidget
     */
    public function setProductCount(string $productCount): VerifyDataForPurchaseWidget
    {
        $this->productCount = $productCount;
        return $this;
    }

    /**
     * @return string
     */
    public function getClientFirstName(): string
    {
        return $this->clientFirstName;
    }

    /**
     * @param string $clientFirstName
     * @return VerifyDataForPurchaseWidget
     */
    public function setClientFirstName(string $clientFirstName): VerifyDataForPurchaseWidget
    {
        $this->clientFirstName = $clientFirstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getClientLastName(): string
    {
        return $this->clientLastName;
    }

    /**
     * @param string $clientLastName
     * @return VerifyDataForPurchaseWidget
     */
    public function setClientLastName(string $clientLastName): VerifyDataForPurchaseWidget
    {
        $this->clientLastName = $clientLastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientEmail()
    {
        return $this->clientEmail;
    }

    /**
     * @param mixed $clientEmail
     * @return VerifyDataForPurchaseWidget
     */
    public function setClientEmail($clientEmail)
    {
        $this->clientEmail = $clientEmail;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return VerifyDataForPurchaseWidget
     */
    public function setLanguage(string $language): VerifyDataForPurchaseWidget
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string
     */
    public function getTheme(): string
    {
        return $this->theme;
    }

    /**
     * @param string $theme
     * @return VerifyDataForPurchaseWidget
     */
    public function setTheme(string $theme): VerifyDataForPurchaseWidget
    {
        $this->theme = $theme;
        return $this;
    }

    /**
     * @return string
     */
    public function getMerchantTransactionSecureType(): string
    {
        return $this->merchantTransactionSecureType;
    }

    /**
     * @param string $merchantTransactionSecureType
     * @return VerifyDataForPurchaseWidget
     */
    public function setMerchantTransactionSecureType(string $merchantTransactionSecureType): VerifyDataForPurchaseWidget
    {
        $this->merchantTransactionSecureType = $merchantTransactionSecureType;
        return $this;
    }

    /**
     * @return bool
     */
    public function isStraightWidget(): bool
    {
        return $this->straightWidget;
    }

    /**
     * @param bool $straightWidget
     * @return VerifyDataForPurchaseWidget
     */
    public function setStraightWidget(bool $straightWidget): VerifyDataForPurchaseWidget
    {
        $this->straightWidget = $straightWidget;
        return $this;
    }
}
