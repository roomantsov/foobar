<?php


namespace App\DTO\Request;


use Symfony\Component\Validator\Constraints as Assert;

class CarwashOrderRequestDTO implements RequestObject
{
    /**
     * @var integer
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     * @Assert\GreaterThanOrEqual(12)
     */
    private $amount;

    /**
     * @var boolean
     * @Assert\Type("boolean")
     */
    private $discount;

    /**
     * @var int|null
     * @Assert\Type("numeric")
     */
    private $discountSum;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $cardId;


    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     * @return CarwashOrderRequestDTO
     */
    public function setAmount(string $amount): CarwashOrderRequestDTO
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDiscount(): bool
    {
        return $this->discount;
    }

    /**
     * @param bool $discount
     * @return CarwashOrderRequestDTO
     */
    public function setDiscount(bool $discount): CarwashOrderRequestDTO
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * @return integer|null
     */
    public function getDiscountSum(): ?int
    {
        return $this->discountSum;
    }

    /**
     * @param int|null $discountSum
     * @return CarwashOrderRequestDTO
     */
    public function setDiscountSum(?int $discountSum): CarwashOrderRequestDTO
    {
        $this->discountSum = $discountSum;
        return $this;
    }

    /**
     * @return string
     */
    public function getCardId(): string
    {
        return $this->cardId;
    }

    /**
     * @param string $cardId
     * @return CarwashOrderRequestDTO
     */
    public function setCardId(string $cardId): CarwashOrderRequestDTO
    {
        $this->cardId = $cardId;
        return $this;
    }
}
