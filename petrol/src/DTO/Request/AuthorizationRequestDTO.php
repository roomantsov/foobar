<?php


namespace App\DTO\Request;


use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as SWG;

class AuthorizationRequestDTO implements RequestObject
{
    /**
     * @var string
     * @SWG\Property(description="Phone number, only ukrainian numbers", example="0682421643")
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     * @Assert\Length(min = 10, max = 10)
     */
    private $phone;

    /**
     * @var int
     * @SWG\Property(description="Code from sms", example=51515)
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     * @Assert\Length(min = 5, max = 5)
     */
    private $code;

    /**
     * @var string
     * @SWG\Property(description="application's version")
     * @Assert\NotBlank()
     */
    private $version;

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return AuthorizationRequestDTO
     */
    public function setPhone(string $phone): AuthorizationRequestDTO
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return AuthorizationRequestDTO
     */
    public function setCode(int $code): AuthorizationRequestDTO
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @param string $version
     * @return AuthorizationRequestDTO
     */
    public function setVersion(string $version): AuthorizationRequestDTO
    {
        $this->version = $version;
        return $this;
    }
}
