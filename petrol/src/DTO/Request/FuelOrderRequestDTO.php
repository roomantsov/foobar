<?php


namespace App\DTO\Request;


use Symfony\Component\Validator\Constraints as Assert;

class FuelOrderRequestDTO implements RequestObject
{
    /**
     * @var int|string
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     */
    private $fuelAmount;

    /**
     * @var boolean
     * @Assert\Type("boolean")
     */
    private $discount;

    /**
     * @var int|null
     * @Assert\Type("numeric")
     */
    private $discountSum;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $cardId;


    /**
     * @return int
     */
    public function getFuelAmount(): int
    {
        return $this->fuelAmount;
    }

    /**
     * @param int|string $fuelAmount
     * @return FuelOrderRequestDTO
     */
    public function setFuelAmount($fuelAmount): FuelOrderRequestDTO
    {
        $this->fuelAmount = intval($fuelAmount);
        return $this;
    }

    /**
     * @return bool
     */
    public function isDiscount(): bool
    {
        return $this->discount;
    }

    /**
     * @param bool $discount
     * @return FuelOrderRequestDTO
     */
    public function setDiscount(bool $discount): FuelOrderRequestDTO
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDiscountSum(): ?int
    {
        return $this->discountSum;
    }

    /**
     * @param int|null $discountSum
     * @return FuelOrderRequestDTO
     */
    public function setDiscountSum(?int $discountSum): FuelOrderRequestDTO
    {
        $this->discountSum = $discountSum;
        return $this;
    }

    /**
     * @return string
     */
    public function getCardId(): string
    {
        return $this->cardId;
    }

    /**
     * @param string $cardId
     * @return FuelOrderRequestDTO
     */
    public function setCardId(string $cardId): FuelOrderRequestDTO
    {
        $this->cardId = $cardId;
        return $this;
    }
}
