<?php


namespace App\DTO\Request;


use Symfony\Component\Validator\Constraints as Assert;

class ReviewRequestDTO implements RequestObject
{
    /**
     * @var integer
     * @Assert\NotBlank()
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @var integer
     * @Assert\Choice({1, 2, 3, 4, 5})
     */
    private $rate;

    /**
     * @var string
     * @Assert\NotBlank(normalizer="trim")
     */
    private $review;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ReviewRequestDTO
     */
    public function setId(int $id): ReviewRequestDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return ReviewRequestDTO
     */
    public function setUsername(string $username): ReviewRequestDTO
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return int
     */
    public function getRate(): int
    {
        return $this->rate;
    }

    /**
     * @param int $rate
     * @return ReviewRequestDTO
     */
    public function setRate(int $rate): ReviewRequestDTO
    {
        $this->rate = $rate;
        return $this;
    }

    /**
     * @return string
     */
    public function getReview(): string
    {
        return $this->review;
    }

    /**
     * @param string $review
     * @return ReviewRequestDTO
     */
    public function setReview(string $review): ReviewRequestDTO
    {
        $this->review = $review;
        return $this;
    }


}