<?php


namespace App\DTO\Request;



use App\Service\Account\AuthorizationService;
use App\Utils\UploadedBase64File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as SWG;

class UserRequestDTO implements RequestObject
{
    public const IMAGE_FIELD = 'Hobbies';

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Choice(choices=AuthorizationService::REQUIRE_HOLDER_DATA)
     * @Assert\Type("string")
     * @SWG\Property(type="string")
     * @Groups({"default"})
     */
    private $key;

    /**
     * @var string
     * @Assert\NotBlank(normalizer="trim")
     * @Assert\Type("string")
     * @SWG\Property(type="string")
     * @Groups({"default"})
     */
    private $value;


    private $image;

    /**
     * @Assert\Image(maxSize = "15M")
     */
    private $decodedImage;

    /**
     * @return string
     */
    public function getKey(): ?string
    {
        return $this->key;
    }

    /**
     * @param string $key
     * @return UserRequestDTO
     */
    public function setKey(string $key): UserRequestDTO
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return UserRequestDTO
     */
    public function setValue(string $value): UserRequestDTO
    {
        if ($this->key == self::IMAGE_FIELD) {
            $this->setImage($value);
        }

        $this->value = $value;
        return $this;
    }

    /**
     * @param string $image
     * @return UserRequestDTO
     */
    public function setImage(?string $image): self
    {
        $this->image = $image;
        $base64image = base64_decode($this->extractBase64String() ?? $image);
        $this->decodedImage = new UploadedBase64File($base64image, 'user_image');

        return $this;
    }

    /**
     * @return UploadedBase64File|null
     */
    public function getDecodedImage(): ?UploadedBase64File
    {
        return $this->decodedImage;
    }

    private function extractBase64String()
    {
        $data = explode( ';base64,', $this->image);
        return $data[1] ?? null;
    }
}