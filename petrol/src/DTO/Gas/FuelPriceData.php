<?php


namespace App\DTO\Gas;


use Symfony\Component\Serializer\Annotation\SerializedName;

class FuelPriceData
{
    private $errorCode;

    private $errorDescription;

    /**
     * @SerializedName("fuel")
     */
    private $fuels;

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    /**
     * @param string $errorCode
     * @return FuelPriceData
     */
    public function setErrorCode(string $errorCode): self
    {
        $this->errorCode = $errorCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getErrorDescription(): string
    {
        return $this->errorDescription;
    }

    /**
     * @param string $errorDescription
     * @return FuelPriceData
     */
    public function setErrorDescription(string $errorDescription): self
    {
        $this->errorDescription = $errorDescription;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getFuels()
    {
        return $this->fuels['Fuel'];
    }

    /**
     * @param mixed $fuels
     * @return FuelPriceData
     */
    public function setFuels($fuels): self
    {
        $this->fuels = $fuels;

        return $this;
    }


}