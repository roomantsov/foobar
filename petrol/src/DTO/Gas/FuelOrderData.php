<?php


namespace App\DTO\Gas;


use App\Service\Wayforpay\Wayforpay;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Money;
use Money\Parser\DecimalMoneyParser;

class FuelOrderData
{
    private $errorCode;

    private $errorDescription;

    private $status;

    private $statusName;

    private $fuelId;

    private $orderAmount;

    private $giveAmount;

    private $orderId;

    private $locTransactionId;

    private $orderSum;

    private $discount;

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    /**
     * @param string $errorCode
     * @return FuelOrderData
     */
    public function setErrorCode(string $errorCode): self
    {
        $this->errorCode = $errorCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getErrorDescription(): string
    {
        return $this->errorDescription;
    }

    /**
     * @param string $errorDescription
     * @return FuelOrderData
     */
    public function setErrorDescription(string $errorDescription): self
    {
        $this->errorDescription = $errorDescription;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return FuelOrderData
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatusName()
    {
        return $this->statusName;
    }

    /**
     * @param mixed $statusName
     * @return FuelOrderData
     */
    public function setStatusName($statusName): self
    {
        $this->statusName = $statusName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFuelId()
    {
        return $this->fuelId;
    }

    /**
     * @param mixed $fuelId
     * @return FuelOrderData
     */
    public function setFuelId($fuelId): self
    {
        $this->fuelId = $fuelId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderAmount()
    {
        return $this->orderAmount;
    }

    /**
     * @param mixed $orderAmount
     * @return FuelOrderData
     */
    public function setOrderAmount($orderAmount): self
    {
        $this->orderAmount = $orderAmount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     * @return FuelOrderData
     */
    public function setOrderId($orderId): self
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGiveAmount()
    {
        return $this->giveAmount;
    }

    /**
     * @param mixed $giveAmount
     * @return FuelOrderData
     */
    public function setGiveAmount($giveAmount): self
    {
        $this->giveAmount = $giveAmount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocTransactionId()
    {
        return $this->locTransactionId;
    }

    /**
     * @param mixed $locTransactionId
     * @return FuelOrderData
     */
    public function setLocTransactionId($locTransactionId): self
    {
        $this->locTransactionId = $locTransactionId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderSum()
    {
        return $this->orderSum;
    }

    /**
     * @param mixed $orderSum
     * @return FuelOrderData
     */
    public function setOrderSum($orderSum): self
    {
        $this->orderSum = $orderSum;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiscount(){
        return $this->discount;
    }

    /**
     * @param mixed $discount
     * @return FuelOrderData
     */
    public function setDiscount($discount): self
    {
        $this->discount = $discount;
        return $this;
    }

    public function getBonusRedeem(): Money
    {
        $currencies = new ISOCurrencies();
        $moneyParser = new DecimalMoneyParser($currencies);

        return $moneyParser->parse((string)$this->getDiscount(), new Currency(Wayforpay::CURRENCY));
    }
}