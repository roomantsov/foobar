<?php


namespace App\DTO\Gas;


use App\Entity\CardToken;
use App\Service\Wayforpay\Wayforpay;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Money;
use Money\Parser\DecimalMoneyParser;
use Symfony\Component\Serializer\Annotation\Groups;

class DispenserStatusData
{
    /**
     * @var string
     * @Groups({"dispenser_status"})
     */
    private $errorCode;

    /**
     * @var string
     * @Groups({"dispenser_status"})
     */
    private $errorDescription;

    /**
     * @var string
     * @Groups({"dispenser_status"})
     */
    private $nozzleId;

    /**
     * @var int
     * @Groups({"dispenser_status"})
     */
    private $fuelId;

    /**
     * @var string
     * @Groups({"dispenser_status"})
     */
    private $fuelName;

    /**
     * @var Money
     * @Groups({"dispenser_status"})
     */
    private $fuelPrice;

    /**
     * @var CardToken[]
     * @Groups({"dispenser_status"})
     */
    private $tokens;


    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    /**
     * @param string $errorCode
     * @return DispenserStatusData
     */
    public function setErrorCode(string $errorCode): self
    {
        $this->errorCode = $errorCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getErrorDescription(): string
    {
        return $this->errorDescription;
    }

    /**
     * @param string $errorDescription
     * @return DispenserStatusData
     */
    public function setErrorDescription(string $errorDescription): self
    {
        $this->errorDescription = $errorDescription;

        return $this;
    }

    /**
     * @return string
     */
    public function getNozzleId(): string
    {
        return $this->nozzleId;
    }

    /**
     * @param string $nozzleId
     * @return DispenserStatusData
     */
    public function setNozzleId(string $nozzleId): self
    {
        $this->nozzleId = $nozzleId;

        return $this;
    }

    /**
     * @return string
     */
    public function getFuelId(): string
    {
        return $this->fuelId;
    }

    /**
     * @param string $fuelId
     * @return DispenserStatusData
     */
    public function setFuelId(string $fuelId): self
    {
        $this->fuelId = $fuelId;

        return $this;
    }

    /**
     * @return string
     */
    public function getFuelName(): string
    {
        return $this->fuelName;
    }

    /**
     * @param string $fuelName
     * @return DispenserStatusData
     */
    public function setFuelName(string $fuelName): self
    {
        $this->fuelName = $fuelName;

        return $this;
    }

    public function getFuelPrice(): Money
    {
        return $this->fuelPrice;
    }

    /**
     * @param string $fuelPrice
     * @return DispenserStatusData
     */
    public function setFuelPrice(string $fuelPrice): self
    {
        $currencies = new ISOCurrencies();

        $moneyParser = new DecimalMoneyParser($currencies);

        $this->fuelPrice = $moneyParser->parse($fuelPrice, new Currency(Wayforpay::CURRENCY));

        return $this;
    }

    /**
     * @return array
     */
    public function getTokens(): array
    {
        return $this->tokens;
    }

    /**
     * @param array $tokens
     * @return DispenserStatusData
     */
    public function setTokens(array $tokens)
    {
        $this->tokens = $tokens;
        return $this;
    }



}