<?php


namespace App\DTO\MPosRequest;


use Symfony\Component\Serializer\Annotation\SerializedName;

class GetFuelPrice
{
    /**
     * @var string
     * @SerializedName("Type")
     */
    private $type = 'GET_FUEL_PRICE';

    /**
     * @var string
     * * @SerializedName("SessionId")
     */
    private $sessionId;

    /**
     * @var string
     * * @SerializedName("TerminalId")
     */
    private $terminalId;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return GetFuelPrice
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     * @return GetFuelPrice
     */
    public function setSessionId(string $sessionId): self
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTerminalId(): string
    {
        return $this->terminalId;
    }

    /**
     * @param string $terminalId
     * @return GetFuelPrice
     */
    public function setTerminalId(string $terminalId): self
    {
        $this->terminalId = $terminalId;

        return $this;
    }


}