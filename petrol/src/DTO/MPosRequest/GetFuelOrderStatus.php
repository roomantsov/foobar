<?php


namespace App\DTO\MPosRequest;


use Symfony\Component\Serializer\Annotation\SerializedName;

class GetFuelOrderStatus
{
    /**
     * @SerializedName("Type")
     */
    private $type = 'GET_FUEL_ORDER_STATUS';

    /**
     * @SerializedName("SessionId")
     */
    private $sessionId;

    /**
     * @SerializedName("TerminalId")
     */
    private $terminalId;

    /**
     * @SerializedName("DispenserId")
     */
    private $dispenserId;

    /**
     * @SerializedName("OrderId")
     */
    private $orderId;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return GetFuelOrderStatus
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     * @return GetFuelOrderStatus
     */
    public function setSessionId(string $sessionId): self
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTerminalId(): string
    {
        return $this->terminalId;
    }

    /**
     * @param string $terminalId
     * @return GetFuelOrderStatus
     */
    public function setTerminalId(string $terminalId): self
    {
        $this->terminalId = $terminalId;

        return $this;
    }

    /**
     * @return string
     */
    public function getDispenserId(): string
    {
        return $this->dispenserId;
    }

    /**
     * @param string $dispenserId
     * @return GetFuelOrderStatus
     */
    public function setDispenserId(string $dispenserId): self
    {
        $this->dispenserId = $dispenserId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     * @return GetFuelOrderStatus
     */
    public function setOrderId($orderId): self
    {
        $this->orderId = $orderId;
        return $this;
    }
}