<?php


namespace App\DTO\MPosRequest;


use Money\Currencies\ISOCurrencies;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Money;
use Symfony\Component\Serializer\Annotation\SerializedName;

class SetFuelOrder
{
    /**
     * @var string
     * @SerializedName("Type")
     */
    private $type = 'SET_FUEL_ORDER';

    /**
     * @var string
     * * @SerializedName("SessionId")
     */
    private $sessionId;

    /**
     * @var string
     * * @SerializedName("TerminalId")
     */
    private $terminalId;

    /**
     * @SerializedName("DispenserId")
     */
    private $dispenserId;

    /**
     * @SerializedName("FuelId")
     */
    private $fuelId;

    /**
     * @SerializedName("OrderAmount")
     */
    private $orderAmount;

    /**
     * @SerializedName("OrderSum")
     */
    private $orderSum;

    /**
     * @SerializedName("OrderDiscount")
     */
    private string $orderDiscount;

    /**
     * @SerializedName("InitiatorTransactionId")
     */
    private $initiatorTransactionId;

    /**
     * @SerializedName("PayType")
     */
    private $payType = 14;

    /**
     * @SerializedName("Messages")
     */
    private $messages;

    /**
     * SetFuelOrder constructor.
     */
    public function __construct()
    {
        $this->messages['Message']['Channel'] = 1;
        $this->messages['Message']['MessageDescription'] = 'Дізнавайтесь інформацію про стан бонусного балансу у мобільному додатку.';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return SetFuelOrder
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     * @return SetFuelOrder
     */
    public function setSessionId(string $sessionId): self
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTerminalId(): string
    {
        return $this->terminalId;
    }

    /**
     * @param string $terminalId
     * @return SetFuelOrder
     */
    public function setTerminalId(string $terminalId): self
    {
        $this->terminalId = $terminalId;

        return $this;
    }

    /**
     * @return string
     */
    public function getDispenserId(): string
    {
        return $this->dispenserId;
    }

    /**
     * @param string $dispenserId
     * @return SetFuelOrder
     */
    public function setDispenserId(string $dispenserId): self
    {
        $this->dispenserId = $dispenserId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFuelId()
    {
        return $this->fuelId;
    }

    /**
     * @param mixed $fuelId
     * @return SetFuelOrder
     */
    public function setFuelId($fuelId): self
    {
        $this->fuelId = $fuelId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderAmount()
    {
        return $this->orderAmount;
    }

    /**
     * @param mixed $orderAmount
     * @return SetFuelOrder
     */
    public function setOrderAmount($orderAmount): self
    {
        $this->orderAmount = $orderAmount;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderSum(): string
    {
        return $this->orderSum;
    }

    /**
     * @param Money $orderSum
     * @return SetFuelOrder
     */
    public function setOrderSum(Money $orderSum): self
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $this->orderSum = $moneyFormatter->format($orderSum);
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderDiscount(): string
    {
        return $this->orderDiscount;
    }

    /**
     * @param Money $orderDiscount
     * @return SetFuelOrder
     */
    public function setOrderDiscount(Money $orderDiscount): SetFuelOrder
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $this->orderDiscount = $moneyFormatter->format($orderDiscount);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInitiatorTransactionId()
    {
        return $this->initiatorTransactionId;
    }

    /**
     * @param mixed $initiatorTransactionId
     * @return SetFuelOrder
     */
    public function setInitiatorTransactionId($initiatorTransactionId): self
    {
        $this->initiatorTransactionId = $initiatorTransactionId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPayType(): int
    {
        return $this->payType;
    }

    /**
     * @param int $payType
     * @return SetFuelOrder
     */
    public function setPayType(int $payType): SetFuelOrder
    {
        $this->payType = $payType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     * @return SetFuelOrder
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
        return $this;
    }
}
