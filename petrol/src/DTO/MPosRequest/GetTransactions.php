<?php


namespace App\DTO\MPosRequest;


use DateTime;
use Symfony\Component\Serializer\Annotation\SerializedName;

class GetTransactions
{
    /**
     * @var string
     * @SerializedName("Type")
     */
    private $type = 'GET_TRANSACTIONS';

    /**
     * @var string
     * * @SerializedName("HolderId")
     */
    private $holderId;

    /**
     * @var string
     * * @SerializedName("FromDate")
     */
    private $fromDate;

    /**
     * @var string
     * * @SerializedName("ToDate")
     */
    private $toDate;

    public function __construct()
    {
        $now = new DateTime();
        // MPos takes 00:00 as a default time, so we should add 1 day to get transactions including current day
        $this->toDate = $now->modify("+1 day")->format('d.m.Y');
        $this->fromDate = $now->modify('-29 days')->format('d.m.Y');
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return GetTransactions
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getHolderId(): string
    {
        return $this->holderId;
    }

    /**
     * @param string $holderId
     * @return GetTransactions
     */
    public function setHolderId(string $holderId): self
    {
        $this->holderId = $holderId;

        return $this;
    }

    /**
     * @return string
     */
    public function getFromDate(): string
    {
        return $this->fromDate;
    }


    /**
     * @return string
     */
    public function getToDate(): string
    {
        return $this->toDate;
    }
}
