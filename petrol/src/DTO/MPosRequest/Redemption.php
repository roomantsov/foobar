<?php


namespace App\DTO\MPosRequest;


use DateTime;
use Money\Currencies\ISOCurrencies;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Money;
use Symfony\Component\Serializer\Annotation\SerializedName;

class Redemption
{
    /**
     * @SerializedName("Type")
     */
    private $type = 'REDEMPTION';

    private $context;

    private $transactionNo;

    private $transactionHash;

    private $online = 1;

    private $totalAmount;

    private $bonusRedeem;

    private $totalAmountBeforeDiscount;

    /**
     * @SerializedName("PayType")
     */
    private $payType = 14;

    /**
     * @SerializedName("GoodsList")
     */
    private $goodsList;

    public function __construct()
    {
        $date = new DateTime();
        $this->context['transactionDate'] = $date->format("Y-m-d H:i:s");
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Redemption
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param $cardNo
     * @return Redemption
     */
    public function setCardNo($cardNo): self
    {
        $this->context['cardNo'] = $cardNo;
        return $this;
    }

    /**
     * @param $terminalId
     * @return Redemption
     */
    public function setTerminalId($terminalId): self
    {
        $this->context['terminalID'] = $terminalId;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getTransactionNo()
    {
        return $this->transactionNo;
    }

    /**
     * @param mixed $transactionNo
     * @return Redemption
     */
    public function setTransactionNo($transactionNo): self
    {
        $this->transactionNo = $transactionNo;
        return $this;
    }

    /**
     * @return string
     */
    public function getTransactionHash(): string
    {
        return $this->transactionHash;
    }

    /**
     * @param mixed $transactionHash
     * @return Redemption
     */
    public function setTransactionHash($transactionHash): self
    {
        $this->transactionHash = $transactionHash;
        return $this;
    }

    /**
     * @return int
     */
    public function getOnline(): int
    {
        return $this->online;
    }

    /**
     * @param int $online
     * @return Redemption
     */
    public function setOnline(int $online): self
    {
        $this->online = $online;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * @param Money $totalAmount
     * @return self
     */
    public function setTotalAmount(Money $totalAmount): self
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $this->totalAmount = $moneyFormatter->format($totalAmount);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBonusRedeem()
    {
        return $this->bonusRedeem;
    }

    /**
     * @param Money $bonusRedeem
     * @return Redemption
     */
    public function setBonusRedeem(Money $bonusRedeem): self
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $this->bonusRedeem = $moneyFormatter->format($bonusRedeem);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalAmountBeforeDiscount()
    {
        return $this->totalAmountBeforeDiscount;
    }

    /**
     * @param mixed $totalAmountBeforeDiscount
     * @return Redemption
     */
    public function setTotalAmountBeforeDiscount(Money $totalAmountBeforeDiscount): self
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $this->totalAmountBeforeDiscount = $moneyFormatter->format($totalAmountBeforeDiscount);
        return $this;
    }

    /**
     * @return int
     */
    public function getPayType(): int
    {
        return $this->payType;
    }

    /**
     * @param int $payType
     * @return Redemption
     */
    public function setPayType(int $payType): self
    {
        $this->payType = $payType;
        return $this;
    }

    /**
     * @return array
     */
    public function getGoodsList(): array
    {
        return $this->goodsList;
    }

    /**
     * @return string|null
     */
    public function getTransactionDate(): ?string
    {
        return $this->context['transactionDate'] ?? null;
    }

    /**
     * @param array $goodsList
     * @return Redemption
     */
    public function setGoodsList($goodsList):self
    {
        $this->goodsList = $goodsList;
        return $this;
    }

    public function setGoodsCodes($code): self
    {
        $this->goodsList['goods']['code1'] = $code;
        $this->goodsList['goods']['code2'] = $code;
        $this->goodsList['goods']['code3'] = $code;
        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name): self
    {
        $this->goodsList['goods']['name'] = $name;
        return $this;
    }

    /**
     * @param $qty
     * @return $this
     */
    public function setQty($qty): self
    {
        $this->goodsList['goods']['quantity'] = $qty;
        return $this;
    }

    /**
     * @param Money $price
     * @return $this
     */
    public function setPrice(Money $price): self
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $this->goodsList['goods']['price'] = $moneyFormatter->format($price);
        return $this;
    }

    /**
     * @param Money $amount
     * @return $this
     */
    public function setAmount(Money $amount): self
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $this->goodsList['goods']['amount'] = $moneyFormatter->format($amount);
        return $this;
    }

    /**
     * @param $amountDiscount
     * @return $this
     */
    public function setAmountDiscount($amountDiscount): self
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $this->goodsList['goods']['amountDiscount'] = $moneyFormatter->format($amountDiscount);
        return $this;
    }
}
