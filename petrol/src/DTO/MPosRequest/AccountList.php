<?php


namespace App\DTO\MPosRequest;


use Symfony\Component\Serializer\Annotation\SerializedName;

class AccountList
{
    /**
     * @var string
     * @SerializedName("Type")
     */
    private $type = 'ACCOUNT_LIST';

    /**
     * @var string
     * * @SerializedName("HolderId")
     */
    private $holderId;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return AccountList
     */
    public function setType(string $type): AccountList
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getHolderId(): string
    {
        return $this->holderId;
    }

    /**
     * @param string $holderId
     * @return AccountList
     */
    public function setHolderId(string $holderId): AccountList
    {
        $this->holderId = $holderId;

        return $this;
    }


}