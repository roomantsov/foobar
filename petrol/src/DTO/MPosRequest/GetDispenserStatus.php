<?php


namespace App\DTO\MPosRequest;


use Symfony\Component\Serializer\Annotation\SerializedName;

class GetDispenserStatus
{
    /**
     * @SerializedName("Type")
     */
    private $type = 'GET_DISPENSER_STATUS';

    /**
     * @SerializedName("SessionId")
     */
    private $sessionId;

    /**
     * @SerializedName("TerminalId")
     */
    private $terminalId;

    /**
     * @SerializedName("DispenserId")
     */
    private $dispenserId;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return GetDispenserStatus
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     * @return GetDispenserStatus
     */
    public function setSessionId(string $sessionId): self
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTerminalId(): string
    {
        return $this->terminalId;
    }

    /**
     * @param string $terminalId
     * @return GetDispenserStatus
     */
    public function setTerminalId(string $terminalId): self
    {
        $this->terminalId = $terminalId;

        return $this;
    }

    /**
     * @return string
     */
    public function getDispenserId(): string
    {
        return $this->dispenserId;
    }

    /**
     * @param string $dispenserId
     * @return GetDispenserStatus
     */
    public function setDispenserId(string $dispenserId): self
    {
        $this->dispenserId = $dispenserId;

        return $this;
    }
}