<?php


namespace App\DTO\MPosRequest;


use Symfony\Component\Serializer\Annotation\SerializedName;

class ChangeHolderData
{
    /**
     * @var string
     * @SerializedName("Type")
     */
    private $type = 'CHANGE_PERSONAL_DATA';

    /**
     * @var string
     * * @SerializedName("HolderId")
     */
    private $holderId;

    /**
     * @var array
     * @SerializedName("HolderData")
     */
    private $holderData;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return ChangeHolderData
     */
    public function setType(string $type): ChangeHolderData
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getHolderId(): string
    {
        return $this->holderId;
    }

    /**
     * @param string $holderId
     * @return ChangeHolderData
     */
    public function setHolderId(string $holderId): ChangeHolderData
    {
        $this->holderId = $holderId;

        return $this;
    }

    /**
     * @return array
     */
    public function getHolderData(): array
    {
        return $this->holderData;
    }

    /**
     * @param array $holderData
     * @return ChangeHolderData
     */
    public function setHolderData(array $holderData): ChangeHolderData
    {
        if (array_key_exists('FirstName', $holderData)) {
            $holderData['FirstName'] = ucfirst(mb_substr($holderData['FirstName'], 0, 18));
        } elseif (array_key_exists('SecondName', $holderData)) {
            $holderData['SecondName'] = ucfirst(mb_substr($holderData['SecondName'], 0, 18));
        } elseif ( array_key_exists('LastName', $holderData)) {
            $holderData['LastName'] = ucfirst(mb_substr($holderData['LastName'], 0, 18));
        }

        $this->holderData = $holderData;

        return $this;
    }


}