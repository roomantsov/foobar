<?php


namespace App\DTO\MPosRequest;

use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * Class Authorization
 * @package App\DataTransfer
 */
class Authorization
{
    /**
     * @var string
     * @SerializedName("Type")
     */
    private $type = 'AUTHORIZATION';

    private $siteUser;

    private $sitePassword;

    /**
     * Authorization constructor.
     * @param $siteUser string
     * @param $sitePassword string
     */
    public function __construct(string $siteUser, string $sitePassword)
    {
        $this->siteUser = $siteUser;
        $this->sitePassword = $sitePassword;
    }


    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Authorization
     */
    public function setType($type): Authorization
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteUser()
    {
        return $this->siteUser;
    }

    /**
     * @param mixed $siteUser
     * @return Authorization
     */
    public function setSiteUser($siteUser): Authorization
    {
        $this->siteUser = $siteUser;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSitePassword()
    {
        return $this->sitePassword;
    }

    /**
     * @param mixed $sitePassword
     * @return Authorization
     */
    public function setSitePassword($sitePassword): Authorization
    {
        $this->sitePassword = $sitePassword;

        return $this;
    }


}