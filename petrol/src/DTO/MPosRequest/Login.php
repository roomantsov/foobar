<?php


namespace App\DTO\MPosRequest;


use Symfony\Component\Serializer\Annotation\SerializedName;

class Login
{
    /**
     * @var string
     * @SerializedName("Type")
     */
    private $type = 'LOGIN';

    /**
     * @var string
     * @SerializedName("Mobile")
     */
    private $mobile;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Login
     */
    public function setType(string $type): Login
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param mixed $mobile
     * @return Login
     */
    public function setMobile($mobile): Login
    {
        $this->mobile = '38'.$mobile;

        return $this;
    }


}