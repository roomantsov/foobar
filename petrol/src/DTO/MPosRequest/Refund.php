<?php


namespace App\DTO\MPosRequest;


use DateTime;
use Money\Currencies\ISOCurrencies;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Money;
use Symfony\Component\Serializer\Annotation\SerializedName;

class Refund
{
    /**
     * @SerializedName("Type")
     */
    private string $type = 'REFUND';

    private $context;

    private string $transactionNo;

    private string $transactionHash;

    private int $online = 1;

    private $totalAmount;

    /**
     * @SerializedName("PayType")
     */
    private int $payType = 14;

    /**
     * @SerializedName("GoodsList")
     */
    private array $goodsList;

    private array $originalTransactionIdentifiers;

    public function __construct()
    {
        $date = new DateTime();
        $this->context['transactionDate'] = $date->format("Y-m-d H:i:s");
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Refund
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param $cardNo
     * @return Refund
     */
    public function setCardNo($cardNo): self
    {
        $this->context['cardNo'] = $cardNo;
        return $this;
    }

    /**
     * @param string $terminalId
     * @return Refund
     */
    public function setTerminalId(string $terminalId): self
    {
        $this->context['terminalID'] = $terminalId;
        return $this;
    }



    /**
     * @return string
     */
    public function getTransactionNo(): string
    {
        return $this->transactionNo;
    }

    /**
     * @param string $transactionNo
     * @return Refund
     */
    public function setTransactionNo(string $transactionNo): self
    {
        $this->transactionNo = $transactionNo;
        return $this;
    }

    /**
     * @return string
     */
    public function getTransactionHash(): string
    {
        return $this->transactionHash;
    }

    /**
     * @param mixed $transactionHash
     * @return Refund
     */
    public function setTransactionHash(string $transactionHash): self
    {
        $this->transactionHash = $transactionHash;
        return $this;
    }

    /**
     * @return int
     */
    public function getOnline(): int
    {
        return $this->online;
    }

    /**
     * @param int $online
     * @return Refund
     */
    public function setOnline(int $online): self
    {
        $this->online = $online;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalAmount(): string
    {
        return $this->totalAmount;
    }

    /**
     * @param Money $totalAmount
     * @return Refund
     */
    public function setTotalAmount(Money $totalAmount): self
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        //needs to be a negative
        $this->totalAmount = $moneyFormatter->format($totalAmount) * (-1);
        return $this;
    }

    /**
     * @return int
     */
    public function getPayType(): int
    {
        return $this->payType;
    }

    /**
     * @param int $payType
     * @return Refund
     */
    public function setPayType(int $payType): self
    {
        $this->payType = $payType;
        return $this;
    }

    /**
     * @return array
     */
    public function getGoodsList(): array
    {
        return $this->goodsList;
    }

    /**
     * @param mixed $goodsList
     * @return Refund
     */
    public function setGoodsList(array $goodsList):self
    {
        $this->goodsList = $goodsList;
        return $this;
    }

    /**
     * @param $code
     * @return $this
     */
    public function setGoodsCodes($code): self
    {
        $this->goodsList['goods']['code1'] = $code;
        $this->goodsList['goods']['code2'] = $code;
        $this->goodsList['goods']['code3'] = $code;
        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->goodsList['goods']['name'] = $name;
        return $this;
    }

    /**
     * @param $qty
     * @return $this
     */
    public function setQty($qty): self
    {
        $this->goodsList['goods']['quantity'] = $qty * (-1);
        return $this;
    }

    /**
     * @param Money $price
     * @return $this
     */
    public function setPrice(Money $price): self
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $this->goodsList['goods']['price'] = $moneyFormatter->format($price);
        return $this;
    }

    /**
     * @param Money $amount
     * @return $this
     */
    public function setAmount(Money $amount): self
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $this->goodsList['goods']['amount'] = $moneyFormatter->format($amount) * (-1);
        return $this;
    }

    /**
     * @param $amountDiscount
     * @return $this
     */
    public function setAmountDiscount($amountDiscount): self
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $this->goodsList['goods']['amountDiscount'] = $moneyFormatter->format($amountDiscount) * (-1);
        return $this;
    }

    /**
     * @param Money $bonusMaxRedeem
     * @return $this
     */
    public function setBonusMaxRedeem(Money $bonusMaxRedeem): self
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $this->goodsList['goods']['bonusMaxRedeem'] = $moneyFormatter->format($bonusMaxRedeem);
        return $this;
    }

    /**
     * @return array
     */
    public function getOriginalTransactionIdentifiers(): array
    {
        return $this->originalTransactionIdentifiers;
    }

    /**
     * @param $originalTransactionDate
     * @return Refund
     */
    public function setOriginalTransactionDate($originalTransactionDate): self
    {
        $this->originalTransactionIdentifiers['originalTransactionDate'] = $originalTransactionDate;
        return $this;
    }

    /**
     * @param $originalTransactionNo
     * @return $this
     */
    public function setOriginalTransactionNo($originalTransactionNo): self
    {
        $this->originalTransactionIdentifiers['originalTransactionNo'] = $originalTransactionNo;
        return $this;
    }

    /**
     * @param $originalTerminalID
     * @return $this
     */
    public function setOriginalTerminalID($originalTerminalID): self
    {
        $this->originalTransactionIdentifiers['originalTerminalID'] = $originalTerminalID;
        return $this;
    }

    public function setOriginalClmTrnId($originalClmTrnId): self
    {
        $this->originalTransactionIdentifiers['originalClmTrnId'] = $originalClmTrnId;
        return $this;
    }
}
