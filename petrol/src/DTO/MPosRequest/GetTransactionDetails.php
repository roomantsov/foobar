<?php


namespace App\DTO\MPosRequest;


use Symfony\Component\Serializer\Annotation\SerializedName;

class GetTransactionDetails
{
    /**
     * @var string
     * @SerializedName("Type")
     */
    private $type = 'GET_TRANSACTION_DETAILS';

    /**
     * @var string
     * * @SerializedName("HolderId")
     */
    private $holderId;

    /**
     * @var string
     * * @SerializedName("TransactionId")
     */
    private $transactionId;


    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return GetTransactionDetails
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getHolderId(): string
    {
        return $this->holderId;
    }

    /**
     * @param string $holderId
     * @return GetTransactionDetails
     */
    public function setHolderId(string $holderId): GetTransactionDetails
    {
        $this->holderId = $holderId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTransactionId(): string
    {
        return $this->transactionId;
    }

    /**
     * @param string $transactionId
     * @return GetTransactionDetails
     */
    public function setTransactionId(string $transactionId): GetTransactionDetails
    {
        $this->transactionId = $transactionId;
        return $this;
    }

}