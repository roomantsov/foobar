<?php


namespace App\DTO\Discount;


use App\Service\Wayforpay\Wayforpay;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Money;
use Money\Parser\DecimalMoneyParser;

class DiscountData
{

    /**
     * @var string
     */
    private $errorCode;

    /**
     * @var string
     */
    private $errorDescription;

    /**
     * @var string|array
     */
    private $messages;

    /**
     * @var array|null
     */
    private $bonusList;


    private $balanceActive;


    private $balanceTotal;

    /**
     * @var string|array
     */
    private $clmTrnId;

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    /**
     * @param string $errorCode
     * @return DiscountData
     */
    public function setErrorCode(string $errorCode): DiscountData
    {
        $this->errorCode = $errorCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getErrorDescription(): string
    {
        return $this->errorDescription;
    }

    /**
     * @param string $errorDescription
     * @return DiscountData
     */
    public function setErrorDescription(string $errorDescription): DiscountData
    {
        $this->errorDescription = $errorDescription;
        return $this;
    }

    /**
     * @return string|array
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param array|string $messages
     * @return DiscountData
     */
    public function setMessages($messages): DiscountData
    {
        $this->messages = $messages;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getBonusList(): ?array
    {
        return $this->bonusList;
    }

    /**
     * @return string|null
     */
    public function getTransactionDate(): ?string
    {
        return $this->bonusList['transactionDate'] ?? null;
    }

    /**
     * @param array|null $bonusList
     * @return DiscountData
     */
    public function setBonusList(?array $bonusList): DiscountData
    {
        $this->bonusList = $bonusList['GoodsData'] ?? null;
        return $this;
    }


    public function getBalanceTotal(): Money
    {
        return $this->balanceTotal;
    }

    /**
     * @param string $balanceTotal
     * @return DiscountData
     */
    public function setBalanceTotal(string $balanceTotal): DiscountData
    {
        $currencies = new ISOCurrencies();
        $moneyParser = new DecimalMoneyParser($currencies);

        $this->balanceTotal = $moneyParser->parse($balanceTotal, new Currency(Wayforpay::CURRENCY));
        return $this;
    }

    /**
     * @return string|array
     */
    public function getClmTrnId()
    {
        return $this->clmTrnId;
    }

    /**
     * @param string|array $clmTrnId
     * @return DiscountData
     */
    public function setClmTrnId($clmTrnId): DiscountData
    {
        $this->clmTrnId = $clmTrnId;
        return $this;
    }


    /**
     * @param mixed $balanceActive
     * @return DiscountData
     */
    public function setBalanceActive(string $balanceActive): self
    {
        $currencies = new ISOCurrencies();
        $moneyParser = new DecimalMoneyParser($currencies);

        $this->balanceActive = $moneyParser->parse($balanceActive, new Currency(Wayforpay::CURRENCY));
        return $this;
    }


    public function getBalanceActive(): Money
    {
        return $this->balanceActive;
    }

    /**
     * @return Money|null
     */
    public function getBonusRedeem(): ?Money
    {
        if (isset($this->bonusList['bonusRedeem'])) {
            $currencies = new ISOCurrencies();
            $moneyParser = new DecimalMoneyParser($currencies);

            return $moneyParser->parse((string)$this->bonusList['bonusRedeem'], new Currency(Wayforpay::CURRENCY));
        }

        return null;
    }

    /**
     * @param Money $bonusRedeem
     * @return DiscountData
     */
    public function setBonusRedeem(Money $bonusRedeem): self
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $this->bonusList['bonusRedeem'] = $moneyFormatter->format($bonusRedeem);
        return $this;
    }

    /**
     * @return Money|null
     */
    public function getBonusIssuance(): ?Money
    {
        if (isset($this->bonusList['bonusIssuance'])) {
            $currencies = new ISOCurrencies();
            $moneyParser = new DecimalMoneyParser($currencies);

            return $moneyParser->parse((string)$this->bonusList['bonusIssuance'], new Currency(Wayforpay::CURRENCY));
        }

        return null;
    }
}
