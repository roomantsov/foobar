<?php


namespace App\DTO\Carwash;


class OrderRequest
{
    private string $login;

    private string $termCode;

    private string $postCode;

    private int $advance;

    private string $apiKey;

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     * @return OrderRequest
     */
    public function setLogin(string $login): OrderRequest
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return string
     */
    public function getTermCode(): string
    {
        return $this->termCode;
    }

    /**
     * @param string $termCode
     * @return OrderRequest
     */
    public function setTermCode(string $termCode): OrderRequest
    {
        $this->termCode = $termCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostCode(): string
    {
        return $this->postCode;
    }

    /**
     * @param string $postCode
     * @return OrderRequest
     */
    public function setPostCode(string $postCode): OrderRequest
    {
        $this->postCode = $postCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getAdvance(): int
    {
        return $this->advance;
    }

    /**
     * @param int $advance
     * @return OrderRequest
     */
    public function setAdvance(int $advance): OrderRequest
    {
        $this->advance = $advance;
        return $this;
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     * @return OrderRequest
     */
    public function setApiKey(string $apiKey): OrderRequest
    {
        $this->apiKey = $apiKey;
        return $this;
    }
}