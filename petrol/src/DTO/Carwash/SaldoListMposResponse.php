<?php


namespace App\DTO\Carwash;


use Symfony\Component\Serializer\Annotation\SerializedName;

class SaldoListMposResponse
{
    private string $errorCode;

    private string $errorDescription;

    /**
     * @SerializedName("GoodList");
     */
    private array $goodList;

    /**
     * SaldoListMposResponse constructor.
     * @param string $errorCode
     * @param string $errorDescription
     * @param array $goodList
     */
    public function __construct(string $errorCode, string $errorDescription, array $goodList)
    {
        $this->errorCode = $errorCode;
        $this->errorDescription = $errorDescription;
        $this->goodList = $goodList;
    }

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    /**
     * @return string
     */
    public function getErrorDescription(): string
    {
        return $this->errorDescription;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->goodList['good']['price'];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->goodList['good']['name'];
    }
}