<?php


namespace App\DTO\Carwash;


use Symfony\Component\Serializer\Annotation\SerializedName;

class SaldoListMposRequest
{

    /**
     * @SerializedName("Type")
     */
    private string $type = 'GET_GOOD_SALDO_LIST';

    /**
     * @SerializedName("SessionId")
     */
    private string $sessionId;

    /**
     * @SerializedName("TerminalId")
     */
    private int $terminalId;

    /**
     * @SerializedName("GoodList")
     */
    private array $goodList = [];

    /**
     * SaldoListMposRequest constructor.
     * @param string $sessionId
     * @param int $terminalId
     * @param int $code
     */
    public function __construct(
        string $sessionId,
        int $terminalId,
        int $code
    )
    {
        $this->sessionId = $sessionId;
        $this->terminalId = $terminalId;
        $this->goodList['good']['code1'] = $code;
        $this->goodList['good']['name'] = 'Item';

    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * @return int
     */
    public function getTerminalId(): int
    {
        return $this->terminalId;
    }

    /**
     * @return array
     */
    public function getGoodList(): array
    {
        return $this->goodList;
    }


}