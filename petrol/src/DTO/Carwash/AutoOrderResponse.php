<?php


namespace App\DTO\Carwash;


class AutoOrderResponse
{
    private string $errorCode;

    private string $errorDescription;

    private ?string $Status;

    private ?string $StatusName;

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    /**
     * @param string $errorCode
     * @return AutoOrderResponse
     */
    public function setErrorCode(string $errorCode): AutoOrderResponse
    {
        $this->errorCode = $errorCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getErrorDescription(): string
    {
        return $this->errorDescription;
    }

    /**
     * @param string $errorDescription
     * @return AutoOrderResponse
     */
    public function setErrorDescription(string $errorDescription): AutoOrderResponse
    {
        $this->errorDescription = $errorDescription;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->Status;
    }

    /**
     * @param string|null $Status
     * @return AutoOrderResponse
     */
    public function setStatus(?string $Status): AutoOrderResponse
    {
        $this->Status = $Status;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatusName(): ?string
    {
        return $this->StatusName;
    }

    /**
     * @param string|null $StatusName
     * @return AutoOrderResponse
     */
    public function setStatusName(?string $StatusName): AutoOrderResponse
    {
        $this->StatusName = $StatusName;
        return $this;
    }
}