<?php


namespace App\DTO\Carwash;


class OrderResponse
{
    private bool $success;

    private string $msg;

    /**
     * OrderResponse constructor.
     * @param bool $success
     * @param string $msg
     */
    public function __construct(bool $success, string $msg)
    {
        $this->success = $success;
        $this->msg = $msg;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return string
     */
    public function getMsg(): string
    {
        return $this->msg;
    }




}