<?php


namespace App\DTO\Carwash;


use Symfony\Component\Serializer\Annotation\SerializedName;

class AutoOrderRequest
{

    /**
     * @SerializedName("Type")
     */
    private string $type = 'SET_GOODS_AUTO_ORDER';

    /**
     * @SerializedName("SessionId")
     */
    private string $sessionId;

    /**
     * @SerializedName("TerminalId")
     */
    private int $terminalId;

    /**
     * @SerializedName("InitiatorTransactionId")
     */
    private string $initiatorTransactionId;

    /**
     * @SerializedName("Paid")
     */
    private int $paid = 1;

    /**
     * @SerializedName("GoodList")
     */
    private array $goodList = [];

    /**
     * AutoOrderRequest constructor.
     * @param string $sessionId
     * @param int $terminalId
     * @param int $code
     * @param string $initiatorTransactionId
     * @param string $name
     * @param string $quantity
     * @param string $price
     * @param string $amount
     * @param string $discount
     */
    public function __construct(
        string $sessionId,
        int $terminalId,
        int $code,
        string $initiatorTransactionId,
        string $name,
        string $quantity,
        string $price,
        string $amount,
        string $discount

    )
    {
        $this->sessionId = $sessionId;
        $this->terminalId = $terminalId;
        $this->initiatorTransactionId = $initiatorTransactionId;
        $this->goodList['good']['code1'] = $code;
        $this->goodList['good']['name'] = $name;
        $this->goodList['good']['quantity'] = $quantity;
        $this->goodList['good']['price'] = $price;
        $this->goodList['good']['amount'] = $amount;
        $this->goodList['good']['discount'] = $discount;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * @return int
     */
    public function getTerminalId(): int
    {
        return $this->terminalId;
    }

    /**
     * @return string
     */
    public function getInitiatorTransactionId(): string
    {
        return $this->initiatorTransactionId;
    }

    /**
     * @return int
     */
    public function getPaid(): int
    {
        return $this->paid;
    }

    /**
     * @return array
     */
    public function getGoodList(): array
    {
        return $this->goodList;
    }


}