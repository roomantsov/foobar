<?php


namespace App\DTO;


class UserData
{
    private $errorCode;

    private $errorDescription;

    private $sessionId;

    private $holderId;

    private $holderData;

    private $accounts;

    private $transactions;

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    /**
     * @param string $errorCode
     * @return UserData
     */
    public function setErrorCode(string $errorCode): self
    {
        $this->errorCode = $errorCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getErrorDescription(): string
    {
        return $this->errorDescription;
    }

    /**
     * @param string $errorDescription
     * @return UserData
     */
    public function setErrorDescription(string $errorDescription): self
    {
        $this->errorDescription = $errorDescription;

        return $this;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     * @return UserData
     */
    public function setSessionId(string $sessionId): self
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * @return string
     */
    public function getHolderId()
    {
        return $this->holderId;
    }

    /**
     * @param string $holderId
     * @return UserData
     */
    public function setHolderId(string $holderId): self
    {
        $this->holderId = $holderId;

        return $this;
    }

    /**
     * @return array
     */
    public function getHolderData()
    {
        return $this->holderData;
    }

    /**
     * @param array $holderData
     * @return UserData
     */
    public function setHolderData(array $holderData): self
    {
        $this->holderData = $holderData;

        return $this;
    }

    /**
     * @return array|string
     */
    public function getAccounts()
    {
        return $this->accounts;
    }

    /**
     * @param array|string $accounts
     * @return UserData
     */
    public function setAccounts($accounts): self
    {
        $this->accounts = $accounts;

        return $this;
    }

    /**
     * @return array
     */
    public function getDiscountCard()
    {
        return $this->accounts['Account']['Cards']['Card']['cardNo'] ?? null;
    }

    /**
     * @return array
     */
    public function getDiscountScheme()
    {
        return $this->accounts['Account']['AccountScheme'] ?? null;
    }

    public function getBonusBalance()
    {
        return $this->accounts['Account']['ActiveBalance'] ?? null;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->holderData['FirstName'] ?? '';
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->holderData['LastName'] ?? '';
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->holderData['Email'] ?? '';
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName(string $firstName): self
    {
        $this->holderData['FirstName'] = $firstName;
        return $this;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName(string $lastName): self
    {
        $this->holderData['LastName'] = $lastName;
        return $this;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->holderData['Email'] = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransactions()
    {
        return $this->transactions['TransactionData'] ?? null;
    }

    /**
     * @return mixed
     */
    public function getTransactionsOrderByDesc()
    {
        return gettype($this->getTransactions()) === "array" ? array_reverse($this->getTransactions()) : $this->getTransactions();
    }

    /**
     * @param mixed $transactions
     * @return UserData
     */
    public function setTransactions($transactions): self
    {
        $this->transactions = $transactions;
        return $this;
    }


}