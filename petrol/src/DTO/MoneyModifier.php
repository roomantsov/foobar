<?php


namespace App\DTO;




class MoneyModifier
{
    public string $amount;

    public array $currency;

    public function __construct($amount, $currency)
    {
        $this->amount = $amount;
        $this->currency = ['code' => $currency];
    }
}