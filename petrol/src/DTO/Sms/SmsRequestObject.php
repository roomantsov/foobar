<?php


namespace App\DTO\Sms;


class SmsRequestObject
{
    private $operation = 'SENDSMS';

    private $message  = [
        '@source' => '',
        '@rate' => '1',
        '@start_time' => 'AUTO',
        '@end_time' => 'AUTO',
        '@lifetime' => '4',
        'body' => '',
        'recipient' => '',
    ];

    /**
     * SmsRequestObject constructor.
     * @param string $alfaName
     */
    public function __construct(string $alfaName)
    {
        $this->setAlfaName($alfaName);
    }

    /**
     * @return string
     */
    public function getOperation(): string
    {
        return $this->operation;
    }

    /**
     * @return array
     */
    public function getMessage(): array
    {
        return $this->message;
    }



    /**
     * @param string $body
     * @return $this
     */
    public function setBody(string $body): self
    {
        $this->message['body'] = $body;

        return $this;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function setRecipient(string $phone): self
    {
        $this->message['recipient'] = '+38'.$phone;

        return $this;
    }

    /**
     * @param string $rate
     * @return $this
     */
    public function setRate(string $rate): self
    {
        $this->message['@rate'] = $rate;

        return $this;
    }

    /**
     * @param string $start_time
     * @return $this
     */
    public function setStartTime(string $start_time): self
    {
        $this->message['@start_time'] = $start_time;

        return $this;
    }

    /**
     * @param string $end_time
     * @return $this
     */
    public function setEndTime(string $end_time): self
    {
        $this->message['@end_time'] = $end_time;

        return $this;
    }

    /**
     * @param string $lifetime
     * @return $this
     */
    public function setLifeTime(string $lifetime): self
    {
        $this->message['@lifetime'] = $lifetime;

        return $this;
    }

    /**
     * @param string $alfaName
     */
    public function setAlfaName(string $alfaName): void
    {
        $this->message['@source'] = $alfaName;
    }


    /**
     * @return string
     */
    public function getAlfaName(): string
    {
        return $this->message['@source'];
    }


    /**
     * @return bool
     */
    public function isAlfaName(): bool
    {
        return $this->getAlfaName() ? true : false;
    }









}