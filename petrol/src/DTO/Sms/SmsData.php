<?php


namespace App\DTO\Sms;


class SmsData
{

    private $code;

    private $description;

    private $date;

    private $raw;

    private $campaignId;


    public function getCode()
    {
        return $this->code;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getCampaignId()
    {
        return $this->campaignId;
    }

    public function getRaw()
    {
        return $this->campaignId;
    }



    /**
     * @param mixed $state
     */
    public function setState($state): void
    {
        $this->raw = $state;

        $this->code = $state['@code'] ?? '';
        $this->campaignId = $state['@campaignID'] ?? '';
        $this->description = $state['#'] ?? '';
        $this->date = $state['@date'] ?? '';
    }

}