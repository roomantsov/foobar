<?php


namespace App\DTO;


use Money\Money;

class Product
{
    /**
     * @var int
     */
    protected int $id;

    /**
     * @var Money
     */
    protected Money $price;

    /**
     * @var string
     */
    protected string $name;

    /**
     * @var float
     */
    protected float $amount;

    /**
     * @var float
     */
    private $giveAmount = 0;

    /**
     * @var Money
     */
    private Money $bonusRedeem;

    public function __construct()
    {
        $this->bonusRedeem = Money::UAH(0);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Product
     */
    public function setId(int $id): Product
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Money
     */
    public function getPrice(): Money
    {
        return $this->price;
    }

    /**
     * @param Money $price
     * @return Product
     */
    public function setPrice(Money $price): Product
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Product
     */
    public function setName(string $name): Product
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return Product
     */
    public function setAmount(float $amount): Product
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return float
     */
    public function getGiveAmount(): float
    {
        return $this->giveAmount;
    }

    /**
     * @param float $giveAmount
     * @return Product
     */
    public function setGiveAmount(float $giveAmount): self
    {
        $this->giveAmount = $giveAmount;
        return $this;
    }

    /**
     * @return Money
     */
    public function getBonusRedeem(): Money
    {
        return $this->bonusRedeem;
    }

    /**
     * @param Money $bonusRedeem
     * @return Product
     */
    public function setBonusRedeem(Money $bonusRedeem): self
    {
        $this->bonusRedeem = $bonusRedeem;
        return $this;
    }


    /**
     * @return Money
     */
    public function getEstimatedOrderAmount(): Money
    {
        return $this->price->multiply($this->amount)->subtract($this->bonusRedeem);
    }

    /**
     * @return Money
     */
    public function getActualOrderAmount(): Money
    {
        return $this->price->multiply($this->giveAmount)->subtract($this->bonusRedeem);
    }

    /**
     * @return Money
     */
    public function getOrderAmountWithoutBonuses(): Money
    {
        return $this->price->multiply($this->giveAmount);
    }

    /**
     * @return Money
     */
    public function getRefundAmount(): Money
    {
        return $this->getEstimatedOrderAmount()->subtract($this->getActualOrderAmount());
    }

}