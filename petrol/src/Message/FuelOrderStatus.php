<?php


namespace App\Message;


use App\DTO\Product;
use App\DTO\Request\RequestObject;
use DateTime;
use DateTimeInterface;
use Money\Money;

class FuelOrderStatus implements RequestObject
{
    private string $terminal;

    private string $dispenser;

    private string $orderId;

    private Product $fuel;

    private bool $discount;

    private ?int $discountSum;

    private string $phone;

    private string $orderUuid;

    private string $card;

    private $giveAmount;

    private $orderStatus;

    private string $cardPan;

    private string $cardUuid;

    private string $holdStatus;

    private int $holdStatusCode;

    private float $holdSum;

    private string $successUrl;

    /**
     * @var DateTimeInterface
     */
    private $createDate;

    public function __construct()
    {
        $this->createDate = new DateTime();
    }

    /**
     * @return string
     */
    public function getTerminal(): string
    {
        return $this->terminal;
    }

    /**
     * @return string
     */
    public function getDispenser(): string
    {
        return $this->dispenser;
    }

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->orderId;
    }

    /**
     * @return Product
     */
    public function getFuel(): Product
    {
        return $this->fuel;
    }

    /**
     * @return bool
     */
    public function isDiscount(): bool
    {
        return $this->discount;
    }

    /**
     * @return int|null
     */
    public function getDiscountSum(): ?int
    {
        return $this->discountSum;
    }

    /**
     * @param int|null $discountSum
     * @return FuelOrderStatus
     */
    public function setDiscountSum(?int $discountSum): FuelOrderStatus
    {
        $this->discountSum = $discountSum;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getOrderUuid(): string
    {
        return $this->orderUuid;
    }

    /**
     * @return string
     */
    public function getCard(): string
    {
        return $this->card;
    }

    /**
     * @return float|null
     */
    public function getGiveAmount(): ?float
    {
        return $this->giveAmount;
    }

    /**
     * @param float|null $giveAmount
     * @return FuelOrderStatus
     */
    public function setGiveAmount(?float $giveAmount): FuelOrderStatus
    {
        $this->giveAmount = $giveAmount;
        return $this;
    }

    /**
     * @param string $terminal
     * @return FuelOrderStatus
     */
    public function setTerminal(string $terminal): self
    {
        $this->terminal = $terminal;
        return $this;
    }

    /**
     * @param string $dispenser
     * @return FuelOrderStatus
     */
    public function setDispenser(string $dispenser): self
    {
        $this->dispenser = $dispenser;
        return $this;
    }

    /**
     *
     * @param string $orderId
     * @return FuelOrderStatus
     */
    public function setOrderId(string $orderId): FuelOrderStatus
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * @param Product $fuel
     * @return FuelOrderStatus
     */
    public function setFuel(Product $fuel): self
    {
        if ($fuel instanceof Product) {
            $this->fuel = $fuel;
        } else {
            $product = new Product();
            $product
                ->setId($fuel['id'] ?? null)
                ->setPrice(Money::UAH($fuel['price']['amount'] ?? '0'))
                ->setName($fuel['name'] ?? null)
                ->setAmount($fuel['amount'] ?? null)
            ;

            $this->fuel = $product;
        }

        return $this;
    }

    /**
     * @param bool $discount
     * @return FuelOrderStatus
     */
    public function setDiscount(bool $discount): FuelOrderStatus
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * @param string $phone
     * @return FuelOrderStatus
     */
    public function setPhone(string $phone): FuelOrderStatus
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @param string $orderUuid
     * @return FuelOrderStatus
     */
    public function setOrderUuid(string $orderUuid): FuelOrderStatus
    {
        $this->orderUuid = $orderUuid;
        return $this;
    }

    /**
     * @param string $card
     * @return FuelOrderStatus
     */
    public function setCard(string $card): FuelOrderStatus
    {
        $this->card = $card;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderStatus(): ?string
    {
        return $this->orderStatus;
    }

    /**
     * @param string|null $orderStatus
     * @return FuelOrderStatus
     */
    public function setOrderStatus(?string $orderStatus): self
    {
        $this->orderStatus = $orderStatus;
        return $this;
    }

    /**
     * @return string
     */
    public function getCardPan(): string
    {
        return $this->cardPan;
    }

    /**
     * @param string $cardPan
     * @return FuelOrderStatus
     */
    public function setCardPan($cardPan): self
    {
        $this->cardPan = $cardPan;
        return $this;
    }

    /**
     * @return string
     */
    public function getHoldStatus(): ?string
    {
        return $this->holdStatus;
    }

    /**
     * @param string $holdStatus
     * @return FuelOrderStatus
     */
    public function setHoldStatus(string $holdStatus): self
    {
        $this->holdStatus = $holdStatus;
        return $this;
    }

    /**
     * @return float
     */
    public function getHoldSum(): ?float
    {
        return $this->holdSum;
    }

    /**
     * @param float $holdSum
     * @return FuelOrderStatus
     */
    public function setHoldSum(float $holdSum): self
    {
        $this->holdSum = $holdSum;
        return $this;
    }

    /**
     * @return int
     */
    public function getHoldStatusCode(): int
    {
        return $this->holdStatusCode;
    }

    /**
     * @param int $holdStatusCode
     * @return FuelOrderStatus
     */
    public function setHoldStatusCode(int $holdStatusCode): self
    {
        $this->holdStatusCode = $holdStatusCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCardUuid(): string
    {
        return $this->cardUuid;
    }

    /**
     * @param string $cardUuid
     * @return FuelOrderStatus
     */
    public function setCardUuid(string $cardUuid): self
    {
        $this->cardUuid = $cardUuid;
        return $this;
    }

    /**
     * @return string
     */
    public function getSuccessUrl(): string
    {
        return $this->successUrl;
    }

    /**
     * @param string $successUrl
     * @return FuelOrderStatus
     */
    public function setSuccessUrl(string $successUrl): self
    {
        $this->successUrl = $successUrl;
        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreateDate(): DateTimeInterface
    {
        return $this->createDate;
    }


}
