<?php


namespace App\Message;


class PublisherMessage
{
    /**
     * @var int
     */
    private $orderId;

    /**
     * @var array
     */
    private $content;

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     * @return PublisherMessage
     */
    public function setOrderId(int $orderId): self
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->content;
    }

    /**
     * @param array $content
     * @return PublisherMessage
     */
    public function setContent(array $content): self
    {
        $this->content = $content;
        return $this;
    }


}
