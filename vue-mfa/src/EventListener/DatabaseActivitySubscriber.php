<?php

namespace App\EventListener;

use App\Entity\Application;
use App\Entity\ApplicationImage;
use App\Service\ImageService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;


class DatabaseActivitySubscriber implements EventSubscriber
{
    private ImageService $imageService;

    public function getSubscribedEvents(): array
    {
        return [
            Events::postRemove
        ];
    }

    public function postRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if($entity instanceof ApplicationImage){
            $this->imageService->removeImageFile(
                ApplicationImage::class,
                'imageUrl',
                $entity->getImageUrl()
            );
        }

        if($entity instanceof Application){
            $this->imageService->removeImageFile(
                Application::class,
                'iconUrl',
                $entity->getIconUrl()
            );
        }
    }

    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }
}
