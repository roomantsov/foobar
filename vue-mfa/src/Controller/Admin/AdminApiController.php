<?php

namespace App\Controller\Admin;

use App\Entity\Application;
use App\Entity\User;
use App\Service\ImageService;
use App\Service\SlugService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/api")
 */
class AdminApiController extends AbstractController
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em){
        $this->em = $em;
    }

    /**
     * @Route("/app/save", name="appSaveAction", methods={"POST"})
     * @param Request $request
     * @param ImageService $imageService
     * @return JsonResponse
     */
    public function saveApplication(
        Request $request,
        ImageService $imageService,
        SlugService $slugService,
        Security $security
    ): JsonResponse
    {
        $isNewApp = false;
        if($request->request->get('id')){
            $appRepository = $this->em->getRepository(Application::class);
            $app = $appRepository->find($request->request->get('id'));
        }

        if(!isset($app) || !$app){
            $app = new Application();
            $isNewApp = true;
        }

        /** @var User $user */
        $user = $security->getUser();
        $app
            ->setTitle($request->request->get('appTitle'))
            ->setInternalTitle($request->request->get('internalTitle'))
            ->setUser($user)
            ->setScreenOrderJson($request->request->get('imageGalleryJson'));

        $currentIconUrl = $app->getIconUrl();

        if($request->files->has('appIcon')){
            $imageFile = $imageService->uploadAppIcon($request->files->get('appIcon'));

            $app->setIconUrl($imageFile);
        }

        if($isNewApp){
            $app->setSlug($slugService->getUniqueSlug($app));
            $this->em->persist($app);
        }

        if($request->files->has('images')){
            $imageService->uploadAppScreens($request->files->get('images'), $app);
        }

        $this->em->flush();

        $imageService->deleteNotUsed($app, json_decode($request->request->get('imageGalleryJson')));

        if($currentIconUrl && $currentIconUrl !== $app->getIconUrl()){
            $imageService->removeImageFile(
                Application::class,
                'iconUrl',
                $currentIconUrl
            );
        }

        return $this->json([
            'status' => 'OK',
            'app'    => $app
        ]);
    }

    /**
     * @Route("/apps/all", name="allAppsAction", methods={"GET"})
     */
    public function getAllApplications(Security $security): JsonResponse
    {
        $appRepository = $this->em->getRepository(Application::class);

        return $this->json([
            'status'       => 'OK',
            'applications' => $appRepository->findBy([
                'user' => $security->getUser()
            ])
        ]);
    }

    /**
     * @Route("/app/{id}", name="getAppByIdAction", requirements={"id"="\d+"}, methods={"GET"})
     */
    public function getApplicationById(
        int $id,
        Security $security
    ): JsonResponse
    {
        $appRepository = $this->em->getRepository(Application::class);
        $app = $appRepository->find($id);

        if(!$app){
            return $this->json([
                'status'  => 'error',
                'message' => sprintf(
                    "There is no app with such id: %d",
                    $id
                )
            ], 404);
        }

        if($app->getUser() !== $security->getUser()){
            return $this->json([
                'status'  => 'error',
                'message' => "You don't have access to this app."
            ], 403);
        }

        return $this->json([
            'status'      => 'OK',
            'application' => $app,
            'appIconSrc'  => $app->getIconUrl()
        ]);
    }

    /**
     * @Route("/app/{id}", name="deleteAppAction", requirements={"id"="\d+"}, methods={"DELETE"})
     * @param int $id
     * @return JsonResponse
     */
    public function deleteApplication(
        int $id,
        Security $security
    ): JsonResponse
    {
        $appRepository = $this->em->getRepository(Application::class);
        $app = $appRepository->find($id);

        if(!$app){
            return $this->json([
                'status'  => 'error',
                'message' => sprintf(
                    "There is no app with such id: %d",
                    $id
                )
            ], 500);
        }

        if($app->getUser() !== $security->getUser()){
            return $this->json([
                'status'  => 'error',
                'message' => "You don't have access to this app."
            ], 403);
        }

        $this->em->remove($app);
        $this->em->flush();

        return $this->json([]);
    }
}
