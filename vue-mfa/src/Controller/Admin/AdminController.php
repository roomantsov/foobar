<?php

namespace App\Controller\Admin;

use App\Entity\Application;
use App\Entity\ApplicationImage;
use App\Entity\User;
use App\Service\ImageUrlHelper;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/")
 */
class AdminController extends AbstractDashboardController
{
    /**
     * @Route("/", name="admin")
     */
    public function index(): Response
    {
        return $this->redirectToRoute('appsManagement');
    }

    /**
     * @Route("/apps", name="appsManagement")
     */
    public function appsManagement(ImageUrlHelper $imageUrlHelper): Response
    {
        header("Access-Control-Allow-Origin: *");
        return $this->render("admin/page/apps.html.twig", [
            'viewData' => [
                'iconBaseUrl' => $imageUrlHelper->getMediaPath(Application::class, 'iconUrl'),
                'appScreenBaseUrl' => $imageUrlHelper->getMediaPath(ApplicationImage::class, 'imageUrl')
            ]
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('StarkCode');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Apps', 'fa fa-rocket', 'appsManagement');
        yield MenuItem::linkToCrud('Users', 'fa fa-user', User::class)
            ->setPermission('ROLE_ADMIN');
    }

    public function configureAssets(): Assets
    {
        return Assets::new()
            ->addHtmlContentToHead('<style>:root { --color-primary: #4e14e3; }</style>');
    }
}
