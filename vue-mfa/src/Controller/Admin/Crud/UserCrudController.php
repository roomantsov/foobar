<?php

namespace App\Controller\Admin\Crud;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Component\Security\Core\Security;

class UserCrudController extends AbstractCrudController
{
    const SUFFICIENT_ROLE = 'ROLE_USER';
    const HIGHEST_ROLE = 'ROLE_SUPER_ADMIN';

    private $security;

    private $roleHierarchy;

    private $adminUrlGenerator;

    public function __construct(Security $security, RoleHierarchyInterface $roleHierarchy, AdminUrlGenerator $adminUrlGenerator)
    {
        $this->security = $security;
        $this->roleHierarchy = $roleHierarchy;
        $this->adminUrlGenerator = $adminUrlGenerator;
    }

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setEntityPermission(self::SUFFICIENT_ROLE);
    }

    public function configureFields(string $pageName): iterable
    {
        $passwordField = TextField::new('plain_password', 'Password')
            ->onlyOnForms()
            ->setFormType(PasswordType::class)
            ->setRequired(true)
            ->setHelp('Leave it blank if you\'re not gonna change it.');

        if($pageName == Crud::PAGE_EDIT) {
            $passwordField->setRequired(false);
        }

        $availableRoles = [];
        if($pageName === Crud::PAGE_EDIT || $pageName === Crud::PAGE_NEW){
            $reachableRoles = $this->roleHierarchy->getReachableRoleNames($this->security->getUser()->getRoles());
        } else {
            $reachableRoles = $this->roleHierarchy->getReachableRoleNames([self::HIGHEST_ROLE]);
        }

        foreach($reachableRoles as $role){
            $availableRoles[$role] = $role; // Made for choices list
        }

        return [
            EmailField::new('email'),
            TextField::new('username'),
            ChoiceField::new('roles')
                ->setChoices($availableRoles)
                ->allowMultipleChoices()
                ->autocomplete(),
            $passwordField,
        ];
    }

    public function edit(AdminContext $context)
    {
        /** @var User $userEntity */
        $actionsConfig = $context->getCrud()->getActionsConfig();
        $reachableRoles = $this->roleHierarchy->getReachableRoleNames($this->security->getUser()->getRoles());
        $userEntity = $context->getEntity()->getInstance();

        $sufficientPermission = true;

        foreach($userEntity->getRoles() as $role){
            if(!in_array($role, $reachableRoles)){
                $sufficientPermission = false;
                break;
            }
        }

        if(!$sufficientPermission){
            $this->addFlash(
                'danger',
                '<b>Permission denied!</b> You can edit only: ' . implode(', ', $reachableRoles)
            );

            $this->adminUrlGenerator
                ->setController(self::class)
                ->setAction(Crud::PAGE_DETAIL)
                ->setEntityId($userEntity->getId());

            return $this->redirect($this->adminUrlGenerator->generateUrl());
        }

        return parent::edit($context);
    }

    public function detail(AdminContext $context)
    {
        $context->getCrud()->getActionsConfig()->disableActions([Action::DELETE]);

        return parent::detail($context);
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_EDIT, Action::DELETE)
            ->remove(Crud::PAGE_INDEX, Action::DELETE);
    }

}
