<?php

namespace App\Controller;

use App\Entity\Application;
use App\Entity\ApplicationImage;
use App\Service\ImageUrlHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/app")
 */
class PwaController extends AbstractController
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/{slug}", name="homeAction", methods={"GET"})
     */
    public function applicationHome(
        string $slug,
        ImageUrlHelper $imageUrlHelper,
        SerializerInterface $serializer
    ): Response
    {
        $appRepository = $this->em->getRepository(Application::class);
        $app = $appRepository->findOneBy([
            'slug' => $slug
        ]);

        if(null === $app){
            throw new NotFoundHttpException("There is no such app.");
        }

        $applicationsImages = $app->getApplicationImages()->toArray();
        $sortOrder = array_flip(json_decode($app->getScreenOrderJson()));
        usort($applicationsImages, function(ApplicationImage $a, ApplicationImage $b) use ($sortOrder) {
            return $sortOrder[$a->getSortingUuid()] > $sortOrder[$b->getSortingUuid()];
        });

        return $this->render('pwa/home.html.twig', [
            'app'              => $app,
            'appScreensJson'   => $serializer->serialize($applicationsImages, 'json'),
            'appScreenBaseUrl' => $imageUrlHelper->getMediaPath(ApplicationImage::class, 'imageUrl'),
            'iconBaseUrl'      => $imageUrlHelper->getMediaPath(Application::class, 'iconUrl')
        ]);
    }

    /**
     * @Route("/{slug}/manifest.json", name="manifest", methods={"GET"})
     */
    public function manifest(
        string $slug,
        ImageUrlHelper $imageUrlHelper,
        Request $request
    ): Response
    {
        $appRepository = $this->em->getRepository(Application::class);
        $app = $appRepository->findOneBy([
            'slug' => $slug
        ]);

        if(null === $app){
            throw new NotFoundHttpException("There is no such app.");
        }

        $icon = pathinfo($app->getIconUrl());

        return $this->json([
            'id' => $app->getSlug(),
            'name' => $app->getTitle(),
            'short_name' => $app->getTitle(),
            'start_url' => $this->generateUrl('homeAction', [
                'slug' => $app->getSlug()
            ]),
            'display' => 'standalone',
            'background_color' => '#fdfdfd',
            'theme_color' => '#fdfdfd',
            'orientation' => 'portrait-primary',
            'icons' => [
                [
                    'src' => sprintf('%s/%s/%s',
                        $request->getSchemeAndHttpHost(),
                        $imageUrlHelper->getMediaPath(Application::class, 'iconUrl'),
                        $app->getIconUrl()
                    ),
                    'type' => 'image/' . $icon['extension']
                ]
            ]
        ]);
    }
}