<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

class ImageUrlHelper
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getMediaPath(string $entityClass, string $field): string
    {
        $imageUrlConfig = $this->container->getParameter('image_url_config');

        return $imageUrlConfig[$entityClass][$field];
    }

}