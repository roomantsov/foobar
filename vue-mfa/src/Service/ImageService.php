<?php

namespace App\Service;

use App\Entity\Application;
use App\Entity\ApplicationImage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\String\ByteString;

class ImageService
{
    public const ALLOWED_MIME_TYPES = [
        'image/png',
        'image/jpg',
        'image/jpeg',
        'image/webp'
    ];

    private ImageUrlHelper $imageUrlHelper;
    private EntityManagerInterface $em;
    private ParameterBagInterface $parameterBag;

    public function __construct(
        ImageUrlHelper $imageUrlHelper,
        EntityManagerInterface $em,
        ParameterBagInterface $parameterBag
    ){
        $this->imageUrlHelper = $imageUrlHelper;
        $this->em = $em;
        $this->parameterBag = $parameterBag;
    }

    public function uploadAppIcon($appIconFile): string
    {
        if(!$this->validateFile($appIconFile)){
            // todo: push the error
        }

        $appIconNewName = sprintf(
            "%s.%s",
            ByteString::fromRandom(16)->toString(),
            $appIconFile->getClientOriginalExtension()
        );

        $appIconFile->move(
            $this->imageUrlHelper->getMediaPath(Application::class, 'iconUrl'),
            $appIconNewName
        );

        return $appIconNewName;
    }

    public function uploadAppScreens(
        array $screens,
        Application $app
    ): array
    {
        $uploadedScreens = [];

        foreach($screens as $sortingUuid => $screen){
            if(!$this->validateFile($screen)){
                continue;
            }

            $appScreenUrl = $this->uploadAppScreen($screen);
            $appImage = (new ApplicationImage())
                ->setImageUrl($appScreenUrl)
                ->setSortingUuid($sortingUuid)
                ->setApplication($app);

            $this->em->persist($appImage);

            $uploadedScreens[] = $appImage;
        }

        return $uploadedScreens;
    }

    public function uploadAppScreen($screen): string
    {
        $newFileName = sprintf(
            "%s.%s",
            ByteString::fromRandom(16)->toString(),
            $screen->getClientOriginalExtension()
        );

        $screen->move(
            $this->imageUrlHelper->getMediaPath(ApplicationImage::class, 'imageUrl'),
            $newFileName
        );

        return $newFileName;
    }

    public function validateFile($file): bool
    {
        return in_array($file->getMimeType(), self::ALLOWED_MIME_TYPES);
    }

    public function deleteNotUsed(
        Application $app,
        array $usedSortingUuids
    ): void
    {
        $queryBuilder = $this->em->createQueryBuilder();
        $queryBuilder
            ->select('ai')
            ->from(ApplicationImage::class, 'ai')
            ->where('ai.application = :app')
            ->setParameter('app', $app->getId());

        if(count($usedSortingUuids)){
            $queryBuilder->andWhere($queryBuilder->expr()->notIn('ai.sortingUuid', $usedSortingUuids));
        }

        $notUsedImages = $queryBuilder->getQuery()->getResult();
        foreach($notUsedImages as $image){
            $this->em->remove($image);
        }

        $this->em->flush();
    }

    public function removeImageFile(
        string $entityFqsn,
        string $field,
        string $imageFileUrl
    ): void
    {
        $iconFilesPath = $this->imageUrlHelper->getMediaPath($entityFqsn, $field);
        @unlink($this->parameterBag->get('kernel.project_dir') . '/public/' . $iconFilesPath . '/' . $imageFileUrl);
    }
}
