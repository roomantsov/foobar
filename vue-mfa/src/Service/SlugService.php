<?php

namespace App\Service;

use App\Entity\SlugIdentifiableInterface;
use Doctrine\ORM\EntityManagerInterface;

class SlugService
{
    public const SLUG_LENGTH = 6;

    public EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getUniqueSlug(SlugIdentifiableInterface $entity): string
    {
        $slug = $this->generateSlug();

        if($this->checkIfSlugExists($entity, $slug)){
            return $this->getUniqueSlug($entity);
        }

        return $slug;
    }

    public function checkIfSlugExists(
        SlugIdentifiableInterface $entity,
        string $slug
    ): bool
    {
        $entityRepository = $this->entityManager->getRepository(get_class($entity));
        $existingEntityBySlug = $entityRepository->findOneBy(['slug' => $slug]);

        return null !== $existingEntityBySlug;
    }

    public function generateSlug(): string
    {
        $string = '';
        $vowels = ['a', 'e', 'i', 'o', 'u'];
        $consonants = [
            'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm',
            'n', 'p', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'
        ];

        $max = self::SLUG_LENGTH / 2;
        for ($i = 1; $i <= $max; $i++)
        {
            $string .= rand(0, 1) ? $consonants[rand(0,19)] : strtoupper($consonants[rand(0,19)]);
            $string .= rand(0, 1) ? $vowels[rand(0,4)] : strtoupper($vowels[rand(0,4)]);
        }

        return $string;
    }
}