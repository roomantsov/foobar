<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ApplicationRepository;

/**
 * @ORM\Entity(repositoryClass=ApplicationRepository::class)
 */
class Application implements \JsonSerializable, SlugIdentifiableInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $internalTitle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $iconUrl;

    /**
     * @ORM\OneToMany(targetEntity=ApplicationImage::class, mappedBy="application", orphanRemoval=true, cascade={"persist"})
     */
    private $applicationImages;

    /**
     * @ORM\Column(type="text")
     */
    private $screenOrderJson;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $user;

    public function __construct()
    {
        $this->applicationImages = new ArrayCollection();
    }

    public function jsonSerialize()
    {
        return [
            'id'               => $this->getId(),
            'internalTitle'    => $this->getInternalTitle(),
            'appTitle'         => $this->getTitle(),
            'imageGalleryJson' => $this->getScreenOrderJson(),
            'appIcon'          => $this->getIconUrl(),
            'slug'             => $this->getSlug(),
            'appScreens'       => $this->getApplicationImages()
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getInternalTitle(): ?string
    {
        return $this->internalTitle;
    }

    public function setInternalTitle(string $internalTitle): self
    {
        $this->internalTitle = $internalTitle;

        return $this;
    }

    public function getIconUrl(): ?string
    {
        return $this->iconUrl;
    }

    public function setIconUrl(string $iconUrl): self
    {
        $this->iconUrl = $iconUrl;

        return $this;
    }

    public function getScreenOrderJson(): ?string
    {
        return $this->screenOrderJson;
    }

    public function setScreenOrderJson(string $screenOrderJson): self
    {
        $this->screenOrderJson = $screenOrderJson;

        return $this;
    }

    /**
     * @return Collection<int, ApplicationImage>
     */
    public function getApplicationImages(): Collection
    {
        return $this->applicationImages;
    }

    public function addApplicationImage(ApplicationImage $applicationImage): self
    {
        if (!$this->applicationImages->contains($applicationImage)) {
            $this->applicationImages[] = $applicationImage;
            $applicationImage->setApplication($this);
        }

        return $this;
    }

    public function removeApplicationImage(ApplicationImage $applicationImage): self
    {
        if ($this->applicationImages->removeElement($applicationImage)) {
            // set the owning side to null (unless already changed)
            if ($applicationImage->getApplication() === $this) {
                $applicationImage->setApplication(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
