<?php

namespace App\Entity;

interface SlugIdentifiableInterface
{
    public function getSlug(): ?string;
    public function setSlug(string $slug);
}