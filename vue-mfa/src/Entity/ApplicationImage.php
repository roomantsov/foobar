<?php

namespace App\Entity;

use App\Repository\ApplicationImageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ApplicationImageRepository::class)
 */
class ApplicationImage implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imageUrl;

    /**
     * @ORM\ManyToOne(targetEntity=Application::class, inversedBy="applicationImages")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $application;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sortingUuid;

    public function jsonSerialize()
    {
        return [
            'id'          => $this->getId(),
            'img'         => $this->getImageUrl(),
            'sortingUuid' => $this->getSortingUuid()
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function setImageUrl(?string $imageUrl): self
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    public function getApplication(): ?Application
    {
        return $this->application;
    }

    public function setApplication(?Application $application): self
    {
        $this->application = $application;

        return $this;
    }

    public function getSortingUuid(): ?string
    {
        return $this->sortingUuid;
    }

    public function setSortingUuid(string $sortingUuid): self
    {
        $this->sortingUuid = $sortingUuid;

        return $this;
    }
}
