<?php

declare(strict_types=1);

namespace Uplink\UpworkSearch\Domain\Service;

use Doctrine\ORM\EntityManagerInterface;
use Uplink\UpworkSearch\Infrastructure\DTO\Assembler\JobPostAssembler;
use Uplink\UpworkSearch\Infrastructure\DTO\JobPostCollection;
use Uplink\UpworkSearch\Infrastructure\Entity\JobPost;
use Uplink\UpworkSearch\Infrastructure\Loader\JobLoaderInterface;
use Uplink\UpworkSearch\Infrastructure\Repository\JobPostRepository;
use Uplink\UpworkSearch\Infrastructure\Repository\SearchRepository;

class JobSyncService
{
    protected SearchRepository $searchRepository;

    protected JobLoaderInterface $jobLoader;

    protected JobPostRepository $jobPostRepository;

    protected JobPostAssembler $jobPostAssembler;

    protected EntityManagerInterface $entityManager;

    public function __construct(
        SearchRepository $searchRepository,
        JobLoaderInterface $jobLoader,
        JobPostRepository $jobPostRepository,
        JobPostAssembler $jobPostAssembler,
        EntityManagerInterface $entityManager
    ) {
        $this->searchRepository = $searchRepository;
        $this->jobLoader = $jobLoader;
        $this->jobPostRepository = $jobPostRepository;
        $this->jobPostAssembler = $jobPostAssembler;
        $this->entityManager = $entityManager;
    }

    public function syncJobPosts() {
        $searches = $this->searchRepository->findBy([
            'isActive' => true
        ]);

        foreach ($searches as $search) {
            $jobPosts = $this->jobLoader->loadJobPosts($search);
            $this->uploadJobPosts($jobPosts);
            $search->setSyncedAt(new \DateTimeImmutable());
        }

        $this->entityManager->flush();
    }

    public function checkIsUnique(string $postUrl): bool
    {
        $jobPost = $this->jobPostRepository->findOneBy([
            'postUrl' => $postUrl
        ]);

        return $jobPost === null;
    }

    protected function uploadJobPosts(JobPostCollection $jobPosts): void
    {
        foreach ($jobPosts->getItems() as $jobPostDTO) {
            if (!$this->checkIsUnique($jobPostDTO->getPostUrl())) {
                continue;
            }

            $jobPostEntity = new JobPost();
            $this->jobPostRepository->add(
                $this->jobPostAssembler->applyToEntity($jobPostDTO, $jobPostEntity),
                true
            );
        }
    }
}
