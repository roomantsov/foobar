<?php

namespace Uplink\UpworkSearch\Application\Command\Handler;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Uplink\UpworkSearch\Application\Command\SyncJobsCommand;
use Uplink\UpworkSearch\Domain\Service\JobSyncService;

class SyncJobsCommandHandler implements MessageHandlerInterface
{
    protected JobSyncService $jobSyncService;

    public function __construct(JobSyncService $jobSyncService) {
        $this->jobSyncService = $jobSyncService;
    }

    public function __invoke(SyncJobsCommand $command) {
        $this->jobSyncService->syncJobPosts();
    }
}