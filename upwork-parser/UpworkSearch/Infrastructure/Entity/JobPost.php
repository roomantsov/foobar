<?php

declare(strict_types=1);

namespace Uplink\UpworkSearch\Infrastructure\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Uplink\Common\Infrastructure\Entity\Skill;
use Uplink\Common\Infrastructure\Entity\Subcategory;
use Uplink\UpworkSearch\Infrastructure\Repository\JobPostRepository;
use Carbon\Carbon;

/**
 * @ORM\Entity(repositoryClass=JobPostRepository::class)
 */
class JobPost
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $budget;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rangeFrom;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rangeTo;

    /**
     * @ORM\ManyToOne(targetEntity=Subcategory::class, inversedBy="jobPosts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $subcategory;

    /**
     * @ORM\ManyToMany(targetEntity=Skill::class, inversedBy="jobPosts")
     */
    private $skills;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postUrl;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $applyUrl;

    /**
     * @ORM\ManyToOne(targetEntity=Search::class, inversedBy="jobPosts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $search;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $postedAt;

    public function __construct()
    {
        $this->skills = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getBudget(): ?int
    {
        return $this->budget;
    }

    public function setBudget(?int $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    public function getRangeFrom(): ?int
    {
        return $this->rangeFrom;
    }

    public function setRangeFrom(?int $rangeFrom): self
    {
        $this->rangeFrom = $rangeFrom;

        return $this;
    }

    public function getRangeTo(): ?int
    {
        return $this->rangeTo;
    }

    public function setRangeTo(?int $rangeTo): self
    {
        $this->rangeTo = $rangeTo;

        return $this;
    }

    public function getSubcategory(): ?Subcategory
    {
        return $this->subcategory;
    }

    public function setSubcategory(?Subcategory $subcategory): self
    {
        $this->subcategory = $subcategory;

        return $this;
    }

    /**
     * @return Collection<int, Skill>
     */
    public function getSkills(): Collection
    {
        return $this->skills;
    }

    public function addSkill(Skill $skill): self
    {
        if (!$this->skills->contains($skill)) {
            $this->skills[] = $skill;
        }

        return $this;
    }

    public function removeSkill(Skill $skill): self
    {
        $this->skills->removeElement($skill);

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPostUrl(): ?string
    {
        return $this->postUrl;
    }

    public function setPostUrl(string $postUrl): self
    {
        $this->postUrl = $postUrl;

        return $this;
    }

    public function getApplyUrl(): ?string
    {
        return $this->applyUrl;
    }

    public function setApplyUrl(string $applyUrl): self
    {
        $this->applyUrl = $applyUrl;

        return $this;
    }

    public function getSearch(): ?Search
    {
        return $this->search;
    }

    public function setSearch(?Search $search): self
    {
        $this->search = $search;

        return $this;
    }

    public function getPostedAt(): ?\DateTimeImmutable
    {
        return $this->postedAt;
    }

    public function setPostedAt(\DateTimeImmutable $postedAt): self
    {
        $this->postedAt = $postedAt;

        return $this;
    }

    public function getPostedAgo(): string
    {
        return Carbon::instance($this->getPostedAt())->diffForHumans();
    }
}
