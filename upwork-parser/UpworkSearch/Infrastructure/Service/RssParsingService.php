<?php

declare(strict_types=1);

namespace Uplink\UpworkSearch\Infrastructure\Service;

use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Money;
use Money\Parser\IntlLocalizedDecimalParser;
use Uplink\UpworkSearch\Infrastructure\DTO\JobPostCollection\JobPost;

class RssParsingService
{
    const APPLY_URL_PATTERN = 'https://www.upwork.com/ab/proposals/job/~%s/apply/';

    public function parseTitle(string $title): string
    {
        return html_entity_decode(trim(str_replace(
            '- Upwork',
            '',
            $title
        )));
    }

    public function parseSkills(string $content): array
    {
        $skillsString = $this->extractValue('<b>Skills</b>:', $content);

        if (!$skillsString) {
            return [];
        }

        return array_map('trim', explode(',', $skillsString));
    }

    public function parseDescription(string $content): string
    {
        $descriptionHtml = str_ireplace("\n", '', $content);

        return trim(html_entity_decode(str_ireplace(
            array("<br />", "<br>", "<br/>"),
            "\r\n",
            explode('<b>', $descriptionHtml)[0]
        ), ENT_QUOTES));
    }

    public function parseCountry(string $content): string
    {
        return $this->extractValue('<b>Country</b>:', $content);
    }

    public function parseCategory(string $content): string
    {
        return $this->extractValue('<b>Category</b>:', $content);
    }

    public function parsePostedAt(string $content): \DateTimeImmutable
    {
        return new \DateTimeImmutable(
            $this->extractValue('<b>Posted On</b>:', $content)
        );
    }

    public function parseBudget(string $content): Money
    {
        $budgetString = substr($this->extractValue('<b>Budget</b>:', $content), 1);

        return $this->parseMoney($budgetString);
    }

    public function parseRangeFrom(string $content): Money
    {
        return $this->parseMoney(substr(
            $this->parseRange($content)[0],
            1
        ));
    }

    public function parseRangeTo(string $content): Money
    {
        return $this->parseMoney(substr(
            $this->parseRange($content)[1],
            1
        ));
    }

    public function parseType(string $content): ?string
    {
        $type = null;

        if (strrpos($content, '<b>Budget</b>:')) {
            $type = JobPost::TYPE_FIXED;
        } else if (strrpos($content, '<b>Hourly Range</b>:')) {
            $type = JobPost::TYPE_HOURLY;
        }

        return $type;
    }

    public function parsePostUrl(string $url): string
    {
        return urldecode(explode('?', $url)[0]);
    }

    public function parseApplyUrl(string $url): string
    {
        $postUrl = $this->parsePostUrl($url);
        $postKey = explode('~', $postUrl)[1];

        return sprintf(
            self::APPLY_URL_PATTERN,
            $postKey
        );
    }

    protected function parseMoney($string): Money
    {
        $currencies = new ISOCurrencies();

        $numberFormatter = new \NumberFormatter('en_US', \NumberFormatter::DECIMAL);
        $moneyParser = new IntlLocalizedDecimalParser($numberFormatter, $currencies);

        return $moneyParser->parse($string, new Currency('USD'));
    }

    protected function parseRange(string $content): array
    {
        $rangeString = $this->extractValue('<b>Hourly Range</b>:', $content);

        return explode('-', $rangeString);
    }

    protected function extractValue(string $keyString, string $content): ?string
    {
        if (strpos($content, $keyString) === false) {
            return null;
        }

        $valueHtml = trim(
            explode(
                '<br />',
                explode(
                    $keyString,
                    $content,
                    2
                )[1]
            )[0]
        );

        return html_entity_decode($valueHtml);
    }
}
