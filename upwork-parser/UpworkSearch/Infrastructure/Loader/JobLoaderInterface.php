<?php

declare(strict_types=1);

namespace Uplink\UpworkSearch\Infrastructure\Loader;

use Uplink\UpworkSearch\Infrastructure\DTO\JobPostCollection;
use Uplink\UpworkSearch\Infrastructure\Entity\Search;

interface JobLoaderInterface
{
    public function loadJobPosts(Search $search): JobPostCollection;
}
