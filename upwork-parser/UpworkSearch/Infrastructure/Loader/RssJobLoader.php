<?php

declare(strict_types=1);

namespace Uplink\UpworkSearch\Infrastructure\Loader;

use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Uplink\UpworkSearch\Infrastructure\DTO\JobPostCollection;
use Uplink\UpworkSearch\Infrastructure\Entity\Search;

class RssJobLoader implements JobLoaderInterface
{
    protected HttpClientInterface $client;

    protected SerializerInterface $serializer;

    public function __construct(
        HttpClientInterface $client,
        SerializerInterface $serializer
    ) {
        $this->client = $client;
        $this->serializer = $serializer;
    }

    /**
     * @throws
     */
    public function loadJobPosts(Search $search): JobPostCollection
    {
            $rssResult = $this->client->request('GET', $search->getRss())->getContent();

            return $this->serializer->deserialize(
                $rssResult,
                JobPostCollection::class,
                'xml',
                [
                    'search' => $search
                ]
            );
    }
}
