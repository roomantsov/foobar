<?php

namespace Uplink\UpworkSearch\Infrastructure\Serializer\Denormalizer;

use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Uplink\UpworkSearch\Infrastructure\DTO\Assembler\JobPostCollectionAssembler;
use Uplink\UpworkSearch\Infrastructure\DTO\JobPostCollection;

class RssJobPostCollectionDenormalizer implements DenormalizerInterface, CacheableSupportsMethodInterface
{
    protected JobPostCollectionAssembler $jobPostCollectionAssembler;

    public function __construct(JobPostCollectionAssembler $jobPostCollectionAssembler) {
        $this->jobPostCollectionAssembler = $jobPostCollectionAssembler;
    }

    public function denormalize($data, string $type, string $format = null, array $context = []): JobPostCollection
    {
        return $this->jobPostCollectionAssembler->assembleFromArray(
            $data['channel']['item'],
            $context['search']
        );
    }

    public function supportsDenormalization($data, string $type, string $format = null): bool
    {
        return $type === JobPostCollection::class;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}