<?php

declare(strict_types=1);

namespace Uplink\UpworkSearch\Infrastructure\DTO\Assembler;

use Money\Money;
use Uplink\Common\Domain\Service\CategoryService;
use Uplink\Common\Domain\Service\SkillService;
use Uplink\UpworkSearch\Infrastructure\DTO\JobPostCollection\JobPost as JobPostDTO;
use Uplink\UpworkSearch\Infrastructure\Entity\JobPost as JobPostEntity;
use Uplink\UpworkSearch\Infrastructure\Entity\Search;
use Uplink\UpworkSearch\Infrastructure\Service\RssParsingService;

class JobPostAssembler
{
    protected RssParsingService $parsingService;

    protected SkillService $skillService;

    protected CategoryService $categoryService;

    public function __construct(
        RssParsingService $parsingService,
        SkillService $skillService,
        CategoryService $categoryService
    ) {
        $this->parsingService = $parsingService;
        $this->skillService = $skillService;
        $this->categoryService = $categoryService;
    }

    public function assembleFromArray(array $array, Search $search): JobPostDTO
    {
        $jobPost = new JobPostDTO();
        $content = $array['content:encoded'];

        $jobPost
            ->setTitle($this->parsingService->parseTitle($array['title']))
            ->setType($this->parsingService->parseType($content))
            ->setCategory($this->parsingService->parseCategory($content))
            ->setCountry($this->parsingService->parseCountry($content))
            ->setDescription($this->parsingService->parseDescription($content))
            ->setSkills($this->parsingService->parseSkills($content))
            ->setPostedAt($this->parsingService->parsePostedAt($content))
            ->setPostUrl($this->parsingService->parsePostUrl($array['link']))
            ->setApplyUrl($this->parsingService->parseApplyUrl($array['link']))
            ->setSearch($search);

        if ($jobPost->getType() === JobPostDTO::TYPE_HOURLY) {
            $jobPost
                ->setRangeFrom($this->parsingService->parseRangeFrom($content))
                ->setRangeTo($this->parsingService->parseRangeTo($content));
        } else if ($jobPost->getType() === JobPostDTO::TYPE_FIXED) {
            $jobPost->setBudget($this->parsingService->parseBudget($content));
        }

        return $jobPost;
    }

    public function applyToEntity(JobPostDTO $jobPostDTO, JobPostEntity $entity): JobPostEntity
    {
        $entity
            ->setTitle($jobPostDTO->getTitle())
            ->setDescription($jobPostDTO->getDescription())
            ->setType($jobPostDTO->getType())
            ->setCountry($jobPostDTO->getCountry())
            ->setPostUrl($jobPostDTO->getPostUrl())
            ->setApplyUrl($jobPostDTO->getApplyUrl())
            ->setSearch($jobPostDTO->getSearch())
            ->setPostedAt($jobPostDTO->getPostedAt());

        if ($jobPostDTO->getBudget() instanceof Money) {
            $entity->setBudget((int) $jobPostDTO->getBudget()->getAmount());
        } elseif (
            $jobPostDTO->getRangeFrom() instanceof Money
            && $jobPostDTO->getRangeTo() instanceof Money
        ) {
            $entity
                ->setRangeFrom((int) $jobPostDTO->getRangeFrom()->getAmount())
                ->setRangeTo((int) $jobPostDTO->getRangeTo()->getAmount());
        }

        foreach ($jobPostDTO->getSkills() as $skill) {
            $entity->addSkill($this->skillService->findOrCreateSkillByTitle($skill));
        }

        $entity->setSubcategory(
            $this->categoryService->findOrCreateSubcategoryByTitle($jobPostDTO->getCategory())
        );

        return $entity;
    }
}
