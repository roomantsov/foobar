<?php

declare(strict_types=1);

namespace Uplink\UpworkSearch\Infrastructure\DTO\Assembler;

use Uplink\UpworkSearch\Infrastructure\DTO\JobPostCollection;
use Uplink\UpworkSearch\Infrastructure\Entity\Search;

class JobPostCollectionAssembler
{
    protected JobPostAssembler $jobPostAssembler;

    public function __construct(JobPostAssembler $jobPostAssembler) {
        $this->jobPostAssembler = $jobPostAssembler;
    }

    public function assembleFromArray(array $array, Search $search): JobPostCollection
    {
        $jobPostCollection = new JobPostCollection();

        foreach ($array as $item) {
            $jobPostCollection->addItem(
                $this->jobPostAssembler->assembleFromArray($item, $search)
            );
        }

        return $jobPostCollection;
    }
}
