<?php

declare(strict_types=1);

namespace Uplink\UpworkSearch\Infrastructure\DTO\JobPostCollection;

use DateTimeImmutable;
use Money\Money;
use Uplink\UpworkSearch\Infrastructure\Entity\Search;

class JobPost
{
    public const TYPE_HOURLY = 'hourly';
    public const TYPE_FIXED = 'fixed';

    protected string $title = '';
    protected string $description = '';
    protected ?string $type = null;
    protected ?Money $budget = null;
    protected ?Money $rangeFrom = null;
    protected ?Money $rangeTo = null;
    protected DateTimeImmutable $postedAt;
    protected string $category = '';
    protected array $skills = [];
    protected string $country = '';
    protected string $postUrl;
    protected string $applyUrl;
    protected Search $search;

    public function __construct() {
        $this->postedAt = new DateTimeImmutable();
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getBudget(): ?Money
    {
        return $this->budget;
    }

    public function setBudget(?Money $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    public function getRangeFrom(): ?Money
    {
        return $this->rangeFrom;
    }

    public function setRangeFrom(?Money $rangeFrom): self
    {
        $this->rangeFrom = $rangeFrom;

        return $this;
    }

    public function getRangeTo(): ?Money
    {
        return $this->rangeTo;
    }

    public function setRangeTo(?Money $rangeTo): self
    {
        $this->rangeTo = $rangeTo;

        return $this;
    }

    public function getPostedAt(): DateTimeImmutable
    {
        return $this->postedAt;
    }

    public function setPostedAt(DateTimeImmutable $postedAt): self
    {
        $this->postedAt = $postedAt;

        return $this;
    }

    public function getCategory(): string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getSkills(): array
    {
        return $this->skills;
    }

    public function setSkills(array $skills): self
    {
        $this->skills = $skills;

        return $this;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPostUrl(): string
    {
        return $this->postUrl;
    }

    public function setPostUrl(string $postUrl): self
    {
        $this->postUrl = $postUrl;

        return $this;
    }

    public function getApplyUrl(): string
    {
        return $this->applyUrl;
    }

    public function setApplyUrl(string $applyUrl): self
    {
        $this->applyUrl = $applyUrl;

        return $this;
    }

    public function getSearch(): Search
    {
        return $this->search;
    }

    public function setSearch(Search $search): self
    {
        $this->search = $search;

        return $this;
    }
}
