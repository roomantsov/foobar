<?php

declare(strict_types=1);

namespace Uplink\UpworkSearch\Infrastructure\DTO;

use Uplink\UpworkSearch\Infrastructure\DTO\JobPostCollection\JobPost;
use Doctrine\Common\Collections\ArrayCollection;
use Uplink\UpworkSearch\Infrastructure\Entity\Search;

class JobPostCollection
{
    /**
     * @var ArrayCollection<JobPost>
     */
    protected ArrayCollection $items;

    public function __construct() {
        $this->items = new ArrayCollection();
    }

    /**
     * @return ArrayCollection<JobPost>
     */
    public function getItems(): ArrayCollection
    {
        return $this->items;
    }

    public function setItems(ArrayCollection $items): self
    {
        $this->items = $items;

        return $this;
    }

    public function addItem(JobPost $jobPost): self
    {
        $this->items->add($jobPost);

        return $this;
    }
}
