<?php

declare(strict_types=1);

namespace Uplink\UpworkSearch\Presentation\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Uplink\UpworkSearch\Application\Command\SyncJobsCommand;

class SyncJobPosts extends Command
{
    use HandleTrait;

    public function __construct(MessageBusInterface $messageBus, string $name = null) {
        $this->messageBus = $messageBus;
        parent::__construct($name);
    }

    protected static $defaultName = 'uplink:jobs:sync';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->handle(new SyncJobsCommand());
        return self::SUCCESS;
    }
}
