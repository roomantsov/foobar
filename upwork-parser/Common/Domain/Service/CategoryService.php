<?php

namespace Uplink\Common\Domain\Service;

use Uplink\Common\Infrastructure\Entity\Subcategory;
use Uplink\Common\Infrastructure\Repository\SubcategoryRepository;

class CategoryService
{
    protected SubcategoryRepository $subcategoryRepository;

    public function __construct(SubcategoryRepository $subcategoryRepository) {
        $this->subcategoryRepository = $subcategoryRepository;
    }

    public function findOrCreateSubcategoryByTitle(string $title): Subcategory
    {
        $skill = $this->subcategoryRepository->findOneBy(['title' => $title]);
        return $skill ?? $this->subcategoryRepository->add((new Subcategory())->setTitle($title), true);
    }
}