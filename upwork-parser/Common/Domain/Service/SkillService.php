<?php

declare(strict_types=1);

namespace Uplink\Common\Domain\Service;

use Uplink\Common\Infrastructure\Entity\Skill;
use Uplink\Common\Infrastructure\Repository\SkillRepository;

class SkillService
{
    protected SkillRepository $skillRepository;

    public function __construct(SkillRepository $skillRepository) {
        $this->skillRepository = $skillRepository;
    }

    public function findOrCreateSkillByTitle(string $title): Skill
    {
        $skill = $this->skillRepository->findOneBy(['title' => $title]);
        return $skill ?? $this->skillRepository->add((new Skill())->setTitle($title), true);
    }
}
